<?php
/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 5/29/18
 * Time: 3:34 PM
 */

namespace MiamiOH\CourseSectionWebService\Services;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCreditHoursDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionLevelDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionSummaryCollection;
use MiamiOH\Pike\Domain\Collection\CrossListedCourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\PersonCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\Model\CourseSectionCreditHoursDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionLevelDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\Model\CourseSectionSummary;
use MiamiOH\Pike\Domain\Model\CrossListedCourseSection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Exception\NotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;

class CourseSectionService extends BaseCourseSectionService
{
    public function getCourseSection()
    {
        $this->setup();
        try {
            if ($this->request->isPaged()) {
                $this->options['limit'] = $this->request->getLimit();
                $this->options['offset'] = $this->request->getOffset();
            }

            // get a list of composed model name
            $subObjects = $this->request->getSubObjects();

            // construct search object by filters
            $searchRequest = $this->setFilters($this->options);

            // get search results
            $courseSectionCollection = $this->pike
                ->getViewCourseSectionService()
                ->searchCourseSection($searchRequest);

            // retrieve actual total number of items
            $totalRecords = 0;
            if ($courseSectionCollection->isPageable()) {
                $totalRecords = $courseSectionCollection->getTotalNumOfItems();
            }

            $payload = $this->constructPayload(
                $courseSectionCollection,
                $subObjects
            );

            $this->response->setTotalObjects($totalRecords);
            $this->response->setPayload($payload);
        } catch (BadRequest|InvalidArgumentException|\InvalidArgumentException $e) {
            $this->response->setStatus(App::API_BADREQUEST);
            $this->response->setPayload([$e->getMessage()]);
        }

        return $this->response;
    }

    public function getCourseSectionGuid()
    {
        $this->setup();

        $guid = $this->request->getResourceParam('courseSectionGuid');
        try {
            $subObjectList = $this->request->getSubObjects();
            $courseSection = $this->pike
                ->getViewCourseSectionService()
                ->getByGuid($guid);

            $payload = $this->constructPayload(
                new CourseSectionCollection([$courseSection]),
                $subObjectList
            );

            $this->response->setPayload($payload[0]);
        } catch (InvalidArgumentException $e) {
            $this->response->setStatus(App::API_BADREQUEST);
            $this->response->setPayload([$e->getMessage()]);
        } catch (NotFoundException $e) {
            $this->response->setStatus(App::API_NOTFOUND);
        }

        return $this->response;
    }

    public function setFilters(array $options): SearchCourseSectionRequest
    {
        $searchCourseSectionRequest = new SearchCourseSectionRequest();

        foreach ($options as $key => $value) {
            switch ($key) {
                case 'courseSectionGuid':
                    $searchCourseSectionRequest->setCourseSectionGuids($value);
                    break;
                case 'campusCode':
                    $searchCourseSectionRequest->setCampusCodes($value);
                    break;
                case 'termCode':
                    $searchCourseSectionRequest->setTermCodes($value);
                    break;
                case 'crn':
                    $searchCourseSectionRequest->setCrns($value);
                    break;
                case 'course_subjectCode':
                    $searchCourseSectionRequest->setSubjectCodes($value);
                    break;
                case 'course_number':
                    $searchCourseSectionRequest->setCourseNumbers($value);
                    break;
                case 'courseSectionCode':
                    $searchCourseSectionRequest->setCourseSectionCodes($value);
                    break;
                case 'courseSectionStatusCode':
                    $searchCourseSectionRequest->setCourseSectionStatusCodes($value);
                    break;
                case 'enrollmentCount_numberOfMax':
                    $numberOfMax = $this->getCountRange($value);
                    $searchCourseSectionRequest->setNumOfMaxEnrollments($numberOfMax[0], $numberOfMax[1] ?? null);
                    break;
                case 'enrollmentCount_numberOfCurrent':
                    $numberOfCurrent = $this->getCountRange($value);
                    $searchCourseSectionRequest->setNumOfCurrentEnrollments($numberOfCurrent[0], $numberOfCurrent[1] ?? null);
                    break;
                case 'enrollmentCount_numberOfActive':
                    $numberOfActive = $this->getCountRange($value);
                    $searchCourseSectionRequest->setNumOfActiveEnrollments($numberOfActive[0], $numberOfActive[1] ?? null);
                    break;
                case 'enrollmentCount_numberOfAvailable':
                    $numberOfAvailable = $this->getCountRange($value);
                    $searchCourseSectionRequest->setNumOfAvailableEnrollments($numberOfAvailable[0], $numberOfAvailable[1] ?? null);
                    break;
                case 'isMidtermGradeSubmissionAvailable':
                    if ($value !== null && $value !== 'true' && $value !== 'false') {
                        throw new BadRequest('Invalid isMidtermGradeSubmissionAvailable');
                    }
                    if ($value === 'true') {
                        $searchCourseSectionRequest->setIsMidtermGradeSubmissionAvailable(true);
                    }
                    if ($value === 'false') {
                        $searchCourseSectionRequest->setIsMidtermGradeSubmissionAvailable(false);
                    }
                    break;
                case 'isFinalGradeSubmissionAvailable':
                    if ($value !== null && $value !== 'true' && $value !== 'false') {
                        throw new BadRequest('Invalid isFinalGradeSubmissionAvailable');
                    }
                    if ($value === 'true') {
                        $searchCourseSectionRequest->setIsFinalGradeSubmissionAvailable(true);
                    }
                    if ($value === 'false') {
                        $searchCourseSectionRequest->setIsFinalGradeSubmissionAvailable(false);
                    }
                    break;
                case 'hasSeatAvailable':
                    if ($value !== null && $value !== 'true' && $value !== 'false') {
                        throw new BadRequest('Invalid hasSeatAvailable');
                    }
                    if ($value === 'true') {
                        $searchCourseSectionRequest->setHasSeatAvailable(true);
                    }
                    if ($value === 'false') {
                        $searchCourseSectionRequest->setHasSeatAvailable(false);
                    }
                    break;
                case 'partOfTerm_code':
                    $searchCourseSectionRequest->setPartOfTermCodes($value);
                    break;
                case 'partOfTerm_startDate':
                    try {
                        $partOfTermStartDate = $this->getDateRange($value);
                    } catch (\InvalidArgumentException $e) {
                        throw new BadRequest('Invalid partOfTermStartDate');
                    }
                    $searchCourseSectionRequest->setPartOfTermStartDate($partOfTermStartDate[0], $partOfTermStartDate[1] ?? null);
                    break;
                case 'partOfTerm_endDate':
                    try {
                        $partOfTermEndDate = $this->getDateRange($value);
                    } catch (\InvalidArgumentException $e) {
                        throw new BadRequest('Invalid partOfTermEndDate');
                    }
                    $searchCourseSectionRequest->setPartOfTermEndDate($partOfTermEndDate[0], $partOfTermEndDate[1] ?? null);
                    break;
                case 'schedules_startDate':
                    try {
                        $scheduleStartDate = $this->getDateRange($value);
                    } catch (\InvalidArgumentException $e) {
                        throw new BadRequest('Invalid scheduleStartDate');
                    }
                    $searchCourseSectionRequest->setScheduleStartDate($scheduleStartDate[0], $scheduleStartDate[1] ?? null);
                    break;
                case 'schedules_endDate':
                    try {
                        $scheduleEndDate = $this->getDateRange($value);
                    } catch (\InvalidArgumentException $e) {
                        throw new BadRequest('Invalid scheduleEndDate');
                    }
                    $searchCourseSectionRequest->setScheduleEndDate($scheduleEndDate[0], $scheduleEndDate[1] ?? null);
                    break;
                case 'schedules_startTime':
                    try {
                        $scheduleStartTime = $this->getTimeRange($value);
                    } catch (\InvalidArgumentException $e) {
                        throw new BadRequest('Invalid scheduleStartTime');
                    }
                    $searchCourseSectionRequest->setScheduleStartTime($scheduleStartTime[0], $scheduleStartTime[1] ?? null);
                    break;
                case 'schedules_endTime':
                    try {
                        $scheduleEndTime = $this->getTimeRange($value);
                    } catch (\InvalidArgumentException $e) {
                        throw new BadRequest('Invalid scheduleEndTime');
                    }
                    $searchCourseSectionRequest->setScheduleEndTime($scheduleEndTime[0], $scheduleEndTime[1] ?? null);
                    break;
                case 'schedules_buildingCode':
                    $searchCourseSectionRequest->setScheduleBuildingCodes($value);
                    break;
                case 'schedules_roomNumber':
                    $searchCourseSectionRequest->setScheduleRooms($value);
                    break;
                case 'schedules_days':
                    $searchCourseSectionRequest->setScheduleDays($value);
                    break;
                case 'instructors_uniqueId':
                    $searchCourseSectionRequest->setInstructorUniqueIds($value);
                    break;
                case 'limit':
                    $searchCourseSectionRequest->setLimit($value);
                    break;
                case 'offset':
                    $searchCourseSectionRequest->setOffset($value);
                    break;
            }
        }

        return $searchCourseSectionRequest;
    }

    private function constructPayload(
        CourseSectionCollection $courseSectionCollection,
        array $subObjectList
    ): array {
        $courseSectionGuids = [];
        if (count($subObjectList) > 0) {
            $courseSectionGuids = $courseSectionCollection->map(
                function (CourseSection $courseSection) {
                    return (string)$courseSection->getGuid();
                }
            )->toArray();
        }

        $attributesLookup = [];
        if (isset($subObjectList['attributes'])) {
            $attributeCollection = $this->pike
                ->getViewCourseSectionAttributeService()
                ->getCollectionByCourseSectionGuids($courseSectionGuids);
            $attributesLookup = $this->getAttributesLookup($attributeCollection);
        }

        $schedulesLookup = [];
        if (isset($subObjectList['schedules'])) {
            $scheduleCollection = $this->pike
                ->getViewCourseSectionScheduleService()
                ->getCollectionByCourseSectionGuidCollection($courseSectionGuids);
            $schedulesLookup = $this->getSchedulesLookup($scheduleCollection);
        }

        $enrollmentCountLookup = [];
        if (isset($subObjectList['enrollmentCount'])) {
            $enrollmentCountCollection = $this->pike
                ->getViewCourseSectionService()
                ->getEnrollmentCountBysectionGuids($courseSectionGuids);
            $enrollmentCountLookup = $this->getEnrollmentCountLookup($enrollmentCountCollection);
        }

        $instructorLookup = [];
        if (isset($subObjectList['instructors'])) {
            $instructorAssignmentCollection = $this->pike
                ->getViewInstructorAssignmentService()
                ->getCollectionByCourseSectionGuids($courseSectionGuids);
            $uniqueIds = $instructorAssignmentCollection->map(function (
                InstructorAssignment $assignment
            ) {
                return (string)$assignment->getUniqueId();
            })->toArray();
            $personCollection = $this->pike
                ->getViewPersonService()
                ->getCollectionByUniqueIds($uniqueIds);

            $instructorLookup = $this->getInstructorLookup(
                $instructorAssignmentCollection,
                $personCollection
            );
        }
        $crossListedCourseSectionsLookup = [];

        if (isset($subObjectList['crossListedCourseSections'])) {
            $crossListedCourseSectionCollection = $this->pike
                ->getViewCrossListedCourseSectionService()
                ->getCollectionByCourseSectionGuids($courseSectionGuids);
            $crossListedCourseSectionsLookup = $this->getCrossListedCourseSectionsLookup(
                $crossListedCourseSectionCollection
            );
        }

        $enrollmentDistributionsLookup = [];
        if (isset($subObjectList['enrollmentDistribution'])) {
            $enrollmentDistributionCollection = $this->pike
                ->getViewCourseSectionEnrollmentDistributionService()
                ->getCollectionByCourseSectionGuids($courseSectionGuids);
            $enrollmentDistributionsLookup = $this->getEnrollmentDistributionsLookup(
                $enrollmentDistributionCollection
            );
        }

        $payload = [];

        /**
         * @var CourseSection $courseSection
         * */
        foreach ($courseSectionCollection as $courseSection) {
            $courseSectionGuid = (string)$courseSection->getGuid();

            $payload[] = $this->constructCourseSectionWithComposedModels(
                $subObjectList,
                $courseSection,
                $enrollmentCountLookup[$courseSectionGuid] ?? null,
                $instructorLookup[$courseSectionGuid] ?? [],
                $attributesLookup[$courseSectionGuid] ?? null,
                $schedulesLookup[$courseSectionGuid] ?? null,
                $crossListedCourseSectionsLookup[$courseSectionGuid] ?? null,
                $enrollmentDistributionsLookup[$courseSectionGuid] ?? null
            );
        }

        return $payload;
    }

    private function getAttributesLookup(
        CourseSectionAttributeCollection $attributeCollection
    ): array {
        $attributesLookup = [];

        /**
         * @var CourseSectionAttribute $attribute
         * */
        foreach ($attributeCollection as $attribute) {
            $courseSectionGuid = (string)$attribute->getCourseSectionGuid();
            if (!isset($attributesLookup[$courseSectionGuid])) {
                $attributesLookup[$courseSectionGuid] = new CourseSectionAttributeCollection();
            }
            $attributesLookup[$courseSectionGuid]->push($attribute);
        }

        return $attributesLookup;
    }

    private function getSchedulesLookup(
        CourseSectionScheduleCollection $scheduleCollection
    ): array {
        $schedulesLookup = [];

        /**
         * @var CourseSectionSchedule $schedule
         * */
        foreach ($scheduleCollection as $schedule) {
            $courseSectionGuid = (string)$schedule->getCourseSectionGuid();
            if (!isset($schedulesLookup[$courseSectionGuid])) {
                $schedulesLookup[$courseSectionGuid] = new CourseSectionScheduleCollection();
            }
            $schedulesLookup[$courseSectionGuid]->push($schedule);
        }

        return $schedulesLookup;
    }

    private function getEnrollmentCountLookup(
        CourseSectionEnrollmentCountCollection $enrollmentCountCollection
    ): array {
        $enrollmentCountLookup = [];

        /**
         * @var CourseSectionEnrollmentCount $enrollmentCount
         * */
        foreach ($enrollmentCountCollection as $enrollmentCount) {
            $courseSectionGuid = (string)$enrollmentCount->getCourseSectionGuid();

            $enrollmentCountLookup[$courseSectionGuid] = $enrollmentCount;
        }

        return $enrollmentCountLookup;
    }

    private function getCrossListedCourseSectionsLookup(
        CrossListedCourseSectionCollection $crossListedCourseSectionCollection
    ): array {
        $crossListedCourseSectionsLookup = [];

        /**
         * @var CrossListedCourseSection $crossListedCourseSection
         * */
        foreach ($crossListedCourseSectionCollection as $crossListedCourseSection) {
            $hostCourseSectionGuid = (string)$crossListedCourseSection->getHostCourseSectionGuid();
            if (!isset($crossListedCourseSectionsLookup[$hostCourseSectionGuid])) {
                $crossListedCourseSectionsLookup[$hostCourseSectionGuid] = new $crossListedCourseSectionCollection();
            }
            $crossListedCourseSectionsLookup[$hostCourseSectionGuid]->push($crossListedCourseSection);
        }
        return $crossListedCourseSectionsLookup;
    }


    private function getEnrollmentDistributionsLookup(
        CourseSectionEnrollmentDistributionCollection $courseSectionEnrollmentDistributionCollection
    ): array {
        $courseSectionEnrollmentDistributionsLookup = [];

        /**
         * @var CourseSectionEnrollmentDistribution $courseSectionEnrollmentDistribution
         * */
        foreach ($courseSectionEnrollmentDistributionCollection as $courseSectionEnrollmentDistribution) {
            $hostCourseSectionGuid = (string)$courseSectionEnrollmentDistribution->getCourseSectionGuid();
            if (!isset($courseSectionEnrollmentDistributionsLookup[$hostCourseSectionGuid])) {
                $courseSectionEnrollmentDistributionsLookup[$hostCourseSectionGuid] = $courseSectionEnrollmentDistribution;
            }
        }
        return $courseSectionEnrollmentDistributionsLookup;
    }

    private function constructCourseSectionWithComposedModels(
        array $subObjectList,
        CourseSection $courseSection,
        CourseSectionEnrollmentCount $enrollmentCount = null,
        array $instructors = [],
        CourseSectionAttributeCollection $attributeCollection = null,
        CourseSectionScheduleCollection $scheduleCollection = null,
        CrossListedCourseSectionCollection $crossListedCourseCollection = null,
        $distributionCollection = null
    ): array {
        $data = $this->courseSectionToArray($courseSection);

        if (isset($subObjectList['enrollmentCount'])) {
            $data['enrollmentCount'] = $this->enrollmentCountToArray($enrollmentCount);
        }

        if (isset($subObjectList['instructors'])) {
            $data['instructors'] = $instructors;
        }

        if (isset($subObjectList['attributes'])) {
            $data['attributes'] = $this->attributeCollectionToArray($attributeCollection);
        }

        if (isset($subObjectList['schedules'])) {
            $data['schedules'] = $this->scheduleCollectionToArray($scheduleCollection);
        }

        if (isset($subObjectList['crossListedCourseSections'])) {
            $data['crossListedCourseSections'] = $this->crossListedCourseSectionCollectionToArray($crossListedCourseCollection);
        }

        if (isset($subObjectList['enrollmentDistribution'])) {
            $data['enrollmentDistribution'] = $this->enrollmentDistributionToArray($distributionCollection);
        }

        return $data;
    }

    private function courseSectionToArray(CourseSection $courseSection): array
    {
        return [
            'termCode' => (string)$courseSection->getTermCode(),
            'termDescription' => (string)$courseSection->getTermDescription(),
            'crn' => (string)$courseSection->getCrn(),
            'courseSectionGuid' => (string)$courseSection->getGuid(),
            'title' => $courseSection->getTitle(),
            'sectionName' => (string)$courseSection->getSectionName(),
            'instructionTypeCode' => (string)$courseSection->getInstructionalTypeCode(),
            'instructionTypeDescription' => (string)$courseSection->getInstructionalTypeDescription(),
            'courseSectionCode' => $courseSection->getSectionCode(),
            'courseSectionStatusCode' => (string)$courseSection->getCourseSectionStatusCode(),
            'courseSectionStatusDescription' => $courseSection->getCourseSectionStatusDescription(),
            'campusCode' => (string)$courseSection->getCampusCode(),
            'campusName' => $courseSection->getCampusDescription(),
            'creditHoursDescription' => $courseSection->getCreditHoursDescription(),
            'creditHoursLow' => $courseSection->getCreditHoursLow(),
            'creditHoursHigh' => $courseSection->getCreditHoursHigh(),
            'isMidtermGradeSubmissionAvailable' => $courseSection->isMidtermGradeSubmissionAvailable(),
            'isFinalGradeSubmissionAvailable' => $courseSection->isFinalGradeSubmissionAvailable(),
            'isFinalGradeRequired' => $courseSection->isFinalGradeRequired(),
            'standardizedDivisionCode' => (string)$courseSection->getStandardizedDivisionCode(),
            'standardizedDivisionName' => $courseSection->getStandardizedDivisionName(),
            'standardizedDepartmentCode' => (string)$courseSection->getStandardizedDepartmentCode(),
            'standardizedDepartmentName' => $courseSection->getStandardizedDepartmentName(),
            'legacyStandardizedDepartmentCode' => (string)$courseSection->getLegacyStandardizedDepartmentCode(),
            'legacyStandardizedDepartmentName' => $courseSection->getLegacyStandardizedDepartmentName(),
            'creditHoursAvailable' => $courseSection->getCreditHoursAvailable(),
            'isDisplayed' => $courseSection->isDisplayed(),
            'course' => array(
                'schoolCode' => $courseSection->getCourse()->getSchoolCode()->getValue(),
                'schoolName' => $courseSection->getCourse()->getSchoolName(),
                'departmentCode' => $courseSection->getCourse()->getDepartmentCode()->getValue(),
                'departmentName' => $courseSection->getCourse()->getDepartmentName(),
                'title' => $courseSection->getCourse()->getTitle(),
                'subjectCode' => $courseSection->getCourse()->getSubjectCode()->getValue(),
                'subjectDescription' => $courseSection->getCourse()->getSubjectDescription(),
                'number' => $courseSection->getCourse()->getNumber()->getValue(),
                'lectureHoursDescription' => $courseSection->getCourse()->getLectureHoursDescription(),
                'labHoursDescription' => $courseSection->getCourse()->getLabHoursDescription(),
                'creditHoursHigh' => $courseSection->getCourse()->getCreditHoursHigh(),
                'creditHoursLow' => $courseSection->getCourse()->getCreditHoursLow(),
                'lectureHoursHigh' => $courseSection->getCourse()->getLectureHoursHigh(),
                'lectureHoursLow' => $courseSection->getCourse()->getLectureHoursLow(),
                'labHoursHigh' => $courseSection->getCourse()->getLabHoursHigh(),
                'labHoursLow' => $courseSection->getCourse()->getLabHoursLow(),
                'description' => $courseSection->getCourse()->getDescription()
            ),
            'partOfTerm' => array(
                'code' => $courseSection->getPartOfTerm()->getCode()->getValue(),
                'name' => $courseSection->getPartOfTerm()->getDescription(),
                'startDate' => $courseSection->getPartOfTerm()->getStartDate()->toDateString(),
                'endDate' => $courseSection->getPartOfTerm()->getEndDate()->toDateString()
            )
        ];
    }

    private function enrollmentCountToArray(
        CourseSectionEnrollmentCount $enrollmentCount
    ): array {
        return [
            'numberOfMax' => $enrollmentCount->getNumOfMaxEnrollments(),
            'numberOfCurrent' => $enrollmentCount->getNumOfCurrentEnrollments(),
            'numberOfActive' => $enrollmentCount->getNumOfActiveEnrollments(),
            'numberOfAvailable' => $enrollmentCount->getNumOfAvailableEnrollments()
        ];
    }

    private function attributeCollectionToArray(
        ?CourseSectionAttributeCollection $attributeCollection
    ): array {
        if ($attributeCollection === null) {
            return [];
        }

        $attributeArray = [];

        /**
         * @var CourseSectionAttribute $attribute
         * */
        foreach ($attributeCollection as $attribute) {
            $attributeArray[] = $this->attributeToArray($attribute);
        }

        return $attributeArray;
    }

    private function attributeToArray(CourseSectionAttribute $attribute): array
    {
        return [
            'code' => (string)$attribute->getCode(),
            'description' => $attribute->getDescription()
        ];
    }

    private function scheduleCollectionToArray(
        ?CourseSectionScheduleCollection $scheduleCollection
    ): array {
        if ($scheduleCollection === null) {
            return [];
        }

        $scheduleArray = [];

        /**
         * @var CourseSectionSchedule $schedule
         */
        foreach ($scheduleCollection as $schedule) {
            $scheduleArray[] = $this->scheduleToArray($schedule);
        }

        return $scheduleArray;
    }

    private function scheduleToArray(CourseSectionSchedule $schedule): array
    {
        $startTime = $schedule->getStartTime();
        $endTime = $schedule->getEndTime();
        $buildingCode = $schedule->getBuildingCode();
        $scheduleTypeCode = $schedule->getTypeCode();
        $days = $schedule->getDays();

        if ($startTime !== null) {
            $startTime = $startTime->format('H:i');
        }

        if ($endTime !== null) {
            $endTime = $endTime->format('H:i');
        }

        if ($buildingCode !== null) {
            $buildingCode = (string)$buildingCode;
        }

        if ($days !== null) {
            $days = (string)$days;
        }

        if ($scheduleTypeCode !== null) {
            $scheduleTypeCode = (string)$scheduleTypeCode;
        }

        return [
            'startDate' => $schedule->getStartDate()->format('Y-m-d'),
            'endDate' => $schedule->getEndDate()->format('Y-m-d'),
            'startTime' => $startTime,
            'endTime' => $endTime,
            'roomNumber' => $schedule->getRoom(),
            'buildingCode' => $buildingCode,
            'buildingName' => $schedule->getBuildingName(),
            'days' => $days,
            'scheduleTypeCode' => $scheduleTypeCode,
            'scheduleTypeDescription' => $schedule->getTypeDescription(),
        ];
    }

    private function crossListedCourseSectionCollectionToArray(
        ?CrossListedCourseSectionCollection $crossListedCourseCollection
    ): array {
        if ($crossListedCourseCollection === null) {
            return [];
        }

        $crossListedCourseSectionArray = [];

        /**
         * @var CrossListedCourseSection $crossListedCourse
         */
        foreach ($crossListedCourseCollection as $crossListedCourse) {
            $crossListedCourseSectionArray[] = $this->crossListedCourseSectionToArray($crossListedCourse);
        }

        return $crossListedCourseSectionArray;
    }

    private function courseSectionLevelDistributionToArray(
        CourseSectionLevelDistribution $courseSectionLevelDistribution
    ): array {
        return [
            'code' => $courseSectionLevelDistribution->getCode(),
            'description' => $courseSectionLevelDistribution->getDescription(),
            'numberOfCurrentEnrollment' => $courseSectionLevelDistribution->getNumberOfCurrentEnrollment(),
            'numberOfActiveEnrollment' => $courseSectionLevelDistribution->getNumberOfActiveEnrollment(),

        ];
    }

    private function courseSectionCreditHoursDistributionToArray(
        CourseSectionCreditHoursDistribution $courseSectionCreditHoursDistribution
    ): array {
        return [
            'hours' => $courseSectionCreditHoursDistribution->getCredit(),
            'numberOfCurrentEnrollment' => $courseSectionCreditHoursDistribution->getNumberOfCurrentEnrollment(),
            'numberOfActiveEnrollment' => $courseSectionCreditHoursDistribution->getNumberOfActiveEnrollment(),

        ];
    }

    private function courseSectionSummaryToArray(
        CourseSectionSummary $courseSectionSummary
    ): array {
        return [
            'description' => $courseSectionSummary->getDescription(),
            'numberOfCurrentEnrollment' => $courseSectionSummary->getNumberOfCurrentEnrollment(),
            'numberOfActiveEnrollment' => $courseSectionSummary->getNumberOfActiveEnrollment(),

        ];
    }


    private function crossListedCourseSectionToArray(
        CrossListedCourseSection $crossListedCourseSection
    ): array {
        return [
            'crn' => (string)$crossListedCourseSection->getCrn(),
            'courseSectionCode' => $crossListedCourseSection->getSectionCode(),
            'sectionName' => $crossListedCourseSection->getSectionName(),
            'courseSubjectCode' => (string)$crossListedCourseSection->getSubjectCode(),
            'courseNumber' => (string)$crossListedCourseSection->getCourseNumber(),
            'courseSectionGuid' => (string)$crossListedCourseSection->getCourseSectionGuid()
        ];
    }

    private function courseSectionLevelDistributionCollectionToArray(
        ?CourseSectionLevelDistributionCollection $courseSectionLevelDistributionCollection
    ): array {
        if ($courseSectionLevelDistributionCollection === null) {
            return [];
        }

        $courseSectionLevelDistributionArray = [];

        /**
         * @var CourseSectionLevelDistribution $courseSectionLevelDistribution
         */
        foreach ($courseSectionLevelDistributionCollection as $courseSectionLevelDistribution) {
            $courseSectionLevelDistributionArray[] = $this->courseSectionLevelDistributionToArray($courseSectionLevelDistribution);
        }

        return $courseSectionLevelDistributionArray;
    }


    private function courseSectionSummariesCollectionToArray(
        ?CourseSectionSummaryCollection $courseSectionSummaryCollection
    ): array {
        if ($courseSectionSummaryCollection === null) {
            return [];
        }

        $courseSectionSummaryDistributionArray = [];

        /**
         * @var CourseSectionSummary $courseSectionSummary
         */
        foreach ($courseSectionSummaryCollection as $courseSectionSummary) {
            $courseSectionSummaryDistributionArray[] = $this->courseSectionSummaryToArray($courseSectionSummary);
        }

        return $courseSectionSummaryDistributionArray;
    }

    private function courseSectionCreditHoursDistributionCollectionToArray(
        ?CourseSectionCreditHoursDistributionCollection $courseSectionCreditHoursDistributionCollection
    ): array {
        if ($courseSectionCreditHoursDistributionCollection === null) {
            return [];
        }

        $courseSectionCreditHoursDistributionArray = [];

        /**
         * @var CourseSectionCreditHoursDistribution $courseSectionCreditHoursDistribution
         */
        foreach ($courseSectionCreditHoursDistributionCollection as $courseSectionCreditHoursDistribution) {
            $courseSectionCreditHoursDistributionArray[] = $this->courseSectionCreditHoursDistributionToArray($courseSectionCreditHoursDistribution);
        }

        return $courseSectionCreditHoursDistributionArray;
    }

    private function enrollmentDistributionToArray(
        CourseSectionEnrollmentDistribution $courseSectionEnrollmentDistribution
    ): array {
        return [
            'guid' => (string)$courseSectionEnrollmentDistribution->getCourseSectionGuid(),
            'levelDistribution' => $this->courseSectionLevelDistributionCollectionToArray($courseSectionEnrollmentDistribution->getCourseSectionLevelDistributionCollection()),
            'creditHoursDistribution' => $this->courseSectionCreditHoursDistributionCollectionToArray($courseSectionEnrollmentDistribution->getCourseSectionCreditHoursDistributionCollection()),
            'summaries' => $this->courseSectionSummariesCollectionToArray($courseSectionEnrollmentDistribution->getCourseSectionSummaryCollection())
        ];
    }

    private function getInstructorLookup(
        InstructorAssignmentCollection $instructorAssignmentCollection,
        PersonCollection $personCollection
    ): array {
        $instructorLookup = [];
        $personLookup = [];

        /**
         * @var Person $person
         * */
        foreach ($personCollection as $person) {
            $uniqueId = (string)$person->getUniqueId();
            $personLookup[$uniqueId] = $person;
        }

        /**
         * @var InstructorAssignment $assignment
         * */
        foreach ($instructorAssignmentCollection as $assignment) {
            $courseSectionGuid = (string)$assignment->getCourseSectionGuid();
            $uniqueId = (string)$assignment->getUniqueId();
            if (!isset($instructorLookup[$courseSectionGuid])) {
                $instructorLookup[$courseSectionGuid] = [];
            }
            $instructorLookup[$courseSectionGuid][] = $this->assignmentToArray(
                $assignment,
                $personLookup[$uniqueId]
            );
        }

        return $instructorLookup;
    }

    private function personToArray(Person $person): array
    {
        return [
            'uniqueId' => (string)$person->getUniqueId(),
            'lastName' => $person->getLastName(),
            'firstName' => $person->getFirstName(),
            'middleName' => $person->getMiddleName(),
            'prefix' => $person->getPrefix(),
            'suffix' => $person->getSuffix(),
            'preferredFirstName' => $person->getPreferredFirstName(),
            'informalDisplayedName' => $person->getInformalDisplayedName(),
            'formalDisplayedName' => $person->getFormalDisplayedName(),
            'informalSortedName' => $person->getInformalSortedName(),
            'formalSortedName' => $person->getFormalSortedName(),
        ];
    }

    private function assignmentToArray(
        InstructorAssignment $assignment,
        Person $person
    ): array {
        return [
            'isPrimary' => $assignment->isPrimary(),
            'person' => $this->personToArray($person)
        ];
    }

    private function getDateRange(string $value): array
    {
        $dateRange = [];
        $dateRange = is_string($value) ? explode('to', $value) : $value;
        $dateRange[0] = Carbon::createFromFormat('Y-m-d', trim($dateRange[0]));
        $dateRange[0]->setTime(0, 0, 0);
        if (count($dateRange) > 1) {
            $dateRange[1] = Carbon::createFromFormat('Y-m-d', trim($dateRange[1]));
            $dateRange[1]->setTime(0, 0, 0);
        }
        return $dateRange;
    }

    private function getTimeRange(string $value): array
    {
        $timeRange = [];
        $timeRange = is_string($value) ? explode('to', $value) : $value;
        $timeRange[0] = Carbon::createFromFormat('H:i', trim($timeRange[0]));
        if (count($timeRange) > 1) {
            $timeRange[1] = Carbon::createFromFormat('H:i', trim($timeRange[1]));
        }
        return $timeRange;
    }

    private function getCountRange(string $value): array
    {
        $countRange = [];
        $countRange = is_string($value) ? explode('to', $value) : $value;
        $countRange[0] = trim($countRange[0]);
        if (count($countRange) > 1) {
            $countRange[1] = trim($countRange[1]);
        }
        return $countRange;
    }
}
