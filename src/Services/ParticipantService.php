<?php
/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 5/9/18
 * Time: 1:32 PM
 */

namespace MiamiOH\CourseSectionWebService\Services;

use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Exception\NotFoundException;
use MiamiOH\Pike\Exception\Exception;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\RESTng\App;

class ParticipantService extends BaseCourseSectionService
{
    public function getParticipant()
    {
        $this->setup();

        // use current term code if termCode is not provided
        $termCode = $this->getQueryParameterValueByKey('termCode');
        if ($termCode === null) {
            $termCode = (array)$this->pike->getViewTermService()->getCurrentTerm()->getCode();
        }

        // crn is optional
        $crn = $this->getQueryParameterValueByKey('crn');
        if ($crn === null) {
            $crn = [];
        }

        // use role=all if role is not provided
        $role = $this->getQueryParameterValueByKey('role');
        if ($role === null || $role === ['student', 'instructor']) {
            $role = ['all'];
        }
        $this->validateRole($role);

        // uniqueID is optional
        $uniqueId = $this->getQueryParameterValueByKey('uniqueId');
        if ($uniqueId === null) {
            $uniqueId = [];
        }

        $tokenUser = $this->getApiUser()->getUsername();

        $hasFullAccess = $this->hasFullAccess();
        $hasStandardAccess = $this->hasStandardAccess();

        try {
            $tokenUserInstructorAssignmentCollection = $this->pike
                ->getViewInstructorAssignmentService()
                ->getCollectionByTermCodesCrnsUniqeIds($termCode, $crn, array($tokenUser));

            $tokenUserCourseSectionEnrollmentCollection = $this->pike
                ->getViewCourseSectionEnrollmentService()
                ->getCollectionByTermCodesCrnsUniqeIds($termCode, $crn, array($tokenUser));

            $tokenUserInstructorAssignmentArray = $this->instructorAssignmentCollectionToArray($tokenUserInstructorAssignmentCollection);
            $tokenUserCourseSectionEnrollmentArray = $this->courseSectionEnrollmentCollectionToArray($tokenUserCourseSectionEnrollmentCollection);

            // get crns of all the courses taught are taken by token user if $uniqueid id null and $crn is empty
            if ($crn === [] && $uniqueId === []) {
                $uniqueId = [$tokenUser];
                $tokenUserTeachCrn = $this->getCrnAssociatedWithTokenUser(
                    $tokenUserInstructorAssignmentArray,
                    $tokenUser
                );
                $tokenUserTakenCrn = $this->getCrnAssociatedWithTokenUser(
                    $tokenUserCourseSectionEnrollmentArray,
                    $tokenUser
                );
                $crn = array_unique(array_merge($crn, $tokenUserTeachCrn, $tokenUserTakenCrn));
            }

            $instructorAssignmentCollection = $this->pike
                ->getViewInstructorAssignmentService()
                ->getCollectionByTermCodesCrnsUniqeIds($termCode, $crn, $uniqueId);
            $courseSectionEnrollmentCollection = $this->pike
                ->getViewCourseSectionEnrollmentService()
                ->getCollectionByTermCodesCrnsUniqeIds($termCode, $crn, $uniqueId);


            $instructorAssignmentArray = $this->instructorAssignmentCollectionToArray($instructorAssignmentCollection);
            $courseSectionEnrollmentArray = $this->courseSectionEnrollmentCollectionToArray($courseSectionEnrollmentCollection);

            // Get participant information based on the access levels
            if ($hasFullAccess) {
                $this->fullAccessResponse($role, $instructorAssignmentArray, $courseSectionEnrollmentArray);
            } elseif ($hasStandardAccess) {
                $this->standardAccessResponse(
                    $role,
                    $tokenUser,
                    $instructorAssignmentArray,
                    $courseSectionEnrollmentArray
                );
            } else {
                $this->tokenUserResponse(
                    $role,
                    $tokenUser,
                    $instructorAssignmentArray,
                    $courseSectionEnrollmentArray,
                    $tokenUserInstructorAssignmentArray,
                    $tokenUserCourseSectionEnrollmentArray
                );
            }
        } catch (InvalidArgumentException $e) {
            $this->response->setStatus(App::API_BADREQUEST);
            $this->response->setPayload([$e->getMessage()]);
        }

        return $this->response;
    }


    private function fullAccessResponse($role, $instructorAssignmentArray, $courseSectionEnrollmentArray)
    {
        if ($role === ['all']) {
            $this->mergeArrayToPayload($instructorAssignmentArray);
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        } elseif ($role === ['student']) {
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        } elseif ($role === ['instructor']) {
            $this->mergeArrayToPayload($instructorAssignmentArray);
        }
    }

    private function standardAccessResponse(
        $role,
        $tokenUser,
        $instructorAssignmentArray,
        $courseSectionEnrollmentArray
    ) {
        if ($role === ['all']) {
            $this->mergeArrayToPayload($instructorAssignmentArray);
            $courseSectionGuidArray = $this->getCourseSectionAssociatedWithTokenUser(
                $instructorAssignmentArray,
                $tokenUser
            );
            $courseSectionEnrollmentArray = $this->hideOtherStudentSensitiveInformation(
                $courseSectionEnrollmentArray,
                $tokenUser,
                $courseSectionGuidArray
            );
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        } elseif ($role === ['instructor']) {
            $this->mergeArrayToPayload($instructorAssignmentArray);
        } elseif ($role === ['student']) {
            $courseSectionGuidArray = $this->getCourseSectionAssociatedWithTokenUser(
                $instructorAssignmentArray,
                $tokenUser
            );
            $courseSectionEnrollmentArray = $this->hideOtherStudentSensitiveInformation(
                $courseSectionEnrollmentArray,
                $tokenUser,
                $courseSectionGuidArray
            );
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        }
    }

    private function tokenUserResponse(
        $role,
        $tokenUser,
        $instructorAssignmentArray,
        $courseSectionEnrollmentArray,
        $tokenUserInstructorAssignmentArray,
        $tokenUserCourseSectionEnrollmentArray
    ) {
        $tokenUserTeachCourseSections = $this->getCourseSectionAssociatedWithTokenUser(
            $tokenUserInstructorAssignmentArray,
            $tokenUser
        );
        $tokenUserTakenCourseSections = $this->getCourseSectionAssociatedWithTokenUser(
            $tokenUserCourseSectionEnrollmentArray,
            $tokenUser
        );
        if ($role === ['all']) {
            $instructorAssignmentArray = $this->hideOtherInstructorSensitiveInformation(
                $instructorAssignmentArray,
                $tokenUser,
                $tokenUserTakenCourseSections,
                $tokenUserTeachCourseSections
            );
            $this->mergeArrayToPayload($instructorAssignmentArray);
            $courseSectionEnrollmentArray = $this->hideOtherStudentSensitiveInformationForTokenUser(
                $courseSectionEnrollmentArray,
                $tokenUser,
                $tokenUserTakenCourseSections,
                $tokenUserTeachCourseSections
            );
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        } elseif ($role === ['instructor']) {
            $instructorAssignmentArray = $this->hideOtherInstructorSensitiveInformation(
                $instructorAssignmentArray,
                $tokenUser,
                $tokenUserTakenCourseSections,
                $tokenUserTeachCourseSections
            );
            $this->mergeArrayToPayload($instructorAssignmentArray);
        } elseif ($role === ['student']) {
            $courseSectionEnrollmentArray = $this->hideOtherStudentSensitiveInformationForTokenUser(
                $courseSectionEnrollmentArray,
                $tokenUser,
                $tokenUserTakenCourseSections,
                $tokenUserTeachCourseSections
            );
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        }
    }
}
