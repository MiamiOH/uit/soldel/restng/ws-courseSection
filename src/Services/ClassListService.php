<?php

namespace MiamiOH\CourseSectionWebService\Services;

use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\RESTng\App;

class ClassListService extends BaseCourseSectionService
{
    public function getClassList()
    {
        $this->setup();

        if (!$this->hasClassListAccess()) {
            $this->response->setStatus(App::API_UNAUTHORIZED);
            return $this->response;
        }

        // use current term code if termCode is not provided
        $termCode = $this->getQueryParameterValueByKey('termCode');
        if ($termCode === null) {
            $termCode = (array)$this->pike->getViewTermService()->getCurrentTerm()->getCode();
        }

        $uniqueId = $this->getQueryParameterValueByKey('uniqueId');

        try {
            $instructorAssignmentCollection = $this->pike
                ->getViewInstructorAssignmentService()
                ->getCollectionByTermCodesCrnsUniqeIds($termCode, [], $uniqueId);
            $courseSectionEnrollmentCollection = $this->pike
                ->getViewCourseSectionEnrollmentService()
                ->getCollectionByTermCodesCrnsUniqeIds($termCode, [], $uniqueId);


            $instructorAssignmentArray = $this->instructorAssignmentCollectionToArray($instructorAssignmentCollection);
            $courseSectionEnrollmentArray = $this->courseSectionEnrollmentCollectionToArray(
                $this->filterActiveEnrollments($courseSectionEnrollmentCollection)
            );

            $this->mergeArrayToPayload($instructorAssignmentArray);
            $this->mergeArrayToPayload($courseSectionEnrollmentArray);
        } catch (InvalidArgumentException $e) {
            $this->response->setStatus(App::API_BADREQUEST);
            $this->response->setPayload([$e->getMessage()]);
        }

        return $this->response;
    }

    private function filterActiveEnrollments(CourseSectionEnrollmentCollection $collection): CourseSectionEnrollmentCollection
    {
        return $collection->filter(function (CourseSectionEnrollment $enrollment) {
            return $enrollment->isEnrollmentActive();
        });
    }

    protected function courseSectionEnrollmentToArray(
        CourseSectionEnrollment $courseSectionEnrollment,
        CourseSection $courseSection
    ): array {
        return [
            'role' => 'student',
            'uniqueId' => (string) $courseSectionEnrollment->getUniqueId(),
            'termCode' => (string) $courseSection->getTermCode(),
            'crn' => (string) $courseSection->getCrn(),
            'courseSectionGuid' => (string) $courseSectionEnrollment->getCourseSectionGuid(),
            'title' => $courseSection->getCourseTitle(),
            'sectionCode' => $courseSection->getSectionCode(),
            'subjectCode' => (string) $courseSection->getCourse()->getSubjectCode(),
            'number' => (string) $courseSection->getCourse()->getNumber(),
            'sectionName' => $courseSection->getSectionName(),
            'instructionalTypeCode' => (string) $courseSection->getInstructionalTypeCode(),
            'instructionalType' => $courseSection->getInstructionalTypeDescription(),
            'campusCode' => (string) $courseSection->getCampusCode(),
            'campus' => $courseSection->getCampusDescription(),
        ];
    }

    protected function instructorAssignmentToArray(
        InstructorAssignment $instructorAssignment,
        CourseSection $courseSection
    ): array {
        return [
            'role' => 'instructor',
            'uniqueId' => (string) $instructorAssignment->getUniqueId(),
            'termCode' => (string) $courseSection->getTermCode(),
            'crn' => (string) $courseSection->getCrn(),
            'courseSectionGuid' => (string) $instructorAssignment->getCourseSectionGuid(),
            'title' => $courseSection->getCourseTitle(),
            'sectionCode' => $courseSection->getSectionCode(),
            'subjectCode' => (string) $courseSection->getCourse()->getSubjectCode(),
            'number' => (string) $courseSection->getCourse()->getNumber(),
            'sectionName' => $courseSection->getSectionName(),
            'instructionalTypeCode' => (string) $courseSection->getInstructionalTypeCode(),
            'instructionalType' => $courseSection->getInstructionalTypeDescription(),
            'campusCode' => (string) $courseSection->getCampusCode(),
            'campus' => $courseSection->getCampusDescription(),
        ];
    }
}
