<?php
/**
 * Author: liaom
 * Date: 5/1/18
 * Time: 10:45 AM
 */

namespace MiamiOH\CourseSectionWebService\Services;

use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;

class BaseCourseSectionService extends Service
{
    /**
     * @var string
     */
    protected $datasourceName = 'MUWS_GEN_PROD';

    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Response
     */
    protected $response;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var AppMapper
     */
    protected $pike;

    protected function setup()
    {
        $this->request = $this->getRequest();
        $this->response = $this->getResponse();
        $this->options = $this->request->getOptions();
    }

    public function setPikeServiceFactory(PikeServiceFactory $pikeServiceFactory)
    {
        $this->pike = $pikeServiceFactory->getEloquentPikeServiceMapper($this->datasourceName);
    }

    protected function setPayload(array $array)
    {
        $this->response->setPayload($array);
    }

    protected function mergeArrayToPayload(array $array)
    {
        $payload = $this->response->getPayload();
        $payload = array_merge($payload, $array);
        $this->setPayload($payload);
    }

    protected function instructorAssignmentToArray(
        InstructorAssignment $instructorAssignment,
        CourseSection $courseSection
    ): array {
        return [
            'termCode' => (string)$instructorAssignment->getTermCode(),
            'uniqueId' => (string)$instructorAssignment->getUniqueId(),
            'courseSectionGuid' => (string)$instructorAssignment->getCourseSectionGuid(),
            'studentLevel' => null,
            'creditHours' => null,
            'hasAttended' => null,
            'lastAttendDate' => null,
            'enrollmentStatusCode' => null,
            'enrollmentStatusDescription' => null,
            'isEnrollmentActive' => null,
            'isMidtermGradeRequired' => null,
            'isFinalGradeRequired' => null,
            'isGradeSubmissionEligible' => null,
            'gradeSubmissionEligibleComment' => null,
            'grades' => null,
            'role' => 'instructor',
            'crn' => (string)$courseSection->getCrn(),
            'title' => (string)$courseSection->getCourseTitle()
        ];
    }

    protected function instructorAssignmentCollectionToArray(
        InstructorAssignmentCollection $instructorAssignmentCollection
    ): array {
        $guids = $instructorAssignmentCollection->map(function (
            InstructorAssignment $instructorAssignment
        ) {
            return $instructorAssignment->getCourseSectionGuid()->getValue();
        })->toArray();

        $courseSectionCollection = new CourseSectionCollection();

        $guidsArr = array_chunk($guids, 1000, true);

        foreach ($guidsArr as $chunkArr) {
            $results = $this->pike->getViewCourseSectionService()->getByGuids($chunkArr);

            foreach ($results as $resultsDetail) {
                $courseSectionCollection->push($resultsDetail);
            }
        }
        $courseSectionMap = [];

        /**
         * @var CourseSection $courseSection
         * */
        foreach ($courseSectionCollection as $courseSection) {
            $courseSectionMap[$courseSection->getGuid()->getValue()] = $courseSection;
        }

        $data = [];
        /**
         * @var InstructorAssignment $instructorAssignment
         * */
        foreach ($instructorAssignmentCollection as $instructorAssignment) {
            $courseSectionGuid = $instructorAssignment->getCourseSectionGuid()->getValue();
            if (!isset($courseSectionMap[$courseSectionGuid])) {
                throw new \Exception(sprintf("Course section not found (GUID: %s).", $courseSectionGuid));
            }

            $data[] = $this->instructorAssignmentToArray(
                $instructorAssignment,
                $courseSectionMap[$courseSectionGuid]
            );
        }

        // remove duplicate instructor
        $dataMap = [];
        foreach ($data as $i => $singleData) {
            if (isset($dataMap[$singleData['courseSectionGuid']][$singleData['uniqueId']])) {
                unset($data[$i]);
            } else {
                $dataMap[$singleData['courseSectionGuid']] = [];
                $dataMap[$singleData['courseSectionGuid']][$singleData['uniqueId']] = true;
            }
        }

        return $data;
    }

    protected function courseSectionEnrollmentToArray(
        CourseSectionEnrollment $courseSectionEnrollment,
        CourseSection $courseSection
    ): array {
        $lastAttendDate = $courseSectionEnrollment->lastAttendDate();
        if ($lastAttendDate !== null) {
            $lastAttendDate = $lastAttendDate->format('Y-m-d');
        }

        return [
            'termCode' => (string)$courseSectionEnrollment->getTermCode(),
            'crn' => (string)$courseSection->getCrn(),
            'courseSectionGuid' => (string)$courseSectionEnrollment->getCourseSectionGuid(),
            'studentLevel' => (string)$courseSectionEnrollment->getStudentLevelCode(),
            'creditHours' => $courseSectionEnrollment->getCreditHours(),
            'hasAttended' => $courseSectionEnrollment->hasAttended(),
            'lastAttendDate' => $lastAttendDate,
            'enrollmentStatusCode' => $courseSectionEnrollment->getStatusCode(),
            'enrollmentStatusDescription' => $courseSectionEnrollment->getStatusDescription(),
            'isEnrollmentActive' => $courseSectionEnrollment->isEnrollmentActive(),
            'isMidtermGradeRequired' => $courseSectionEnrollment->isMidtermGradeRequired(),
            'isFinalGradeRequired' => $courseSectionEnrollment->isFinalGradeRequired(),
            'isGradeSubmissionEligible' => $courseSectionEnrollment->isGradeSubmissionEligible(),
            'gradeSubmissionEligibleComment' => $courseSectionEnrollment->getGradeSubmissionEligibleComment(),
            'grades' => $this->gradeCollectionToArray($courseSectionEnrollment->getGrades()),
            'uniqueId' => (string)($courseSectionEnrollment->getUniqueId()),
            'role' => 'student',

            'title' => (string)$courseSection->getCourseTitle()
        ];
    }


    protected function gradeToArray(Grade $grade)
    {
        return [
            'value' => (string)$grade->getValue(),
            'type' => (string)$grade->getType(),
            'gradeModeCode' => (string)$grade->getGradeModeCode()
        ];
    }

    protected function gradeCollectionToArray(GradeCollection $gradeCollection)
    {
        $data = [];

        foreach ($gradeCollection as $grade) {
            $data[] = $this->gradeToArray($grade);
        }

        return $data;
    }

    protected function courseSectionEnrollmentCollectionToArray(
        CourseSectionEnrollmentCollection $courseSectionEnrollmentCollection
    ): array {
        $guids = $courseSectionEnrollmentCollection->map(function (
            CourseSectionEnrollment $courseSectionEnrollment
        ) {
            return $courseSectionEnrollment->getCourseSectionGuid()->getValue();
        })->toArray();

        $courseSectionCollection = new CourseSectionCollection();

        $guidsArr = array_chunk($guids, 1000, true);

        foreach ($guidsArr as $chunkArr) {
            $results = $this->pike->getViewCourseSectionService()->getByGuids($chunkArr);

            foreach ($results as $resultsDetail) {
                $courseSectionCollection->push($resultsDetail);
            }
        }


        $courseSectionMap = [];

        /**
         * @var CourseSection $courseSection
         * */
        foreach ($courseSectionCollection as $courseSection) {
            $courseSectionMap[$courseSection->getGuid()->getValue()] = $courseSection;
        }

        $data = [];
        foreach ($courseSectionEnrollmentCollection as $courseSectionEnrollment) {
            $data[] = $this->courseSectionEnrollmentToArray(
                $courseSectionEnrollment,
                $courseSectionMap[$courseSectionEnrollment->getCourseSectionGuid()->getValue()]
            );
        }

        return $data;
    }

    protected function getQueryParameterValueByKey(
        string $key,
        bool $castToLowerCase = true
    ): ?array {
        if (isset($this->options[$key])) {
            $value = $this->options[$key];

            if ($castToLowerCase) {
                $value = array_map(function ($value) {
                    return strtolower(trim($value));
                }, $value);
            }

            return $value;
        }

        return null;
    }

    protected function validateRole(array $role)
    {
        if (sizeof(array_intersect($role, [
                'student',
                'instructor',
                'all'
            ])) === 0
        ) {
            throw new BadRequest('Invalid role');
        }
    }

    protected function hasFullAccess()
    {
        return $this->getApiUser()
            ->checkAuthorization(
                $this->buildAuthorizationConfigByKey('full')
            );
    }

    protected function hasStandardAccess()
    {
        return $this->getApiUser()
            ->checkAuthorization(
                $this->buildAuthorizationConfigByKey('standard')
            );
    }

    protected function hasClassListAccess()
    {
        return $this->getApiUser()
            ->checkAuthorization(
                $this->buildAuthorizationConfigByKey('class-list')
            );
    }

    protected function buildAuthorizationConfigByKey(string $key): array
    {
        return [
            'type' => 'authMan',
            'application' => 'WebServices',
            'module' => 'EnrollmentService',
            'key' => [$key]
        ];
    }

    protected function hideOtherStudentSensitiveInformation(
        array $data,
        string $tokenUser,
        array $courseSectionGuidArray
    ) {
        foreach ($data as &$singleData) {
            if (!in_array(
                $singleData['courseSectionGuid'],
                $courseSectionGuidArray
            ) && $singleData['uniqueId'] !== $tokenUser
            ) {
                unset($singleData['grades']);
                unset($singleData['creditHours']);
                unset($singleData['hasAttended']);
                unset($singleData['enrollmentStatusCode']);
                unset($singleData['enrollmentStatusDescription']);
                unset($singleData['isEnrollmentActive']);
                unset($singleData['isMidtermGradeRequired']);
                unset($singleData['isFinalGradeRequired']);
                unset($singleData['isGradeSubmissionEligible']);
                unset($singleData['gradeSubmissionEligibleComment']);
            }
        }

        return $data;
    }

    protected function hideOtherStudentSensitiveInformationForTokenUser(
        array $data,
        string $tokenUser,
        array $tokenUserTakenCourseSections,
        array $tokenUserTeachCourseSections
    ) {
        foreach ($data as $i => &$singleData) {
            if ($singleData['uniqueId'] !== $tokenUser && !in_array(
                $singleData['courseSectionGuid'],
                $tokenUserTeachCourseSections
            )
            ) {
                if (in_array(
                    $singleData['courseSectionGuid'],
                    $tokenUserTakenCourseSections
                )) {
                    unset($singleData['grades']);
                    unset($singleData['creditHours']);
                    unset($singleData['hasAttended']);
                    unset($singleData['enrollmentStatusCode']);
                    unset($singleData['enrollmentStatusDescription']);
                    unset($singleData['isEnrollmentActive']);
                    unset($singleData['isMidtermGradeRequired']);
                    unset($singleData['isFinalGradeRequired']);
                    unset($singleData['isGradeSubmissionEligible']);
                    unset($singleData['gradeSubmissionEligibleComment']);
                } else {
                    unset($data[$i]);
                }
            }
        }

        return $data;
    }

    protected function hideOtherInstructorSensitiveInformation(
        array $data,
        string $tokenUser,
        array $tokenUserTakenCourseSections,
        array $tokenUserTeachCourseSections
    ) {
        foreach ($data as $i => &$singleData) {
            if ($singleData['uniqueId'] !== $tokenUser && (
                !in_array(
                $singleData['courseSectionGuid'],
                $tokenUserTeachCourseSections
            ) && !in_array(
                            $singleData['courseSectionGuid'],
                            $tokenUserTakenCourseSections
                        )
            )
            ) {
                unset($data[$i]);
            }
        }

        return $data;
    }

    protected function getCourseSectionAssociatedWithTokenUser(
        array $data,
        $tokenUser
    ) {
        $crnAssociatedWithTokenUser = [];
        foreach ($data as &$singleData) {
            if ($singleData['uniqueId'] === $tokenUser) {
                array_push(
                    $crnAssociatedWithTokenUser,
                    $singleData['courseSectionGuid']
                );
            }
        }

        return $crnAssociatedWithTokenUser;
    }

    protected function getCrnAssociatedWithTokenUser(
        array $data,
        $tokenUser
    ) {
        $crnAssociatedWithTokenUser = [];
        foreach ($data as &$singleData) {
            if ($singleData['uniqueId'] === $tokenUser) {
                array_push(
                    $crnAssociatedWithTokenUser,
                    $singleData['crn']
                );
            }
        }

        return $crnAssociatedWithTokenUser;
    }
}
