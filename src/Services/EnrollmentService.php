<?php

namespace MiamiOH\CourseSectionWebService\Services;

use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionEnrollmentRequest;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;

class EnrollmentService extends Service
{
    /**
     * @var string
     */
    private $datasourceName = 'MUWS_SEC_PROD';
    /**
     * @var AppMapper
     */
    private $pike;

    public function setPikeServiceFactory(PikeServiceFactory $pikeServiceFactory)
    {
        $this->pike = $pikeServiceFactory->getEloquentPikeServiceMapper($this->datasourceName);
    }

    public function getEnrollment()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $offset = $request->getOffset();
        $limit = $request->getLimit();


        $termCodes = $options['termCodes'] ?? [];
        $crns = $options['crns'] ?? [];
        $uniqueIds = $options['uniqueIds'] ?? [];
        $status = $options['status'] ?? 'all';

        if (!in_array($status, ['active', 'inactive', 'all'])) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload(['message' => sprintf(
                'invalid status "%s". It must be "active", "inactive" or "all"',
                $status
            )]);
            return $response;
        }

        try {
            $searchRequest = $this->createSearchRequest($termCodes, $crns, $uniqueIds, $status, $limit, $offset);
        } catch (\InvalidArgumentException $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload(['message' => $e->getMessage()]);
            return $response;
        }

        $enrollments = $this->pike
            ->getViewCourseSectionEnrollmentService()
            ->search($searchRequest);

        $courseSectionMap = $this->getCourseSectionMapByGuids(new CourseSectionGuidCollection(
            $enrollments->map(function (CourseSectionEnrollment $enrollment) {
                return $enrollment->getCourseSectionGuid();
            })->toArray()
        ));

        $response->setTotalObjects($enrollments->getTotalNumOfItems());
        $response->setPayload($this->enrollmentCollectionToArray($enrollments, $courseSectionMap));
        $response->setStatus(App::API_OK);
        return $response;
    }

    private function createSearchRequest(
        array $termCodes,
        array $crns,
        array $uniqueIds,
        string $status,
        int $limit,
        int $offset
    ): SearchCourseSectionEnrollmentRequest {
        $searchRequest = new SearchCourseSectionEnrollmentRequest();

        if (count($termCodes) !== 0) {
            $searchRequest->setTermCodes($termCodes);
        }

        if (count($crns) !== 0) {
            $searchRequest->setCrns($crns);
        }

        if (count($uniqueIds) !== 0) {
            $searchRequest->setUniqueIds($uniqueIds);
        }

        if ($status === 'active') {
            $searchRequest->setIsActive(true);
        } elseif ($status === 'inactive') {
            $searchRequest->setIsActive(false);
        }

        $searchRequest->setOffset($offset);
        $searchRequest->setLimit($limit);

        return $searchRequest;
    }

    private function enrollmentCollectionToArray(CourseSectionEnrollmentCollection $enrollments, array $courseSectionMap): array
    {
        return $enrollments->map(function (CourseSectionEnrollment $enrollment) use ($courseSectionMap) {
            /** @var CourseSection|null $courseSection */
            $courseSection = $courseSectionMap[(string)$enrollment->getCourseSectionGuid()] ?? null;
            $crn = '';
            $title = '';

            if ($courseSection !== null) {
                $crn = $courseSection->getCrn()->getValue();
                $title = $courseSection->getCourseTitle();
            }

            $lastAttendDate = $enrollment->lastAttendDate();
            if ($lastAttendDate !== null) {
                $lastAttendDate = $lastAttendDate->format('Y-m-d');
            }

            return [
                'hasAttended' => $enrollment->hasAttended(),
                'lastAttendDate' => $lastAttendDate,
                'isEnrollmentActive' => $enrollment->isEnrollmentActive(),
                'isGradeSubmissionEligible' => $enrollment->isGradeSubmissionEligible(),
                'gradeSubmissionEligibleComment' => $enrollment->getGradeSubmissionEligibleComment(),
                'isFinalGradeRequired' => $enrollment->isFinalGradeRequired(),
                'isMidtermGradeRequired' => $enrollment->isMidtermGradeRequired(),
                'grades' => $this->gradeCollectionToArray($enrollment->getGrades()),
                'uniqueId' => (string)$enrollment->getUniqueId(),
                'enrollmentStatusCode' => $enrollment->getStatusCode(),
                'enrollmentStatusDescription' => $enrollment->getStatusDescription(),
                'courseSectionGuid' => (string)$enrollment->getCourseSectionGuid(),
                'creditHours' => $enrollment->getCreditHours(),
                'studentLevelCode' => (string)$enrollment->getStudentLevelCode(),
                'termCode' => (string)$enrollment->getTermCode(),
                'gradeModeCode' => (string)$enrollment->getGradeModeCode(),
                'crn' => $crn,
                'title' => $title
            ];
        })->toArray();
    }

    private function getCourseSectionMapByGuids(CourseSectionGuidCollection $guids): array
    {
        $chunks = $guids->chunk(200);

        $map = [];

        /** @var CourseSectionGuidCollection $chunk */
        foreach ($chunks as $chunk) {
            $courseSections = $this->pike->getViewCourseSectionService()
                ->getByGuids($chunk->toValueArray());

            /** @var CourseSection $courseSection */
            foreach ($courseSections as $courseSection) {
                $guid = $courseSection->getGuid()->getValue();
                $map[$guid] = $courseSection;
            }
        }

        return $map;
    }

    private function gradeCollectionToArray(GradeCollection $collection): array
    {
        return $collection->map(function (Grade $grade) {
            return [
                'type' => (string)$grade->getType(),
                'grade' => (string)$grade->getValue(),
                'gradeModeCode' => (string)$grade->getGradeModeCode()
            ];
        })->toArray();
    }
}
