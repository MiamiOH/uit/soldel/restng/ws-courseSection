<?php

namespace MiamiOH\CourseSectionWebService\Resources;

use MiamiOH\RESTng\App;

class EnrollmentResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    private $serviceName = 'CourseSectionEnrollmentService';
    private $tag = "courseSection";
    private $resourceRoot = "courseSection.v3.enrollment";
    private $patternRoot = "/courseSection/v3/enrollment";
    private $classPath = 'MiamiOH\CourseSectionWebService\Services\EnrollmentService';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CourseSection.Enrollment.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => ['type' => 'string']
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Enrollment.Grade',
            'type' => 'object',
            'properties' => array(
                'type' => ['type' => 'string'],
                'grade' => ['type' => 'string'],
                'gradeModeCode' => ['type' => 'string']
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Enrollment.Grade.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => 'CourseSection.Enrollment.Grade'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Enrollment',
            'type' => 'object',
            'properties' => array(
                'hasAttended' => ['type' => 'boolean'],
                'lastAttendDate' => ['type' => 'string'],
                'isEnrollmentActive' => ['type' => 'boolean'],
                'isGradeSubmissionEligible' => ['type' => 'boolean'],
                'gradeSubmissionEligibleComment' => ['type' => 'string'],
                'isFinalGradeRequired' => ['type' => 'string'],
                'isMidtermGradeRequired' => ['type' => 'string'],
                'grades' => [
                    'type' => 'array',
                    '$ref' => '#/definitions/CourseSection.Enrollment.Grade.Collection'
                ],
                'uniqueId' => ['type' => 'string'],
                'enrollmentStatusCode' => ['type' => 'string'],
                'enrollmentStatusDescription' => ['type' => 'string'],
                'courseSectionGuid' => ['type' => 'string'],
                'creditHours' => ['type' => 'float'],
                'studentLevelCode' => ['type' => 'string'],
                'termCode' => ['type' => 'string'],
                'gradeModeCode' => ['type' => 'string'],
                'crn' => ['type' => 'string'],
                'title' => ['type' => 'string']
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Enrollment.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => 'CourseSection.Enrollment'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'Course Section Enrollment Service',
            'set' => array(
                'pikeServiceFactory' => array(
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ),
            ),
        ));
    }

    public function registerResources(): void
    {

        //Resource: GET

        $this->addResource(
            [
                'action' => 'read',
                'name' => $this->resourceRoot,
                'description' => "- In order to consuming this API, user must have `full` access in the AuthMan application `WebServices` and module `EnrollmentService`.\n- Default page size of this resource is 10 (unless `limit` is specified).\n- The difference between this resource and `participant` resource is that `participant` resource returns both student information and instructor information, while `enrollment` service only focus on students' enrollment data.\n- Setting `limit` too large is strongly not recommended in order to avoiding slow response.",
                'summary' => 'Get students\' course enrollment data',
                'pattern' => $this->patternRoot,
                'service' => $this->serviceName,
                'method' => 'getEnrollment',
                'isPageable' => true,
                'defaultPageLimit' => 10,
                'tags' => [$this->tag],
                'params' => [],
                'options' => [
                    'termCodes' => [
                        'description' => 'Term code',
                        'type' => 'list'
                    ],
                    'crns' => [
                        'description' => 'Course section crn',
                        'type' => 'list'
                    ],
                    'uniqueIds' => [
                        'description' => 'Unique id',
                        'type' => 'list'
                    ],
                    'status' => [
                        'description' => 'whether enrollment is active',
                        'type' => 'single',
                        'default' => 'all',
                        'enum' => [
                            'all',
                            'active',
                            'inactive'
                        ]
                    ]
                ],
                'middleware' => [
                    'authenticate' => [],
                    'authorize' => array(
                        array(
                            'application' => 'WebServices',
                            'module' => 'EnrollmentService',
                            'key' => 'full'
                        ),
                    )
                ],
                'responses' => [
                    App::API_OK => [
                        'description' => 'Success',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/CourseSection.Enrollment.Collection'
                        ],
                    ],
                    App::API_UNAUTHORIZED => [
                        'description' => 'Unauthorized',
                        'returns' => [],
                    ],
                    App::API_BADREQUEST => [
                        'description' => 'Bad request',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Enrollment.Exception',
                        ],
                    ],
                    App::API_FAILED => [
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Enrollment.Exception',
                        ],
                    ]
                ]
            ]
        );
    }

    public function registerOrmConnections(): void
    {
    }
}
