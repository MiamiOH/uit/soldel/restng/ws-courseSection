<?php

namespace MiamiOH\CourseSectionWebService\Resources;

use MiamiOH\RESTng\App;

class ClassListResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    private $serviceName = 'ClassListService';
    private $tag = "courseSection";
    private $resourceRoot = "courseSection.v3.classList";
    private $patternRoot = "/courseSection/v3/classList";
    private $classPath = 'MiamiOH\CourseSectionWebService\Services\ClassListService';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Participant.ClassListSection',
            'type' => 'object',
            'properties' => array(
                'role' => ['type' => 'string'],
                'uniqueId' => ['type' => 'string'],
                'termCode' => ['type' => 'string'],
                'crn' => ['type' => 'string'],
                'courseSectionGuid' => ['type' => 'string'],
                'title' => ['type' => 'string'],
                'sectionCode' => ['type' => 'string'],
                'subjectCode' => ['type' => 'string'],
                'number' => ['type' => 'string'],
                'sectionName' => ['type' => 'string'],
                'instructionalTypeCode' => ['type' => 'string'],
                'instructionalType' => ['type' => 'string'],
                'campusCode' => ['type' => 'string'],
                'campus' => ['type' => 'string'],
            )
        ));

        $this->addDefinition(array(
            'name' => 'Participant.ClassList',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Participant.ClassListSection'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Participant.ClassList.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'Participant Class List Service',
            'set' => array(
                'pikeServiceFactory' => array(
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ),
            ),
        ));
    }

    public function registerResources(): void
    {

        //Resource: GET

        $this->addResource(
            [
                'action' => 'read',
                'name' => $this->resourceRoot . '.get',
                'description' => "##### Description:\n- Defaults to current term if term code is not provided.\n- Lookup token user's class list if unique id is not provided.\n
                      \n##### Authentication:\nConsumer must provide a valid Miami WS token
                      \n##### Authorization:
                      \n- Contact Application Operations to add your UniqueID with key `class-list` into AuthMan (application: `WebServices`, module: `EnrollmentService`).",
                'summary' => 'Get a person\'s class list for the given term',
                'pattern' => $this->patternRoot,
                'service' => $this->serviceName,
                'method' => 'getClassList',
                'isPageable' => false,
                'tags' => [$this->tag],
                'params' => [],
                'options' => [
                    'termCode' => [
                        'description' => 'TermCode',
                        'type' => 'list'
                    ],
                    'uniqueId' => [
                        'description' => 'UniqueId',
                        'type' => 'list'
                    ],
                ],
                'middleware' => [
                    'authenticate' => []
                ],
                'responses' => [
                    App::API_OK => [
                        'description' => 'A collection of  participants data based on the list of filters passed',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/Participant.ClassList'
                        ],
                    ],
                    App::API_BADREQUEST => [
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/Participant.ClassList.Exception'
                        ],
                    ],
                    App::API_UNAUTHORIZED => [
                        'description' => 'The user could not be authenticated or is not authorized to access the resource.',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/Participant.ClassList.Exception',
                        ],
                    ],
                    App::API_FAILED => [
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/Participant.ClassList.Exception',
                        ],
                    ],
                ]
            ]
        );
    }

    public function registerOrmConnections(): void
    {
    }
}
