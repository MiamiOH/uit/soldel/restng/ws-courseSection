<?php

namespace MiamiOH\CourseSectionWebService\Resources;

use MiamiOH\RESTng\App;

class ParticipantResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    private $serviceName = 'ParticipantService';
    private $tag = "courseSection";
    private $resourceRoot = "courseSection.v3.participant";
    private $patternRoot = "/courseSection/v3/participant";
    private $classPath = 'MiamiOH\CourseSectionWebService\Services\ParticipantService';

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Participant.CourseSectionEnrollment',
            'type' => 'object',
            'properties' => array(
                'termCode' => ['type' => 'string'],
                'courseSectionGuid' => ['type' => 'string'],
                'studentLevel' => ['type' => 'string'],
                'creditHours' => ['type' => 'float'],
                'hasAttended' => ['type' => 'boolean'],
                'lastAttendDate' => ['type' => 'string'],
                'enrollmentStatusCode' => ['type' => 'string'],
                'enrollmentStatusDescription' => ['type' => 'string'],
                'isEnrollmentActive' => ['type' => 'boolean'],
                'isMidtermGradeRequired' => ['type' => 'boolean'],
                'isFinalGradeRequired' => ['type' => 'boolean'],
                'isGradeSubmissionEligible' => ['type' => 'boolean'],
                'gradeSubmissionEligibleComment' => ['type' => 'string'],
                'midtermGrade' => ['type' => 'string'],
                'finalGrade' => ['type' => 'string'],
                'uniqueId' => ['type' => 'string'],
                'role' => ['type' => 'string'],
                'student' => ['type' => 'string'],
                'crn' => ['type' => 'string'],
                'title' => ['type' => 'string'],
            )
        ));

        $this->addDefinition(array(
            'name' => 'Participant.InstructorAssignment',
            'type' => 'object',
            'properties' => array(
                'termCode' => ['type' => 'string'],
                'uniqueId' => ['type' => 'string'],
                'courseSectionGuid' => ['type' => 'string'],
                'role' => ['type' => 'string'],
                'crn' => ['type' => 'string'],
                'title' => ['type' => 'string'],
            )
        ));

        $this->addDefinition(array(
            'name' => 'Participant.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Participant.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Participant.CourseSectionEnrollment'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'My Participant Service',
            'set' => array(
                'pikeServiceFactory' => array(
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ),
            ),
        ));
    }

    public function registerResources(): void
    {

        //Resource: GET

        $this->addResource(
            [
                'action' => 'read',
                'name' => $this->resourceRoot . '.all.get',
                'description' => "##### Description:\n- Defaults to current term if term code is not provided.\n- Lookup token user's courses if neither unique id nor CRN is not provided.\n- Retrieved participant information is based on access levels, special permission (full, standard) is required to lookup other's courses. Person with full access can lookup all data. Person with standard access can lookup all data without sensitive information (e.g. grade).
                      \n##### Authentication:\nConsumer must provide a valid Miami WS token
                      \n##### Authorization:
                      \n- Contact Solution Delivery Support to add your UniqueID with key `full` or `standard` into AuthMan (application: `WebServices`, module: `EnrollmentService`).",
                'summary' => 'Get a collection of participants information based on the filters termcode, crn, uniqueId, role and access levels',
                'pattern' => $this->patternRoot,
                'service' => $this->serviceName,
                'method' => 'getParticipant',
                'isPageable' => false,
                'tags' => [$this->tag],
                'params' => [],
                'options' => [
                    'termCode' => [
                        'description' => 'Term code',
                        'type' => 'list'
                    ],
                    'crn' => [
                        'description' => 'Course section crn',
                        'type' => 'list'
                    ],
                    'uniqueId' => [
                        'description' => 'Unique id',
                        'type' => 'list'
                    ],
                    'role' => [
                        'description' => 'People\'s role',
                        'type' => 'list',
                        'enum' => [
                            'student',
                            'instructor'
                        ]
                    ]
                ],
                'middleware' => [
                    'authenticate' => []
                ],
                'responses' => [
                    App::API_OK => [
                        'description' => 'A collection of  participants data based on the list of filters passed',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/Participant.Collection'
                        ],
                    ],
                    App::API_BADREQUEST => [
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/Participant.Exception'
                        ],
                    ],
                    App::API_UNAUTHORIZED => [
                        'description' => 'The user could not be authenticated or is not authorized to access the resource.',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/Participant.Exception',
                        ],
                    ],
                    App::API_FAILED => [
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/Participant.Exception',
                        ],
                    ],
                ]
            ]
        );
    }

    public function registerOrmConnections(): void
    {
    }
}
