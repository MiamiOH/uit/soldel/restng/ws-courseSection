<?php

namespace MiamiOH\CourseSectionWebService\Resources;

use MiamiOH\RESTng\App;

class CourseSectionResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    private $serviceName = 'CourseSectionService';
    private $tag = "courseSection";
    private $resourceRoot = "courseSection.v3";
    private $patternRoot = "/courseSection/v3";
    private $classPath = 'MiamiOH\CourseSectionWebService\Services\CourseSectionService';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'Course Section Service'
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.LevelDistribution',
            'type' => 'object',
            'properties' => array(
                'code' => array(
                    'type' => 'string',
                    'example' => '1'
                ),
                'description' => array(
                    'type' => 'string',
                    'example' => 'Freshman',
                ),
                'numberOfCurrentEnrollment' => array(
                    'type' => 'int',
                    'example' => 12,
                ),
                'numberOfActiveEnrollment' => array(
                    'type' => 'int',
                    'example' => 12,
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.LevelDistribution.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.LevelDistribution'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.CreditHoursDistribution',
            'type' => 'object',
            'properties' => array(
                'hours' => array(
                    'type' => 'float',
                    'example' => 1
                ),
                'numberOfCurrentEnrollment' => array(
                    'type' => 'int',
                    'example' => 12
                ),
                'numberOfActiveEnrollment' => array(
                    'type' => 'int',
                    'example' => 12
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.CreditHoursDistribution.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.CreditHoursDistribution'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Summaries',
            'type' => 'object',
            'properties' => array(
                'description' => array(
                    'type' => 'string',
                    'example' => "undergraduate"

                ),
                'numberOfCurrentEnrollment' => array(
                    'type' => 'int',
                    'example' => 12
                ),
                'numberOfActiveEnrollment' => array(
                    'type' => 'int',
                    'example' => 12
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Summaries.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.Summaries'
            )
        ));


        $this->addDefinition(array(
            'name' => 'CourseSection.EnrollmentDistribution',
            'type' => 'object',
            'properties' => array(
                'levelDistribution' => array(
                    '$ref' => '#/definitions/CourseSection.LevelDistribution.Collection',
                ),
                'creditHoursDistribution' => array(
                    '$ref' => '#/definitions/CourseSection.CreditHoursDistribution.Collection',
                ),
                'summaries' => array(
                    '$ref' => '#/definitions/CourseSection.Summaries.Collection',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection',
            'type' => 'object',
            'properties' => array(
                'termCode' => ['type' => 'string', 'example' => '201810'],
                'termDescription' => ['type' => 'string', 'example' => 'Fall Semetser 2018 -18'],
                'crn' => ['type' => 'string', 'example' => '72583'],
                'title' => ['type' => 'string', 'example' => 'CR Reading'],
                'courseSectionGuid' => ['type' => 'string', 'example' => '1cf6ded8-be1a-3327-1026-f65fafd37ad5'],
                'sectionName' => ['type' => 'string', 'example' => 'ACC 568 A'],
                'instructionalTypeCode' => ['type' => 'string', 'example' => 'L'],
                'instructionalTypeDescription' => ['type' => 'string', 'example' => 'Lecture'],
                'courseSectionCode' => ['type' => 'string', 'example' => 'A'],
                'courseSectionStatusCode' => ['type' => 'string', 'example' => 'A'],
                'courseSectionStatusDescription' => ['type' => 'string', 'example' => 'Active'],
                'campusCode' => ['type' => 'string', 'example' => 'O'],
                'campusName' => ['type' => 'string', 'example' => 'Oxford'],
                'creditHoursDescription' => ['type' => 'string', 'example' => '3'],
                'creditHoursLow' => ['type' => 'float', 'example' => 3],
                'creditHoursHigh' => ['type' => 'float', 'example' => 3],
                'lectureHours' => ['type' => 'string', 'example' => '3'],
                'labHours' => ['type' => 'string', 'example' => '0'],
                'standardizedDivisionCode' => ['type' => 'string', 'example' => 'FSB'],
                'standardizedDivisionName' => ['type' => 'string', 'example' => 'Farmer School of Business'],
                'standardizedDepartmentCode' => ['type' => 'string', 'example' => 'ACC'],
                'standardizedDepartmentName' => ['type' => 'string', 'example' => 'Accountancy'],
                'LegacyStandardizedDepartmentCode' => ['type' => 'string', 'example' => 'ACC'],
                'LegacyStandardizedDepartmentName' => ['type' => 'string', 'example' => 'Accountancy'],
                'creditHoursAvailable' => ['type' => 'list', 'example' => [3]],
                'enrollmentCount' => [
                    'type' => 'Object',
                    '$ref' => '#/definitions/CourseSection.EnrollmentCount'
                ],
                'partOfTerm' => [
                    'type' => 'object',
                    'properties' => array(
                        'code' => ['type' => 'string', 'example' => '1'],
                        'name' => ['type' => 'string', 'example' => 'Full Semester'],
                        'startDate' => ['type' => 'string', 'example' => '2017-08-28'],
                        'endDate' => ['type' => 'string', 'example' => '2017-12-16'],
                    )
                ],
                'isMidtermGradeSubmissionAvailable' => ['type' => 'boolean', 'example' => false],
                'isFinalGradeSubmissionAvailable' => ['type' => 'boolean', 'example' => false],
                'isFinalGradeRequried' => ['type' => 'boolean', 'example' => true],
                'isDisplayed' => ['type' => 'boolean', 'example' => true],
                'schedules' => [
                    'type' => 'array',
                    '$ref' => '#/definitions/CourseSection.Schedule.Collection'
                ],
                'instructors' => [
                    'type' => 'array',
                    '$ref' => '#/definitions/CourseSection.Instructors.Collection'
                ],
                'attributes' => [
                    'type' => 'array',
                    '$ref' => '#/definitions/CourseSection.Attributes.Collection'
                ],
                'crossListedCourseSections' => [
                    'type' => 'array',
                    '$ref' => '#/definitions/CourseSection.CrossListed.Collection'
                ],
                'enrollmentDistribution' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/CourseSection.EnrollmentDistribution'
                ],
                'course' => [
                    'type' => 'object',
                    'properties' => array(
                        'divisionCode' => ['type' => 'string', 'example' => 'BU'],
                        'divisionName' => ['type' => 'string', 'example' => 'Farmer School of Business'],
                        'departmentName' => ['type' => 'string', 'example' => 'Accountancy'],
                        'departmentCode' => ['type' => 'string', 'example' => 'ACC'],
                        'courseTitle' => ['type' => 'string', 'example' => 'Government and NFP Accounting'],
                        'courseSubjectCode' => ['type' => 'string', 'example' => 'ACC'],
                        'courseSubjectDescription' => ['type' => 'string', 'example' => 'Accountancy'],
                        'courseNumber' => ['type' => 'string', 'example' => '568'],
                        'creditHoursHigh' => ['type' => 'float', 'example' => 3],
                        'creditHoursLow' => ['type' => 'float', 'example' => 3],
                        'lectureHoursLow' => ['type' => 'float', 'example' => 3],
                        'lectureHoursHigh' => ['type' => 'float', 'example' => 3],
                        'labHoursLow' => ['type' => 'float', 'example' => 0],
                        'labHoursHigh' => ['type' => 'float', 'example' => 0],
                    )
                ],
                'courseDescription' => [
                    'type' => 'string',
                    'example' => '468/568 Accounting for Governmental and Not-for-Profit Organizations'
                ]
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Schedule.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.Schedule'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Schedule',
            'type' => 'object',
            'properties' => array(
                'startDate' => ['type' => 'string', 'example' => '2017-12-12'],
                'endDate' => ['type' => 'string', 'example' => '2017-12-12'],
                'startTime' => ['type' => 'string', 'example' => '15:00'],
                'endTime' => ['type' => 'string', 'example' => '17:00'],
                'roomNumber' => ['type' => 'string', 'example' => '0028'],
                'buildingCode' => ['type' => 'string', 'example' => 'FSB'],
                'buildingName' => ['type' => 'string', 'example' => 'Farmer School of Business'],
                'days' => ['type' => 'string', 'example' => 'T'],
                'scheduleTypeCode' => ['type' => 'string', 'example' => 'FEXM'],
                'scheduleTypeDescription' => ['type' => 'string', 'example' => 'Final Exam'],
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Instructors.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.Instructors'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Instructors',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => ['type' => 'string', 'example' => 'eighmeje'],
                'lastName' => ['type' => 'string', 'example' => 'Eighme'],
                'firstName' => ['type' => 'string', 'example' => 'Jan'],
                'middleName' => ['type' => 'string', 'example' => 'Ellen'],
                'prefix' => ['type' => 'string', 'example' => 'Dr.'],
                'suffix' => ['type' => 'string', 'example' => '(null)'],
                'preferredFirstName' => ['type' => 'string', 'example' => '(null)'],
                'informalDisplayedName' => ['type' => 'string', 'example' => 'Jan Ellen Eighme'],
                'formalDisplayedName' => ['type' => 'string', 'example' => 'Dr. Jan Ellen Eighme'],
                'informalSortedName' => ['type' => 'string', 'example' => 'Eighme, Jan Ellen'],
                'formalSortedName' => ['type' => 'string', 'example' => 'Eighme, Jan Ellen Dr.'],
                'isPrimary' => ['type' => 'boolean', 'example' => true],
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Attribute.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.Attributes'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.Attributes',
            'type' => 'object',
            'properties' => array(
                'code' => ['type' => 'string', 'example' => 'BUS'],
                'description' => ['type' => 'string', 'example' => 'Bus Course for Surcharge'],
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.CrossListed.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CourseSection.CrossListed'
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.CrossListed',
            'type' => 'object',
            'properties' => array(
                'crn' => ['type' => 'string', 'example' => '62583'],
                'sectionName' => ['type' => 'string', 'example' => 'ACC 668 B'],
                'sectionCode' => ['type' => 'string', 'example' => 'B'],
                'courseSectionGuid' => ['type' => 'string', 'example' => '11f6ded8-be1a-3327-1026-f65fafd37ad5'],
                'courseSubjectCode' => ['type' => 'string', 'example' => 'ACC'],
                'courseNumber' => ['type' => 'string', 'example' => '668'],
            )
        ));

        $this->addDefinition(array(
            'name' => 'CourseSection.EnrollmentCount',
            'type' => 'object',
            'properties' => array(
                'numberOfMax' => ['type' => 'string', 'example' => '16'],
                'numberOfCurrent' => ['type' => 'string', 'example' => '16'],
                'numberOfActive' => ['type' => 'string', 'example' => '16'],
                'numberOfAvailable' => ['type' => 'string', 'example' => '16'],
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'My Course Section Service',
            'set' => array(
                'pikeServiceFactory' => array(
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ),
            ),
        ));
    }

    public function registerResources(): void
    {
        // Resource GET CourseSection

        $this->addResource(
            [
                'action' => 'read',
                'name' => $this->resourceRoot . '.courseSection.get',
                'description' => "##### Description:\nThis resource provides get course section function to the consumer application. This returns a collection of course section info based on filters",
                'summary' => 'Get a collection of course sections',
                'pattern' => $this->patternRoot . '/courseSection',
                'service' => $this->serviceName,
                'method' => 'getCourseSection',
                'tags' => [$this->tag],
                'params' => [],
                'options' => [
                    'courseSectionGuid' => [
                        'description' => 'course section GUID, e.g. 870ef08b-1366-4531-804b-256f790521e0',
                        'type' => 'list',
                        'example' => 'asdfasdf'
                    ],
                    'campusCode' => [
                        'description' => 'campus code, e.g. O',
                        'type' => 'list'
                    ],
                    'termCode' => [
                        'description' => 'term code, e.g. 201710',
                        'type' => 'list'
                    ],
                    'crn' => [
                        'description' => 'course section crn, e.g. 70591',
                        'type' => 'list'
                    ],
                    'course_subjectCode' => [
                        'description' => 'subject code, e.g. CSE',
                        'type' => 'list'
                    ],
                    'courseSectionCode' => [
                        'description' => 'course section code, e.g. A',
                        'type' => 'list'
                    ],
                    'course_number' => [
                        'description' => 'course number, e.g. 101',
                        'type' => 'list'
                    ],
                    'courseSectionStatusCode' => [
                        'description' => 'course section status code, e.g. A',
                        'type' => 'list'
                    ],
                    'enrollmentCount_numberOfMax' => [
                        'description' => 'number of max enrollments, e.g. 200. In range, "to" separated. e.g. 200 to 300',
                        'type' => 'single'
                    ],
                    'enrollmentCount_numberOfCurrent' => [
                        'description' => 'number of current enrollments, e.g. 30. In range, "to" separated. e.g. 30 to 40',
                        'type' => 'single'
                    ],
                    'enrollmentCount_numberOfActive' => [
                        'description' => 'number of active enrollments, e.g. 30. In range, "to" separated. e.g. 30 to 40',
                        'type' => 'single'
                    ],
                    'enrollmentCount_numberOfAvailable' => [
                        'description' => 'number of available enrollments, e.g. 30. In range, "to" separated. e.g. 30 to 40',
                        'type' => 'single'
                    ],
                    'isMidtermGradeSubmissionAvailable' => [
                        'description' => 'whether midterm grade submission is available, e.g. true',
                        'type' => 'single',
                        'enum' => [true, false]
                    ],
                    'isFinalGradeSubmissionAvailable' => [
                        'description' => 'whether final grade submission is available, e.g. true',
                        'type' => 'single',
                        'enum' => [true, false]
                    ],
                    'hasSeatAvailable' => [
                        'description' => 'whether there is available seat in the section',
                        'type' => 'single',
                        'enum' => [true, false]
                    ],
                    'partOfTerm_code' => [
                        'description' => 'part-of-term code',
                        'type' => 'list'
                    ],
                    'partOfTerm_startDate' => [
                        'description' => 'part-of-term start date in YYYY-MM-DD format. Date in range, "to" separated. e.g. 2018-01-01 to 2018-010-30',
                        'type' => 'single'
                    ],
                    'partOfTerm_endDate' => [
                        'description' => 'part-of-term end date in YYYY-MM-DD format. Date in range, "to" separated. e.g. 2018-01-01 to 2018-010-30',
                        'type' => 'single'
                    ],
                    'schedules_startDate' => [
                        'description' => 'schedule start date in YYYY-MM-DD format. Date in range, "to" separated. e.g. 2018-01-01 to 2018-010-30',
                        'type' => 'single'
                    ],
                    'schedules_endDate' => [
                        'description' => 'schedule end date in YYYY-MM-DD format, Date in range, "to" separated. e.g. 2018-01-01 to 2018-010-30',
                        'type' => 'single'
                    ],
                    'schedules_startTime' => [
                        'description' => 'schedule start time in HH:ii format. Time in range, "to" separated. e.g. 10:01 to 15:30',
                        'type' => 'single'
                    ],
                    'schedules_endTime' => [
                        'description' => 'schedule end time in HH:ii format. Time in range, "to" separated. e.g. 10:01 to 15:30',
                        'type' => 'single'
                    ],
                    'schedules_buildingCode' => [
                        'description' => 'building code',
                        'type' => 'list'
                    ],
                    'schedules_roomNumber' => [
                        'description' => 'room number',
                        'type' => 'list'
                    ],
                    'schedules_days' => [
                        'description' => 'schedule days',
                        'type' => 'single'
                    ],
                    'instructors_uniqueId' => [
                        'description' => 'instructor\'s UniqueID',
                        'type' => 'list'
                    ],
                ],
                'middleware' => [],
                'isComposable' => true,
                'isPageable' => true,
                'defaultPageLimit' => 50,
                'maxPageLimit' => 200,
                'responses' => [
                    App::API_OK => [
                        'description' => 'A collection of  Course section data based on the list of filters passed',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/CourseSection.Collection'
                        ],
                    ],
                    App::API_BADREQUEST => [
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Exception',
                        ],
                    ],
                    App::API_FAILED => [
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Exception',
                        ],
                    ],
                ]
            ]
        );


        // Resource GET CourseSection Object by GUID

        $this->addResource(
            [
                'action' => 'read',
                'name' => $this->resourceRoot . '.courseSection.getByGuid',
                'description' => "##### Description:\nThis resource provides get course section function to the consumer application. This returns course section info by course section Guid.",
                'summary' => 'This returns course section info by course section Guid',
                'pattern' => $this->patternRoot . '/courseSection/:courseSectionGuid',
                'service' => $this->serviceName,
                'method' => 'getCourseSectionGuid',
                'tags' => [$this->tag],
                'params' => [
                    'courseSectionGuid' => [
                        'description' => 'Course section GuId',
                        'type' => 'single',
                        'required' => true
                    ],
                ],
                'options' => [],
                'middleware' => [],
                'isComposable' => true,
                'responses' => [
                    App::API_OK => [
                        'description' => 'A Course section object based on term code and CRN passed',
                        'returns' => [
                            'type' => 'model',
                            '$ref' => '#/definitions/CourseSection'
                        ],
                    ],
                    App::API_NOTFOUND => [
                        'description' => 'Course section not found',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Exception',
                        ],
                    ],
                    App::API_BADREQUEST => [
                        'description' => 'Server could not understand the request due to invalid syntax',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Exception',
                        ],
                    ],
                    App::API_FAILED => [
                        'description' => 'Server Error',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/CourseSection.Exception',
                        ],
                    ],
                ]
            ]
        );
    }

    public function registerOrmConnections(): void
    {
    }
}
