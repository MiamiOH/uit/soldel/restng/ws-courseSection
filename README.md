# Enrollment RESTful Web Service

## Description

ws-courseSection is implemented under RESTng framework and used to provide information of enrollment.
It was originally written in RestServer.

## API Resources

### Participant Web Services

#### Get /courseSection/v3/participant
Return a collection of participants information based on the filters termcode, crn, uniqueId, role and access levels.
Features:
 - Defaults to current term if term code is not provided.
 - Lookup token user's courses if neither unique id nor CRN is not provided.
 - Retrieved participant information is based on access levels, special permission (full, standard) is required to lookup other's courses. Person with full access can lookup all data. Person with standard access can lookup all data without sensitive information (e.g. grade).  (see [data model](./docs/data-models/course-section-participant.md))

##### Authentication:
Consumer must provide a valid Miami WS token.
##### Authorization:
Contact Solution Delivery Support to add your UniqueID with key full or standard into AuthMan (application: WebServices, module: EnrollmentService).

### Course Section Web Services
#### GET /courseSection/v3/courseSection
Return a collection of course sections restricted by specified filters. They are 
sorted by term code, subject code, course number and section code in ascending order.  
Features:
 - allow composing enrollmentCount, instructors, attributes, schedules, crossListedCourseSections and enrollmentDistribution models by using `compose` query parameter. (see [data model](./docs/data-models/core-course-section.md))
 - allow multiple filters. (see [available filters](./docs/filters.md))
 - allow pagination. (see [pagination documentation](./docs/pagination.md))
 - public access

#### GET /courseSection/v3/courseSection/{courseSectionGuid}
Return a course section with specified courseSectionGuid.  
Features:
 - allow composing enrollmentCount, instructors, attributes, schedules, crossListedCourseSections and enrollmentDistribution models by using `compose` query parameter. (see [data model](./docs/data-models/core-course-section.md))
 - public access

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`
3. make sure `pike` has installed as an addon of `restng`
4. make symlink in the `restng` folder (make sure environment variable `$RESTNG_HOME` is set)

```bash
ln -s <path_to_project>/src/RESTconfig $RESTNG_HOME/RESTconfig/courseSection
ln -s <path_to_project>/src/Service $RESTNG_HOME/Service/CourseSection
```

## Data Models
 - [CourseSection](./docs/data-models/core-course-section.md)

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.

## Errors & Resolutions

  - [Error: Course section not found (Guid: .....)](docs/resolution_course_section_guid_not_found.md) 