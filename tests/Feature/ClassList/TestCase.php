<?php

namespace MiamiOH\CourseSectionWebService\Tests\Feature\ClassList;

use Carbon\Carbon;
use MiamiOH\CourseSectionWebService\Services\ParticipantService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentStatus;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Grade;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface;
use PHPUnit\Framework\MockObject\MockObject;


class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $viewInstructorAssignmentService;

    /**
     * @var MockObject
     */
    protected $viewTermService;
    /**
     * @var MockObject
     */
    protected $authorizationValidator;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionEnrollmentService;

    /**
     * @var MockObject
     */
    protected $viewCourseSectionService;


    protected function setUp(): void
    {
        parent::setUp();
        $pikeServiceFactory = $this->createMock(PikeServiceFactory::class);

        $this->viewCourseSectionService = $this->createMock(ViewCourseSectionService::class);
        $this->viewTermService = $this->createMock(ViewTermService::class);
        $this->viewInstructorAssignmentService = $this->createMock(ViewInstructorAssignmentService::class);
        $this->viewCourseSectionEnrollmentService = $this->createMock(ViewCourseSectionEnrollmentService::class);


        $pike = $this->createMock(AppMapper::class);
        $pike->method('getViewCourseSectionService')->willReturn($this->viewCourseSectionService);
        $pike->method('getViewTermService')->willReturn($this->viewTermService);
        $pike->method('getViewInstructorAssignmentService')->willReturn($this->viewInstructorAssignmentService);
        $pike->method('getViewCourseSectionEnrollmentService')->willReturn($this->viewCourseSectionEnrollmentService);


        $pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($pike);

        $this->app->useService([
            'name' => 'PikeServiceFactory',
            'object' => $pikeServiceFactory,
            'description' => 'Mocked Pike Service Factory'
        ]);

        $this->authorizationValidator =
            $this->createMock(\MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface::class);

        $this->app->useService([
            'name' => 'AuthorizationValidator',
            'object' => $this->authorizationValidator,
            'description' => 'Mock authorization validator',
        ]);
    }

    protected function mockCourseSection(
        string $getGuid,
        Course $getCourse,
        string $getTermCode,
        string $getCrn,
        string $getCourseTitle,
        string $getTermDescription,
        string $getSectionCode,
        string $getCreditHoursDescription,
        float $getCreditHoursLow,
        float $getCreditHoursHigh,
        array $getCreditHoursAvailable,
        string $getInstructionalTypeCode,
        string $getInstructionalTypeDescription,
        string $getCampusCode,
        string $getCampusDescription,
        string $getCourseSectionStatusCode,
        string $getCourseSectionStatusDescription,
        float $getMaxNumberOfEnrollment,
        PartOfTerm $getPartOfTerm,
        string $getSectionName,
        bool $isMidtermGradeSubmissionAvailable,
        bool $isFinalGradeSubmissionAvailable,
        bool $isFinalGradeRequired,
        bool $isDisplayed,
        ?string $getStandardizedDivisionCode,
        ?string $getStandardizedDivisionName,
        ?string $getStandardizedDepartmentCode,
        ?string $getStandardizedDepartmentName,
        ?string $getLegacyStandardizedDepartmentCode,
        ?string $getLegacyStandardizedDepartmentName
    ): MockObject {
        $courseSection = $this->createMock(CourseSection::class);

        $courseSection->method('getGuid')->willReturn(new CourseSectionGuid($getGuid));
        $courseSection->method('getCourse')->willReturn($getCourse);
        $courseSection->method('getTermCode')->willReturn(new TermCode($getTermCode));
        $courseSection->method('getCrn')->willReturn(new Crn($getCrn));
        $courseSection->method('getCourseTitle')->willReturn($getCourseTitle);
        $courseSection->method('getTermDescription')->willReturn($getTermDescription);
        $courseSection->method('getSectionCode')->willReturn($getSectionCode);
        $courseSection->method('getCreditHoursDescription')->willReturn($getCreditHoursDescription);
        $courseSection->method('getCreditHoursLow')->willReturn($getCreditHoursLow);
        $courseSection->method('getCreditHoursHigh')->willReturn($getCreditHoursHigh);
        $courseSection->method('getCreditHoursAvailable')->willReturn($getCreditHoursAvailable);
        $courseSection->method('getInstructionalTypeCode')->willReturn(new InstructionalTypeCode($getInstructionalTypeCode));
        $courseSection->method('getInstructionalTypeDescription')->willReturn($getInstructionalTypeDescription);
        $courseSection->method('getCampusCode')->willReturn(new CampusCode($getCampusCode));
        $courseSection->method('getCampusDescription')->willReturn($getCampusDescription);
        $courseSection->method('getCourseSectionStatusCode')->willReturn(new CourseSectionStatusCode($getCourseSectionStatusCode));
        $courseSection->method('getCourseSectionStatusDescription')->willReturn($getCourseSectionStatusDescription);
        $courseSection->method('getMaxNumberOfEnrollment')->willReturn($getMaxNumberOfEnrollment);
        $courseSection->method('getPartOfTerm')->willReturn($getPartOfTerm);
        $courseSection->method('getSectionName')->willReturn($getSectionName);
        $courseSection->method('isMidtermGradeSubmissionAvailable')->willReturn($isMidtermGradeSubmissionAvailable);
        $courseSection->method('isFinalGradeSubmissionAvailable')->willReturn($isFinalGradeSubmissionAvailable);
        $courseSection->method('isFinalGradeRequired')->willReturn($isFinalGradeRequired);
        $courseSection->method('isDisplayed')->willReturn($isDisplayed);
        $courseSection->method('getStandardizedDivisionCode')->willReturn(new DivisionCode($getStandardizedDivisionCode));
        $courseSection->method('getStandardizedDivisionName')->willReturn($getStandardizedDivisionName);
        $courseSection->method('getStandardizedDepartmentCode')->willReturn(new DepartmentCode($getStandardizedDepartmentCode));
        $courseSection->method('getStandardizedDepartmentName')->willReturn($getStandardizedDepartmentName);
        $courseSection->method('getLegacyStandardizedDepartmentCode')->willReturn(new DepartmentCode($getLegacyStandardizedDepartmentCode));
        $courseSection->method('getLegacyStandardizedDepartmentName')->willReturn($getLegacyStandardizedDepartmentName);

        return $courseSection;
    }

    public function mockCourse(
        ?string $getLabHoursDescription,
        string $getTermCode,
        ?string $getLectureHoursDescription,
        ?string $getCreditHoursDescription,
        string $getGuid,
        string $getSubjectCode,
        string $getNumber,
        string $getTermEffectedCode,
        string $getTitle,
        ?string $getDescription,
        string $getSchoolCode,
        string $getSchoolName,
        string $getDepartmentCode,
        string $getDepartmentName,
        string $getSubjectDescription,
        ?float $getCreditHoursLow,
        ?float $getCreditHoursHigh,
        ?float $getLectureHoursLow,
        ?float $getLectureHoursHigh,
        ?float $getLabHoursLow,
        ?float $getLabHoursHigh,
        array $getCreditHoursAvailable
    ): MockObject {
        $obj = $this->createMock(Course::class);

        $obj->method('getLabHoursDescription')->willReturn($getLabHoursDescription);
        $obj->method('getTermCode')->willReturn(new TermCode($getTermCode));
        $obj->method('getLectureHoursDescription')->willReturn($getLectureHoursDescription);
        $obj->method('getCreditHoursDescription')->willReturn($getCreditHoursDescription);
        $obj->method('getGuid')->willReturn(new CourseGuid($getGuid));
        $obj->method('getSubjectCode')->willReturn(new SubjectCode($getSubjectCode));
        $obj->method('getNumber')->willReturn(new CourseNumber($getNumber));
        $obj->method('getTermEffectedCode')->willReturn(new TermCode($getTermEffectedCode));
        $obj->method('getTitle')->willReturn($getTitle);
        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getSchoolCode')->willReturn(new SchoolCode($getSchoolCode));
        $obj->method('getSchoolName')->willReturn($getSchoolName);
        $obj->method('getDepartmentCode')->willReturn(new DepartmentCode($getDepartmentCode));
        $obj->method('getDepartmentName')->willReturn($getDepartmentName);
        $obj->method('getSubjectDescription')->willReturn($getSubjectDescription);
        $obj->method('getCreditHoursLow')->willReturn($getCreditHoursLow);
        $obj->method('getCreditHoursHigh')->willReturn($getCreditHoursHigh);
        $obj->method('getLectureHoursLow')->willReturn($getLectureHoursLow);
        $obj->method('getLectureHoursHigh')->willReturn($getLectureHoursHigh);
        $obj->method('getLabHoursLow')->willReturn($getLabHoursLow);
        $obj->method('getLabHoursHigh')->willReturn($getLabHoursHigh);
        $obj->method('getCreditHoursAvailable')->willReturn($getCreditHoursAvailable);

        return $obj;
    }

    public function mockPartOfTerm(
        string $getCode,
        string $getDescription,
        Carbon $getStartDate,
        Carbon $getEndDate
    ): MockObject {
        $obj = $this->createMock(PartOfTerm::class);

        $obj->method('getCode')->willReturn(new PartOfTermCode($getCode));
        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getStartDate')->willReturn($getStartDate);
        $obj->method('getEndDate')->willReturn($getEndDate);

        return $obj;
    }

    public function mockTerm(
        string $code,
        string $description,
        Carbon $startDate,
        Carbon $endDate,
        bool $isDisplayed
    ): MockObject {
        $obj = $this->createMock(Term::class);

        $obj->method('getCode')->willReturn(new TermCode($code));
        $obj->method('getDescription')->willReturn($description);
        $obj->method('getStartDate')->willReturn($startDate);
        $obj->method('getEndDate')->willReturn($endDate);
        $obj->method('isDisplayed')->willReturn($isDisplayed);

        return $obj;
    }


    public function mockInstructorAssignment(
        string $getUniqueId,
        string $getTermCode,
        string $getCrn,
        string $getGuid,
        string $getCourseSectionGuid,
        bool $isPrimary
    ): MockObject {
        $obj = $this->createMock(InstructorAssignment::class);

        $obj->method('getUniqueId')->willReturn(new UniqueId($getUniqueId));
        $obj->method('getTermCode')->willReturn(new TermCode($getTermCode));
        $obj->method('getCrn')->willReturn(new Crn($getCrn));
        $obj->method('getGuid')->willReturn(new InstructorAssignmentGuid($getGuid));
        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));
        $obj->method('isPrimary')->willReturn($isPrimary);

        return $obj;
    }


    public function mockCourseSectionEnrollment(
        bool $hasAttended,
        bool $isEnrollmentActive,
        bool $isGradeSubmissionEligible,
        string $getGradeSubmissionEligibleComment,
        bool $isFinalGradeRequired,
        bool $isMidtermGradeRequired,
        GradeCollection $grades,
        string $uniqueId,
        string $statusCode,
        string $statusDesc,
        string $courseSectionGuid,
        float $creditHours,
        string $studentLevelCode,
        string $termCode,
        string $gradeModeCode,
        Carbon $lastAttendDate = null
    ): MockObject {
        $obj = $this->createMock(CourseSectionEnrollment::class);

        $obj->method('hasAttended')->willReturn($hasAttended);
        $obj->method('lastAttendDate')->willReturn($lastAttendDate);
        $obj->method('isEnrollmentActive')->willReturn($isEnrollmentActive);
        $obj->method('isGradeSubmissionEligible')->willReturn($isGradeSubmissionEligible);
        $obj->method('getGradeSubmissionEligibleComment')->willReturn($getGradeSubmissionEligibleComment);
        $obj->method('isFinalGradeRequired')->willReturn($isFinalGradeRequired);
        $obj->method('isMidtermGradeRequired')->willReturn($isMidtermGradeRequired);
        $obj->method('getGrades')->willReturn($grades);
        $obj->method('getStatusCode')->willReturn($statusCode);
        $obj->method('getStatusDescription')->willReturn($statusDesc);
        $obj->method('isEnrollmentActive')->willReturn($isEnrollmentActive);
        $obj->method('getUniqueId')->willReturn(new UniqueId($uniqueId));
        $obj->method('getCreditHours')->willReturn($creditHours);
        $obj->method('getTermCode')->willReturn(new TermCode($termCode));
        $obj->method('getStudentLevelCode')->willReturn(new StudentLevelCode($studentLevelCode));
        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($courseSectionGuid));
        $obj->method('getGradeModeCode')->willReturn(new GradeModeCode($gradeModeCode));

        return $obj;
    }

    public function mockGrade(
        string $value,
        string $gradeModeCode,
        string $type,
        string $courseSectionGuid,
        string $crn,
        string $termCode,
        string $uniqueId
    ): MockObject {
        $obj = $this->createMock(\MiamiOH\Pike\Domain\Model\Grade::class);

        $obj->method('getValue')->willReturn(new GradeValue($value));
        $obj->method('getGradeModeCode')->willReturn(new GradeModeCode($gradeModeCode));
        $obj->method('getType')->willReturn(new GradeType($type));
        $obj->method('getCourseSectionGuid')->willReturn($courseSectionGuid);
        $obj->method('getCrn')->willReturn($crn);
        $obj->method('getTermCode')->willReturn($termCode);
        $obj->method('getUniqueId')->willReturn($uniqueId);

        return $obj;
    }

    public function getMockedCourseSection(): CourseSectionCollection
    {
        return new CourseSectionCollection([
            $this->mockCourseSection(
                '387425cc-7fe8-44e9-9278-229cad6c064e',
                $this->mockCourse(
                    '1',
                    '201710',
                    '2',
                    '3',
                    '387425cc-7fe8-44e9-9278-229cad6c064e',
                    'BIO',
                    '631',
                    '201710',
                    'Conservation Science&Community',
                    '631 Conservation Science&Community',
                    'AS',
                    'College of Arts and Science',
                    'BIO',
                    'Biology',
                    'Biology',
                    2,
                    2,
                    2,
                    null,
                    1,
                    null,
                    [3]
                ),
                '201710',
                '70591',
                'Conservation Science&Community',
                'Fall Semester 2016-17',
                'B',
                '2',
                2,
                2,
                [3],
                'Z',
                'Lecture/Lab',
                'O',
                'Oxford',
                'A',
                'Active',
                28,
                $this->mockPartOfTerm(
                    '1',
                    'Full Semester',
                    Carbon::createFromFormat('Y-m-d', '2016-08-29'),
                    Carbon::createFromFormat('Y-m-d', '2016-12-17')
                ),
                'BIO 631 B',
                false,
                false,
                true,
                false,
                'CAS',
                'College of Arts & Science',
                'BIO',
                'Biology',
                'BIO',
                'Biology'
            )
        ]);
    }


    public function getMockedInstructor(): MockObject
    {
        return $this->mockInstructorAssignment(
            'myersca',
            '201710',
            '70591',
            '387425cc-7fe8-44e9-9278-229cad6c064e',
            '387425cc-7fe8-44e9-9278-229cad6c064e',
            true
        );
    }

    public function getMockedTerm(): MockObject
    {
        return $this->mockTerm(
            '201710',
            '',
            Carbon::createFromFormat('Y-m-d', '2016-08-29'),
            Carbon::createFromFormat('Y-m-d', '2016-12-17'),
            true
        );
    }

    public function getMockGrade1()
    {

        return $this->mockGrade(
            'A',
            'S',
            'midterm',
            '',
            '70591',
            '201710',
            'liaom'
        );

    }

    public function getMockGrade2()
    {

        return $this->mockGrade(
            'A+',
            'S',
            'final',
            '',
            '70591',
            '201710',
            'liaom'
        );

    }

    public function getMockedCourseSectionEnrollment(): MockObject
    {
        return $this->mockCourseSectionEnrollment(
            true,
            false,
            true,
            'Grade has already been rolled into academic history',
            true,
            true,
            new GradeCollection([
                $this->getMockGrade1(),
                $this->getMockGrade2()
            ]),
            'ashs',
            'RE',
            '',
            '387425cc-7fe8-44e9-9278-229cad6c064e',
            2,
            'GR',
            '201710',
            's'
        );
    }

    public function getMockedCourseSectionEnrollmentForTokenUser(): MockObject
    {
        return $this->mockCourseSectionEnrollment(
            true,
            false,
            true,
            'Grade has already been rolled into academic history',
            true,
            true,
            new GradeCollection([
                $this->getMockGrade1(),
                $this->getMockGrade2()
            ]),
            'liaom',
            'RE',
            '',
            '387425cc-7fe8-44e9-9278-229cad6c064e',
            2,
            'GR',
            '201710',
            's'
        );
    }

    public function getMockedCourseSectionParticipantResponse(): array
    {
        return [
            [
                "termCode" => "201710",
                "uniqueId" => "myersca",
                "courseSectionGuid" => "387425cc-7fe8-44e9-9278-229cad6c064e",
                "studentLevel" => null,
                "creditHours" => null,
                "hasAttended" => null,
                "lastAttendDate" => null,
                "enrollmentStatusCode" => null,
                "enrollmentStatusDescription" => null,
                "isEnrollmentActive" => null,
                "isMidtermGradeRequired" => null,
                "isFinalGradeRequired" => null,
                "isGradeSubmissionEligible" => null,
                "gradeSubmissionEligibleComment" => null,
                "grades" => null,
                "role" => "instructor",
                "crn" => "70591",
                "title" => "Conservation Science&Community"
            ],
            [
                "termCode" => "201710",
                "crn" => "70591",
                "courseSectionGuid" => "387425cc-7fe8-44e9-9278-229cad6c064e",
                "studentLevel" => "GR",
                "creditHours" => 2,
                "hasAttended" => true,
                "lastAttendDate" => null,
                "enrollmentStatusCode" => "RE",
                "enrollmentStatusDescription" => null,
                "isEnrollmentActive" => null,
                "isMidtermGradeRequired" => true,
                "isFinalGradeRequired" => true,
                "isGradeSubmissionEligible" => true,
                "gradeSubmissionEligibleComment" => "Grade has already been rolled into academic history",
                "grades" => [
                    [
                        "value" => "A",
                        "type" => "midterm",
                        "gradeModeCode" => "S"
                    ],
                    [
                        "value" => "A+",
                        "type" => "final",
                        "gradeModeCode" => "S"
                    ]
                ],
                "uniqueId" => "ashs",
                "role" => "student",
                "title" => "Conservation Science&Community"
            ]
        ];
    }


    public function getMockedCourseSectionParticipantStandaradAccessResponse(): array
    {
        return [
            [
                "termCode" => "201710",
                "uniqueId" => "myersca",
                "courseSectionGuid" => "387425cc-7fe8-44e9-9278-229cad6c064e",
                "studentLevel" => null,
                "creditHours" => null,
                "hasAttended" => null,
                "lastAttendDate" => null,
                "enrollmentStatusCode" => null,
                "enrollmentStatusDescription" => null,
                "isEnrollmentActive" => null,
                "isMidtermGradeRequired" => null,
                "isFinalGradeRequired" => null,
                "isGradeSubmissionEligible" => null,
                "gradeSubmissionEligibleComment" => null,
                "grades" => null,
                "role" => "instructor",
                "crn" => "70591",
                "title" => "Conservation Science&Community"
            ],
            [
                "termCode" => "201710",
                "crn" => "70591",
                "courseSectionGuid" => "387425cc-7fe8-44e9-9278-229cad6c064e",
                "studentLevel" => "GR",
                "uniqueId" => "ashs",
                "role" => "student",
                "title" => "Conservation Science&Community"
            ]
        ];
    }

    public function getMockedClassListResponse(): array
    {
        return [
            [
                'role' => 'instructor',
                'uniqueId' => 'myersca',
                'termCode' => '201710',
                'crn' => '70591',
                'courseSectionGuid' => '387425cc-7fe8-44e9-9278-229cad6c064e',
                'title' => 'Conservation Science&Community',
                'sectionCode' => 'B',
                'subjectCode' => 'BIO',
                'number' => '631',
                'sectionName' => 'BIO 631 B',
                'instructionalTypeCode' => 'Z',
                'instructionalType' => 'Lecture/Lab',
                'campusCode' => 'O',
                'campus' => 'Oxford',
            ]
        ];
    }

}