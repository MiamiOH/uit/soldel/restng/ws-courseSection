<?php

namespace MiamiOH\CourseSectionWebService\Tests\Feature\ClassList;


use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Exception\InvalidTermCodeException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationSpecification;
use MiamiOH\RESTng\Testing\UsesAuthentication;


class GetClassListTest extends TestCase
{
    use UsesAuthentication;

    public function testInvalidAuthentication()
    {
        $this->withToken('liaom')->willNotAuthenticateUser();

        $response = $this->getJson('/courseSection/v3/classList?termCode=201710&uniqueId=publicjq');

        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    public function testInvalidTermCode()
    {
        $this->withToken('liaom')->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'class-list'),
                'liaom'
            )
            ->willReturn(true);

        $this->viewInstructorAssignmentService->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willThrowException(new InvalidTermCodeException());

        $response = $this->getJson('/courseSection/v3/classList?termCode=2017ad10&uniqueId=publicjq');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testGetClassListWithDefaultTermCode()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollmentForTokenUser()
        ]);

        $mockedTerm = $this->getMockedTerm();

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'class-list'),
                'liaom'
            )
            ->willReturn(true);

        $this->viewTermService
            ->method('getCurrentTerm')
            ->willReturn($mockedTerm);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/classList?uniqueId=publicjq");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedClassListResponse()[0]]
            ]
        );
    }
}