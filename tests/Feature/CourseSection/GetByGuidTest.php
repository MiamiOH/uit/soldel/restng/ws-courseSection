<?php
/**
 * Author: liaom
 * Date: 7/25/18
 * Time: 4:06 PM
 */

namespace MiamiOH\CourseSectionWebService\Tests\Feature\CourseSection;


use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Exception\CourseSectionNotFoundException;
use MiamiOH\Pike\Exception\InvalidCourseSectionGuidException;
use MiamiOH\RESTng\App;

class GetByGuidTest extends TestCase
{
    public function testInvalidGuid()
    {
        $this->viewCourseSectionService->method('getByGuid')
            ->willThrowException(new InvalidCourseSectionGuidException());

        $response = $this->getJson('/courseSection/v3/courseSection/sdf');
        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testCourseSectionNotFound()
    {
        $this->viewCourseSectionService->method('getByGuid')
            ->willThrowException(new CourseSectionNotFoundException());

        $response = $this->getJson('/courseSection/v3/courseSection/sdf');
        $response->assertStatus(App::API_NOTFOUND);
    }

    public function testGetByGuid()
    {
        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $response = $this->getJson('/courseSection/v3/courseSection/sdf');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedCourseSectionResponse1()
        ]);
    }

    public function testComposeEnrollmentCount()
    {
        $this->viewCourseSectionService
            ->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $this->viewCourseSectionService
            ->method('getEnrollmentCountBysectionGuids')
            ->willReturn(new CourseSectionEnrollmentCountCollection([
               $this->getMockedEnrollmentCount1()
            ]));

        $response = $this->getJson("/courseSection/v3/courseSection/09899afe-869d-4d65-9965-540198e65f11?compose=enrollmentCount");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedEnrollmentCountResponse1()
        ]);
    }

    public function testComposeSchedules()
    {
        $this->viewCourseSectionService
            ->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $this->viewCourseSectionScheduleService
            ->method('getCollectionByCourseSectionGuidCollection')
            ->willReturn($this->getMockedSchedules1());

        $response = $this->getJson("/courseSection/v3/courseSection/09899afe-869d-4d65-9965-540198e65f11?compose=schedules");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedSchedulesResponse1()
        ]);
    }

    public function testComposeInstructors()
    {
        $this->viewCourseSectionService
            ->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->getMockedInstructors1());

        $this->viewPersonService
            ->method('getCollectionByUniqueIds')
            ->willReturn($this->getMockedPerson1());

        $response = $this->getJson("/courseSection/v3/courseSection/09899afe-869d-4d65-9965-540198e65f11?compose=instructors");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedInstructorResponse()
        ]);
    }

    public function testComposeAttributes()
    {
        $this->viewCourseSectionService
            ->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $this->viewCourseSectionAttributeService
            ->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->getMockedAttribute1());

        $response = $this->getJson("/courseSection/v3/courseSection/09899afe-869d-4d65-9965-540198e65f11?compose=attributes");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedAttributeResponse1()
        ]);
    }

    public function testComposeCrossListedCourseSections()
    {
        $this->viewCourseSectionService
            ->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $this->viewCrossListedCourseSectionService
            ->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->getMockedCrossListedCourseSections1());

        $response = $this->getJson("/courseSection/v3/courseSection/09899afe-869d-4d65-9965-540198e65f11?compose=crossListedCourseSections");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedCrossListedCourseSectionsResponse1()
        ]);
    }

    public function testComposeEnrollmentDistribution()
    {
        $this->viewCourseSectionService
            ->method('getByGuid')
            ->willReturn($this->getMockedCourseSection1());

        $this->viewCourseSectionEnrollmentDistributionService
            ->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->getMockedEnrollmentDistribution1());

        $response = $this->getJson("/courseSection/v3/courseSection/09899afe-869d-4d65-9965-540198e65f11?compose=enrollmentDistribution");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => $this->getMockedEnrollmentDistributionResponse1()
        ]);
    }
}