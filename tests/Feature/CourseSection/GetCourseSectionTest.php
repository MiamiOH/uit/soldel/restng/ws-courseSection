<?php
/**
 * Author: liaom
 * Date: 7/25/18
 * Time: 11:42 AM
 */

namespace MiamiOH\CourseSectionWebService\Tests\Feature\CourseSection;


use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\RESTng\App;

class GetCourseSectionTest extends TestCase
{
    public function testInvalidCourseSectionGuid()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?courseSectionGuid=123');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCampusCode()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?campusCode=adfsdafsadf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidTermCode()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?termCode=asd');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCrn()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?crn=asdfasdfasf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSubjectCode()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?course_subjectCode=afsadfasdfasdfads');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCourseNumber()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?course_number=adfsadfasdfasdfas');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCourseSectionStatusCode()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?courseSectionStatusCode=afasldflasdkfjasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidNumberOfMaxEnrollment()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?enrollmentCount_numberOfMax=-10');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidNumberOfCurrentEnrollment()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?enrollmentCount_numberOfCurrent=-10');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidNumberOfActiveEnrollment()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?enrollmentCount_numberOfActive=-10');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidIsMidtermGradeSubmissionAvailable()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?isMidtermGradeSubmissionAvailable=abc');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidIsFinalGradeSubmissionAvailable()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?isFinalGradeSubmissionAvailable=abc');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidHasSeatAvailable()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?hasSeatAvailable=abc');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidPartOfTermCode()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?partOfTerm_code=afasdfasdfasdfasd');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidPartOfTermStartDate()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?partOfTerm_startDate=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidPartOfTermEndDate()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?partOfTerm_endDate=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSchedulesStartDate()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?schedules_startDate=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSchedulesEndDate()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?schedules_endDate=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSchedulesStartTime()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?schedules_startTime=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSchedulesEndTime()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?schedules_endTime=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSchedulesBuildingCode()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?schedules_buildingCode=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidSchedulesDays()
    {
        $response = $this->getJson('/courseSection/v3/courseSection?schedules_days=asdfasdf');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testGetCourseSection()
    {
        $mockedCourseSectionCollection = new CourseSectionCollection([
            $this->getMockedCourseSection1()
        ]);

        $mockedCourseSectionCollection->setTotalNumOfItems(10);

        $this->viewCourseSectionService
            ->method('searchCourseSection')
            ->willReturn($mockedCourseSectionCollection);

        $response = $this->getJson("/courseSection/v3/courseSection?courseSectionGuid=870ef08b-1366-4531-804b-256f790521e0&campusCode=O&termCode=201710&crn=12345&course_subjectCode=CSE&courseSectionCode=A&course_number=174&courseSectionStatusCode=A&enrollmentCount_numberOfMax=10&enrollmentCount_numberOfCurrent=10 to 11&enrollmentCount_numberOfActive=10&enrollmentCount_numberOfAvailable=10&isMidtermGradeSubmissionAvailable=true&isFinalGradeSubmissionAvailable=false&hasSeatAvailable=true&partOfTerm_code=1&partOfTerm_startDate=2017-01-02&partOfTerm_endDate=2017-05-21&schedules_startDate=2017-01-01&schedules_endDate=2018-05-29 to 2018-05-30&schedules_startTime=13:00 to 14:00&schedules_endTime=14:00&schedules_buildingCode=BEN&schedules_roomNumber=111&schedules_days=MWF&instructors_uniqueId=liaom&limit=1&offset=2");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'total' => 10,
            'data' => [
                $this->getMockedCourseSectionResponse1()
            ]
        ]);
    }

    public function testCourseSectionNotFound()
    {
        $mockedCourseSectionCollection = new CourseSectionCollection();

        $mockedCourseSectionCollection->setTotalNumOfItems(0);

        $this->viewCourseSectionService
            ->method('searchCourseSection')
            ->willReturn($mockedCourseSectionCollection);

        $response = $this->getJson("/courseSection/v3/courseSection");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'total' => 0,
            'data' => []
        ]);
    }
}