<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 4:07 PM
 */

namespace MiamiOH\CourseSectionWebService\Tests\Feature\CourseSection;


use Carbon\Carbon;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewCourseSectionAttributeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentDistributionService;
use MiamiOH\Pike\App\Service\ViewCourseSectionScheduleService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\App\Service\ViewCrossListedCourseSectionService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\ViewPersonService;
use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCreditHoursDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionLevelDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionSummaryCollection;
use MiamiOH\Pike\Domain\Collection\CrossListedCourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\PersonCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\Model\CourseSectionCreditHoursDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionLevelDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\Model\CourseSectionSummary;
use MiamiOH\Pike\Domain\Model\CrossListedCourseSection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use PHPUnit\Framework\MockObject\MockObject;

class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $viewCourseSectionService;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionAttributeService;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionScheduleService;
    /**
     * @var MockObject
     */
    protected $viewInstructorAssignmentService;
    /**
     * @var MockObject
     */
    protected $viewPersonService;
    /**
     * @var MockObject
     */
    protected $viewCrossListedCourseSectionService;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionEnrollmentDistributionService;

    protected function setUp(): void
    {
        parent::setUp();
        $pikeServiceFactory = $this->createMock(PikeServiceFactory::class);

        $this->viewCourseSectionService = $this->createMock(ViewCourseSectionService::class);
        $this->viewCourseSectionAttributeService = $this->createMock(ViewCourseSectionAttributeService::class);
        $this->viewCourseSectionScheduleService = $this->createMock(ViewCourseSectionScheduleService::class);
        $this->viewInstructorAssignmentService = $this->createMock(ViewInstructorAssignmentService::class);
        $this->viewPersonService = $this->createMock(ViewPersonService::class);
        $this->viewCrossListedCourseSectionService = $this->createMock(ViewCrossListedCourseSectionService::class);
        $this->viewCourseSectionEnrollmentDistributionService = $this->createMock(ViewCourseSectionEnrollmentDistributionService::class);


        $pike = $this->createMock(AppMapper::class);
        $pike->method('getViewCourseSectionService')->willReturn($this->viewCourseSectionService);
        $pike->method('getViewCourseSectionAttributeService')->willReturn($this->viewCourseSectionAttributeService);
        $pike->method('getViewCourseSectionScheduleService')->willReturn($this->viewCourseSectionScheduleService);
        $pike->method('getViewInstructorAssignmentService')->willReturn($this->viewInstructorAssignmentService);
        $pike->method('getViewPersonService')->willReturn($this->viewPersonService);
        $pike->method('getViewCrossListedCourseSectionService')->willReturn($this->viewCrossListedCourseSectionService);
        $pike->method('getViewCourseSectionEnrollmentDistributionService')->willReturn($this->viewCourseSectionEnrollmentDistributionService);


        $pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($pike);

        $this->app->useService([
            'name' => 'PikeServiceFactory',
            'object' => $pikeServiceFactory,
            'description' => 'Mocked Pike Service Factory'
        ]);
    }

    protected function mockCourseSection(
        string $getGuid,
        Course $getCourse,
        string $getTermCode,
        string $getCrn,
        string $getCourseTitle,
        string $getTermDescription,
        string $getSectionCode,
        string $getCreditHoursDescription,
        float $getCreditHoursLow,
        float $getCreditHoursHigh,
        array $getCreditHoursAvailable,
        string $getInstructionalTypeCode,
        string $getInstructionalTypeDescription,
        string $getCampusCode,
        string $getCampusDescription,
        string $getCourseSectionStatusCode,
        string $getCourseSectionStatusDescription,
        float $getMaxNumberOfEnrollment,
        PartOfTerm $getPartOfTerm,
        string $getSectionName,
        bool $isMidtermGradeSubmissionAvailable,
        bool $isFinalGradeSubmissionAvailable,
        bool $isFinalGradeRequired,
        bool $isDisplayed,
        ?string $getStandardizedDivisionCode,
        ?string $getStandardizedDivisionName,
        ?string $getStandardizedDepartmentCode,
        ?string $getStandardizedDepartmentName,
        ?string $getLegacyStandardizedDepartmentCode,
        ?string $getLegacyStandardizedDepartmentName
    ): MockObject {
        $courseSection = $this->createMock(CourseSection::class);

        $courseSection->method('getGuid')->willReturn(new CourseSectionGuid($getGuid));
        $courseSection->method('getCourse')->willReturn($getCourse);
        $courseSection->method('getTermCode')->willReturn(new TermCode($getTermCode));
        $courseSection->method('getCrn')->willReturn(new Crn($getCrn));
        $courseSection->method('getCourseTitle')->willReturn($getCourseTitle);
        $courseSection->method('getTermDescription')->willReturn($getTermDescription);
        $courseSection->method('getSectionCode')->willReturn($getSectionCode);
        $courseSection->method('getCreditHoursDescription')->willReturn($getCreditHoursDescription);
        $courseSection->method('getCreditHoursLow')->willReturn($getCreditHoursLow);
        $courseSection->method('getCreditHoursHigh')->willReturn($getCreditHoursHigh);
        $courseSection->method('getCreditHoursAvailable')->willReturn($getCreditHoursAvailable);
        $courseSection->method('getInstructionalTypeCode')->willReturn(new InstructionalTypeCode($getInstructionalTypeCode));
        $courseSection->method('getInstructionalTypeDescription')->willReturn($getInstructionalTypeDescription);
        $courseSection->method('getCampusCode')->willReturn(new CampusCode($getCampusCode));
        $courseSection->method('getCampusDescription')->willReturn($getCampusDescription);
        $courseSection->method('getCourseSectionStatusCode')->willReturn(new CourseSectionStatusCode($getCourseSectionStatusCode));
        $courseSection->method('getCourseSectionStatusDescription')->willReturn($getCourseSectionStatusDescription);
        $courseSection->method('getMaxNumberOfEnrollment')->willReturn($getMaxNumberOfEnrollment);
        $courseSection->method('getPartOfTerm')->willReturn($getPartOfTerm);
        $courseSection->method('getSectionName')->willReturn($getSectionName);
        $courseSection->method('isMidtermGradeSubmissionAvailable')->willReturn($isMidtermGradeSubmissionAvailable);
        $courseSection->method('isFinalGradeSubmissionAvailable')->willReturn($isFinalGradeSubmissionAvailable);
        $courseSection->method('isFinalGradeRequired')->willReturn($isFinalGradeRequired);
        $courseSection->method('isDisplayed')->willReturn($isDisplayed);
        $courseSection->method('getStandardizedDivisionCode')->willReturn(new DivisionCode($getStandardizedDivisionCode));
        $courseSection->method('getStandardizedDivisionName')->willReturn($getStandardizedDivisionName);
        $courseSection->method('getStandardizedDepartmentCode')->willReturn(new DepartmentCode($getStandardizedDepartmentCode));
        $courseSection->method('getStandardizedDepartmentName')->willReturn($getStandardizedDepartmentName);
        $courseSection->method('getLegacyStandardizedDepartmentCode')->willReturn(new DepartmentCode($getLegacyStandardizedDepartmentCode));
        $courseSection->method('getLegacyStandardizedDepartmentName')->willReturn($getLegacyStandardizedDepartmentName);

        return $courseSection;
    }

    public function mockCourse(
        ?string $getLabHoursDescription,
        string $getTermCode,
        ?string $getLectureHoursDescription,
        ?string $getCreditHoursDescription,
        string $getGuid,
        string $getSubjectCode,
        string $getNumber,
        string $getTermEffectedCode,
        string $getTitle,
        ?string $getDescription,
        string $getSchoolCode,
        string $getSchoolName,
        string $getDepartmentCode,
        string $getDepartmentName,
        string $getSubjectDescription,
        ?float $getCreditHoursLow,
        ?float $getCreditHoursHigh,
        ?float $getLectureHoursLow,
        ?float $getLectureHoursHigh,
        ?float $getLabHoursLow,
        ?float $getLabHoursHigh,
        array $getCreditHoursAvailable
    ): MockObject {
        $obj = $this->createMock(Course::class);

        $obj->method('getLabHoursDescription')->willReturn($getLabHoursDescription);
        $obj->method('getTermCode')->willReturn(new TermCode($getTermCode));
        $obj->method('getLectureHoursDescription')->willReturn($getLectureHoursDescription);
        $obj->method('getCreditHoursDescription')->willReturn($getCreditHoursDescription);
        $obj->method('getGuid')->willReturn(new CourseGuid($getGuid));
        $obj->method('getSubjectCode')->willReturn(new SubjectCode($getSubjectCode));
        $obj->method('getNumber')->willReturn(new CourseNumber($getNumber));
        $obj->method('getTermEffectedCode')->willReturn(new TermCode($getTermEffectedCode));
        $obj->method('getTitle')->willReturn($getTitle);
        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getSchoolCode')->willReturn(new SchoolCode($getSchoolCode));
        $obj->method('getSchoolName')->willReturn($getSchoolName);
        $obj->method('getDepartmentCode')->willReturn(new DepartmentCode($getDepartmentCode));
        $obj->method('getDepartmentName')->willReturn($getDepartmentName);
        $obj->method('getSubjectDescription')->willReturn($getSubjectDescription);
        $obj->method('getCreditHoursLow')->willReturn($getCreditHoursLow);
        $obj->method('getCreditHoursHigh')->willReturn($getCreditHoursHigh);
        $obj->method('getLectureHoursLow')->willReturn($getLectureHoursLow);
        $obj->method('getLectureHoursHigh')->willReturn($getLectureHoursHigh);
        $obj->method('getLabHoursLow')->willReturn($getLabHoursLow);
        $obj->method('getLabHoursHigh')->willReturn($getLabHoursHigh);
        $obj->method('getCreditHoursAvailable')->willReturn($getCreditHoursAvailable);

        return $obj;
    }

    public function mockPartOfTerm(
        string $getCode,
        string $getDescription,
        Carbon $getStartDate,
        Carbon $getEndDate
    ): MockObject {
        $obj = $this->createMock(PartOfTerm::class);

        $obj->method('getCode')->willReturn(new PartOfTermCode($getCode));
        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getStartDate')->willReturn($getStartDate);
        $obj->method('getEndDate')->willReturn($getEndDate);

        return $obj;
    }

    public function mockCrossListedCourseSection(
        string $getCrn,
        string $getSubjectCode,
        string $getSectionCode,
        string $getSectionName,
        string $getCourseNumber,
        string $getCourseSectionGuid,
        string $getHostCourseSectionGuid
    ): MockObject {
        $obj = $this->createMock(CrossListedCourseSection::class);

        $obj->method('getCrn')->willReturn(new Crn($getCrn));
        $obj->method('getSubjectCode')->willReturn(new SubjectCode($getSubjectCode));
        $obj->method('getSectionCode')->willReturn($getSectionCode);
        $obj->method('getSectionName')->willReturn($getSectionName);
        $obj->method('getCourseNumber')->willReturn(new CourseNumber($getCourseNumber));
        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));
        $obj->method('getHostCourseSectionGuid')->willReturn(new CourseSectionGuid($getHostCourseSectionGuid));

        return $obj;
    }

    public function mockPerson(
        string $getUniqueId,
        string $getPidm,
        string $getBannerId,
        ?string $getLastName,
        ?string $getFirstName,
        ?string $getMiddleName,
        ?string $getPrefix,
        ?string $getSuffix,
        ?string $getPreferredFirstName,
        string $getInformalDisplayedName,
        string $getFormalDisplayedName,
        string $getInformalSortedName,
        string $getFormalSortedName
    ): MockObject {
        $obj = $this->createMock(Person::class);

        $obj->method('getUniqueId')->willReturn(new UniqueId($getUniqueId));
        $obj->method('getPidm')->willReturn(new Pidm($getPidm));
        $obj->method('getBannerId')->willReturn(new BannerId($getBannerId));
        $obj->method('getLastName')->willReturn($getLastName);
        $obj->method('getFirstName')->willReturn($getFirstName);
        $obj->method('getMiddleName')->willReturn($getMiddleName);
        $obj->method('getPrefix')->willReturn($getPrefix);
        $obj->method('getSuffix')->willReturn($getSuffix);
        $obj->method('getPreferredFirstName')->willReturn($getPreferredFirstName);
        $obj->method('getInformalDisplayedName')->willReturn($getInformalDisplayedName);
        $obj->method('getFormalDisplayedName')->willReturn($getFormalDisplayedName);
        $obj->method('getInformalSortedName')->willReturn($getInformalSortedName);
        $obj->method('getFormalSortedName')->willReturn($getFormalSortedName);

        return $obj;
    }

    public function mockInstructorAssignment(
        string $getUniqueId,
        string $getTermCode,
        string $getCrn,
        string $getGuid,
        string $getCourseSectionGuid,
        bool $isPrimary
    ): MockObject {
        $obj = $this->createMock(InstructorAssignment::class);

        $obj->method('getUniqueId')->willReturn(new UniqueId($getUniqueId));
        $obj->method('getTermCode')->willReturn(new TermCode($getTermCode));
        $obj->method('getCrn')->willReturn(new Crn($getCrn));
        $obj->method('getGuid')->willReturn(new InstructorAssignmentGuid($getGuid));
        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));
        $obj->method('isPrimary')->willReturn($isPrimary);

        return $obj;
    }

    public function mockCourseSectionSchedule(
        string $getCourseSectionGuid,
        Carbon $getStartDate,
        Carbon $getEndDate,
        ?Carbon $getStartTime,
        ?Carbon $getEndTime,
        ?string $getDays,
        ?string $getRoom,
        ?string $getBuildingCode,
        ?string $getBuildingName,
        ?string $getTypeCode,
        ?string $getTypeDescription
    ): MockObject {
        $obj = $this->createMock(CourseSectionSchedule::class);

        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));
        $obj->method('getStartDate')->willReturn($getStartDate);
        $obj->method('getEndDate')->willReturn($getEndDate);
        $obj->method('getStartTime')->willReturn($getStartTime);
        $obj->method('getEndTime')->willReturn($getEndTime);
        $obj->method('getDays')->willReturn(new CourseSectionScheduleDays($getDays));
        $obj->method('getRoom')->willReturn($getRoom);
        $obj->method('getBuildingCode')->willReturn(new BuildingCode($getBuildingCode));
        $obj->method('getBuildingName')->willReturn($getBuildingName);
        $obj->method('getTypeCode')->willReturn(new CourseSectionScheduleTypeCode($getTypeCode));
        $obj->method('getTypeDescription')->willReturn($getTypeDescription);

        return $obj;
    }

    public function mockCourseSectionAttribute(
        string $getCode,
        string $getDescription,
        string $getCourseSectionGuid
    ): MockObject {
        $obj = $this->createMock(CourseSectionAttribute::class);

        $obj->method('getCode')->willReturn(new CourseSectionAttributeCode($getCode));
        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));

        return $obj;
    }

    public function mockCourseSectionEnrollmentDistribution(
        CourseSectionCreditHoursDistributionCollection $getCourseSectionCreditHoursDistributionCollection,
        CourseSectionLevelDistributionCollection $getCourseSectionLevelDistributionCollection,
        CourseSectionSummaryCollection $getCourseSectionSummaryCollection,
        string $getCourseSectionGuid
    ): MockObject {
        $obj = $this->createMock(CourseSectionEnrollmentDistribution::class);

        $obj->method('getCourseSectionCreditHoursDistributionCollection')->willReturn($getCourseSectionCreditHoursDistributionCollection);
        $obj->method('getCourseSectionLevelDistributionCollection')->willReturn($getCourseSectionLevelDistributionCollection);
        $obj->method('getCourseSectionSummaryCollection')->willReturn($getCourseSectionSummaryCollection);
        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));

        return $obj;
    }

    public function mockEnrollmentCount(
        string $getCourseSectionGuid,
        int $getNumOfMaxEnrollments,
        int $getNumOfCurrentEnrollments,
        int $getNumOfActiveEnrollments,
        int $getNumOfAvailableEnrollments
    ): MockObject {
        $obj = $this->createMock(CourseSectionEnrollmentCount::class);

        $obj->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid($getCourseSectionGuid));
        $obj->method('getNumOfMaxEnrollments')->willReturn($getNumOfMaxEnrollments);
        $obj->method('getNumOfCurrentEnrollments')->willReturn($getNumOfCurrentEnrollments);
        $obj->method('getNumOfActiveEnrollments')->willReturn($getNumOfActiveEnrollments);
        $obj->method('getNumOfAvailableEnrollments')->willReturn($getNumOfAvailableEnrollments);

        return $obj;
    }

    public function mockCourseSectionSummary(
        string $getDescription,
        ?int $getNumberOfCurrentEnrollment,
        ?int $getNumberOfActiveEnrollment
    ): MockObject {
        $obj = $this->createMock(CourseSectionSummary::class);

        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getNumberOfCurrentEnrollment')->willReturn($getNumberOfCurrentEnrollment);
        $obj->method('getNumberOfActiveEnrollment')->willReturn($getNumberOfActiveEnrollment);

        return $obj;
    }

    public function mockCourseSectionLevelDistribution(
        string $getCode,
        string $getDescription,
        ?int $getNumberOfCurrentEnrollment,
        ?int $getNumberOfActiveEnrollment
    ): MockObject {
        $obj = $this->createMock(CourseSectionLevelDistribution::class);

        $obj->method('getCode')->willReturn($getCode);
        $obj->method('getDescription')->willReturn($getDescription);
        $obj->method('getNumberOfCurrentEnrollment')->willReturn($getNumberOfCurrentEnrollment);
        $obj->method('getNumberOfActiveEnrollment')->willReturn($getNumberOfActiveEnrollment);

        return $obj;
    }

    public function mockCourseSectionCreditHoursDistribution(
        float $getCredit,
        ?int $getNumberOfCurrentEnrollment,
        ?int $getNumberOfActiveEnrollment
    ): MockObject {
        $obj = $this->createMock(CourseSectionCreditHoursDistribution::class);

        $obj->method('getCredit')->willReturn($getCredit);
        $obj->method('getNumberOfCurrentEnrollment')->willReturn($getNumberOfCurrentEnrollment);
        $obj->method('getNumberOfActiveEnrollment')->willReturn($getNumberOfActiveEnrollment);

        return $obj;
    }

    public function getMockedCourseSection1(): MockObject
    {
        return $this->mockCourseSection(
            '09899afe-869d-4d65-9965-540198e65f11',
            $this->mockCourse(
                '1',
                '201710',
                '2',
                '3',
                '3015c716-5f31-4a3e-8264-8a878de24e1b',
                'CSE',
                '174',
                '201110',
                'Fundmntls-Progrming&Prob Solvg',
                '174 Fundamentals of Programming and Problem Solving',
                'AP',
                'Col of Engineering & Computing',
                'CSE',
                'Comp Sci &Software Engineering',
                'Comp Sci &Software Engineering',
                3,
                3,
                2,
                null,
                1,
                null,
                [3]
            ),
            '201710',
            '59407',
            'Fundmntls-Progrming&Prob Solvg',
            'Fall Semester 2016-17',
            'A',
            '3',
            3,
            3,
            [3],
            'Z',
            'Lecture/Lab',
            'O',
            'Oxford',
            'A',
            'Active',
            28,
            $this->mockPartOfTerm(
                '1',
                'Full Semester',
                Carbon::createFromFormat('Y-m-d', '2016-08-29'),
                Carbon::createFromFormat('Y-m-d', '2016-12-17')
            ),
            'CSE 174 A',
            false,
            false,
            true,
            true,
            'CEC',
            'College of Engineering & Computing',
            'CSE',
            'Computer Science & Software Engineering',
            'CSE',
            'Computer Science & Software Engineering'
        );
    }

    public function getMockedCourseSectionResponse1(): array
    {
        return [
            "termCode" => "201710",
            "termDescription" => "Fall Semester 2016-17",
            "crn" => "59407",
            "courseSectionGuid" => "09899afe-869d-4d65-9965-540198e65f11",
            "sectionName" => "CSE 174 A",
            "instructionTypeCode" => "Z",
            "instructionTypeDescription" => "Lecture/Lab",
            "courseSectionCode" => "A",
            "courseSectionStatusCode" => "A",
            "courseSectionStatusDescription" => "Active",
            "campusCode" => "O",
            "campusName" => "Oxford",
            "creditHoursDescription" => "3",
            "creditHoursLow" => 3,
            "creditHoursHigh" => 3,
            "isMidtermGradeSubmissionAvailable" => false,
            "isFinalGradeSubmissionAvailable" => false,
            "isFinalGradeRequired" => true,
            "standardizedDivisionCode" => "CEC",
            "standardizedDivisionName" => "College of Engineering & Computing",
            "standardizedDepartmentCode" => "CSE",
            "standardizedDepartmentName" => "Computer Science & Software Engineering",
            "legacyStandardizedDepartmentCode" => "CSE",
            "legacyStandardizedDepartmentName" => "Computer Science & Software Engineering",
            "creditHoursAvailable" => [
                3
            ],
            "isDisplayed" => true,
            "course" => [
                "schoolCode" => "AP",
                "schoolName" => "Col of Engineering & Computing",
                "departmentCode" => "CSE",
                "departmentName" => "Comp Sci &Software Engineering",
                "title" => "Fundmntls-Progrming&Prob Solvg",
                "subjectCode" => "CSE",
                "subjectDescription" => "Comp Sci &Software Engineering",
                "number" => "174",
                "lectureHoursDescription" => "2",
                "labHoursDescription" => "1",
                "creditHoursHigh" => 3,
                "creditHoursLow" => 3,
                "lectureHoursHigh" => null,
                "lectureHoursLow" => 2,
                "labHoursHigh" => null,
                "labHoursLow" => 1,
                "description" => "174 Fundamentals of Programming and Problem Solving"
            ],
            "partOfTerm" => [
                "code" => "1",
                "name" => "Full Semester",
                "startDate" => "2016-08-29",
                "endDate" => "2016-12-17"
            ]
        ];
    }

    public function getMockedEnrollmentCount1(): MockObject
    {
        return $this->mockEnrollmentCount(
            '09899afe-869d-4d65-9965-540198e65f11',
            28,
            27,
            24,
            1
        );
    }

    public function getMockedEnrollmentCountResponse1(): array
    {
        return [
            'enrollmentCount' => [
                "numberOfMax" => 28,
                "numberOfCurrent" => 27,
                "numberOfActive" => 24,
                "numberOfAvailable" => 1
            ]
        ];
    }

    public function getMockedSchedules1(): CourseSectionScheduleCollection
    {
        return new CourseSectionScheduleCollection([
            $this->mockCourseSectionSchedule(
                '09899afe-869d-4d65-9965-540198e65f11',
                Carbon::createFromFormat('Y-m-d', '2016-12-14'),
                Carbon::createFromFormat('Y-m-d', '2016-12-14'),
                Carbon::createFromFormat('H:i', '08:00'),
                Carbon::createFromFormat('H:i', '10:00'),
                'W',
                '152',
                'SHD',
                'Shideler Hall',
                'FEXM',
                'Final Exam'
            ),
            $this->mockCourseSectionSchedule(
                '09899afe-869d-4d65-9965-540198e65f11',
                Carbon::createFromFormat('Y-m-d', '2016-08-29'),
                Carbon::createFromFormat('Y-m-d', '2016-12-10'),
                Carbon::createFromFormat('H:i', '08:30'),
                Carbon::createFromFormat('H:i', '09:25'),
                'MW',
                '121',
                'PBD',
                'Peabody Hall',
                'CLAS',
                'Class'
            )
        ]);
    }

    public function getMockedSchedulesResponse1(): array
    {
        return [
            'schedules' => [
                [
                    "startDate" => "2016-12-14",
                    "endDate" => "2016-12-14",
                    "startTime" => "08:00",
                    "endTime" => "10:00",
                    "roomNumber" => "152",
                    "buildingCode" => "SHD",
                    "buildingName" => "Shideler Hall",
                    "days" => "W",
                    "scheduleTypeCode" => "FEXM",
                    "scheduleTypeDescription" => "Final Exam"
                ],
                [
                    "startDate" => "2016-08-29",
                    "endDate" => "2016-12-10",
                    "startTime" => "08:30",
                    "endTime" => "09:25",
                    "roomNumber" => "121",
                    "buildingCode" => "PBD",
                    "buildingName" => "Peabody Hall",
                    "days" => "MW",
                    "scheduleTypeCode" => "CLAS",
                    "scheduleTypeDescription" => "Class"
                ]
            ]
        ];
    }

    public function getMockedInstructors1(): InstructorAssignmentCollection
    {
        return new InstructorAssignmentCollection([
            $this->mockInstructorAssignment(
                'ramasav',
                '201710',
                '59407',
                'aa899afe-869d-4d65-9965-540198e65f11',
                '09899afe-869d-4d65-9965-540198e65f11',
                true
            )
        ]);
    }

    public function getMockedPerson1(): PersonCollection
    {
        return new PersonCollection([
            $this->mockPerson(
                'ramasav',
                '1111111',
                '+01234345',
                'Ramasamy',
                'Vijayalakshmi',
                null,
                'Dr.',
                null,
                null,
                'Vijayalakshmi Ramasamy',
                'Dr. Vijayalakshmi Ramasamy',
                'Ramasamy, Vijayalakshmi ',
                'Ramasamy, Vijayalakshmi Dr.'
            )
        ]);
    }

    public function getMockedInstructorResponse(): array
    {
        return [
            'instructors' => [
                [
                    "isPrimary" => true,
                    "person" => [
                        "uniqueId" => "ramasav",
                        "lastName" => "Ramasamy",
                        "firstName" => "Vijayalakshmi",
                        "middleName" => null,
                        "prefix" => "Dr.",
                        "suffix" => null,
                        "preferredFirstName" => null,
                        "informalDisplayedName" => "Vijayalakshmi Ramasamy",
                        "formalDisplayedName" => "Dr. Vijayalakshmi Ramasamy",
                        "informalSortedName" => "Ramasamy, Vijayalakshmi ",
                        "formalSortedName" => "Ramasamy, Vijayalakshmi Dr."
                    ]
                ]
            ]
        ];
    }

    public function getMockedAttribute1(): CourseSectionAttributeCollection
    {
        return new CourseSectionAttributeCollection([
            $this->mockCourseSectionAttribute(
                'TS',
                'Thematic Sequence',
                '09899afe-869d-4d65-9965-540198e65f11'
            )
        ]);
    }

    public function getMockedAttributeResponse1(): array
    {
        return [
            'attributes' => [
                [
                    'code' => 'TS',
                    'description' => 'Thematic Sequence'
                ]
            ]
        ];
    }

    public function getMockedEnrollmentDistribution1(
    ): CourseSectionEnrollmentDistributionCollection
    {
        return new CourseSectionEnrollmentDistributionCollection([
            $this->mockCourseSectionEnrollmentDistribution(
                new CourseSectionCreditHoursDistributionCollection([
                    $this->mockCourseSectionCreditHoursDistribution(
                        3,
                        27,
                        24
                    )
                ]),
                new CourseSectionLevelDistributionCollection([
                    $this->mockCourseSectionLevelDistribution(
                        '01',
                        'Freshman',
                        10,
                        9
                    ),
                    $this->mockCourseSectionLevelDistribution(
                        '02',
                        'Sophomore',
                        11,
                        9
                    ),
                    $this->mockCourseSectionLevelDistribution(
                        '03',
                        'Junior',
                        4,
                        4
                    ),
                    $this->mockCourseSectionLevelDistribution(
                        '04',
                        'Senior',
                        1,
                        1
                    ),
                    $this->mockCourseSectionLevelDistribution(
                        '99',
                        'Non-Matriculated',
                        1,
                        1
                    )
                ]),
                new CourseSectionSummaryCollection([
                    $this->mockCourseSectionSummary(
                        'undergraduate',
                        27,
                        24
                    ),
                    $this->mockCourseSectionSummary(
                        'underclassman',
                        22,
                        19
                    ),
                    $this->mockCourseSectionSummary(
                        'upperclassman',
                        5,
                        5
                    )
                ]),
                '09899afe-869d-4d65-9965-540198e65f11'
            )
        ]);
    }

    public function getMockedEnrollmentDistributionResponse1(): array
    {
        return [
            'enrollmentDistribution' => [
                "guid" => "09899afe-869d-4d65-9965-540198e65f11",
                "levelDistribution" => [
                    [
                        "code" => "01",
                        "description" => "Freshman",
                        "numberOfCurrentEnrollment" => 10,
                        "numberOfActiveEnrollment" => 9
                    ],
                    [
                        "code" => "02",
                        "description" => "Sophomore",
                        "numberOfCurrentEnrollment" => 11,
                        "numberOfActiveEnrollment" => 9
                    ],
                    [
                        "code" => "03",
                        "description" => "Junior",
                        "numberOfCurrentEnrollment" => 4,
                        "numberOfActiveEnrollment" => 4
                    ],
                    [
                        "code" => "04",
                        "description" => "Senior",
                        "numberOfCurrentEnrollment" => 1,
                        "numberOfActiveEnrollment" => 1
                    ],
                    [
                        "code" => "99",
                        "description" => "Non-Matriculated",
                        "numberOfCurrentEnrollment" => 1,
                        "numberOfActiveEnrollment" => 1
                    ]
                ],
                "creditHoursDistribution" => [
                    [
                        "hours" => 3,
                        "numberOfCurrentEnrollment" => 27,
                        "numberOfActiveEnrollment" => 24
                    ]
                ],
                "summaries" => [
                    [
                        "description" => "undergraduate",
                        "numberOfCurrentEnrollment" => 27,
                        "numberOfActiveEnrollment" => 24
                    ],
                    [
                        "description" => "underclassman",
                        "numberOfCurrentEnrollment" => 22,
                        "numberOfActiveEnrollment" => 19
                    ],
                    [
                        "description" => "upperclassman",
                        "numberOfCurrentEnrollment" => 5,
                        "numberOfActiveEnrollment" => 5
                    ]
                ]
            ]
        ];
    }

    public function getMockedCrossListedCourseSections1(
    ): CrossListedCourseSectionCollection
    {
        return new CrossListedCourseSectionCollection([
            $this->mockCrossListedCourseSection(
                '69082',
                'MTH',
                'A',
                'MTH 547 A',
                '547',
                '00653fdb-1dac-422a-800e-20d8b1d9bac2',
                '09899afe-869d-4d65-9965-540198e65f11'
            )
        ]);
    }

    public function getMockedCrossListedCourseSectionsResponse1(): array
    {
        return [
           'crossListedCourseSections' => [
              [
                  "crn" => "69082",
                  "courseSectionCode" => "A",
                  "sectionName" => "MTH 547 A",
                  "courseSubjectCode" => "MTH",
                  "courseNumber" => "547",
                  "courseSectionGuid" => "00653fdb-1dac-422a-800e-20d8b1d9bac2"
              ]
           ]
        ];
    }
}