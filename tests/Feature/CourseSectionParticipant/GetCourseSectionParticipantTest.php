<?php
/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 7/25/18
 * Time: 10:57 AM
 */

namespace MiamiOH\CourseSectionWebService\Tests\Feature\CourseSectionParticipant;


use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Exception\InvalidCrnException;
use MiamiOH\Pike\Exception\InvalidTermCodeException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationSpecification;
use MiamiOH\RESTng\Testing\UsesAuthentication;


class GetCourseSectionParticipantTest extends TestCase
{
    use UsesAuthentication;

    public function testInvalidAuthentication()
    {
        $this->withToken('liaom')->withoutAcceptHeader()->willNotAuthenticateUser();

        $response = $this->getJson('/courseSection/v3/participant?termCode=201710&crn=70591&token=liaom');

        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    public function testInvalidTermCd()
    {
        $this->withToken('liaom')->withoutAcceptHeader()->willAuthenticateUser();

        $this->viewInstructorAssignmentService->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willThrowException(new InvalidTermCodeException());

        $response = $this->getJson('/courseSection/v3/participant?termCode=2017ad10&token=liaom');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCrn()
    {
        $this->withToken('liaom')->withoutAcceptHeader()->willAuthenticateUser();

        $this->viewInstructorAssignmentService->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willThrowException(new InvalidCrnException());

        $response = $this->getJson('/courseSection/v3/participant?termCode=201710&crn=asff&token=liaom');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testGetParticipantWithFullAccessFilterByAll()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollment()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(true);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => $this->getMockedCourseSectionParticipantResponse()
            ]
        );
    }

    public function testGetParticipantWithFullAccessFilterByInstructor()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollment()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(true);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591&role=instructor");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantResponse()[0]]
            ]
        );
    }

    public function testGetParticipantWithFullAccessFilterByStudent()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollment()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(true);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591&role=student");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantResponse()[1]]
            ]
        );
    }

    public function testGetParticipantWithStandardAccessFilterByAll()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollment()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(true);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => $this->getMockedCourseSectionParticipantStandaradAccessResponse()
            ]
        );
    }

    public function testGetParticipantWithStandardAccessFilterByInstructor()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollment()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(true);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591&role=instructor");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantStandaradAccessResponse()[0]]
            ]
        );
    }

    public function testGetParticipantWithStandardAccessFilterByStudent()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollment()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(true);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591&role=student");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantStandaradAccessResponse()[1]]
            ]
        );
    }

    public function testGetParticipantWithTokenUserResponseFilterByAll()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollmentForTokenUser()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => $this->getMockedCourseSectionParticipantTokenResponse()
            ]
        );
    }


    public function testGetParticipantWithTokenUserResponseFilterByInstructor()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollmentForTokenUser()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591&role=instructor");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantTokenResponse()[0]]
            ]
        );
    }


    public function testGetParticipantWithTokenUserResponseFilterByStudent()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollmentForTokenUser()
        ]);

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?termCode=201710&crn=70591&role=student");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantTokenResponse()[1]]
            ]
        );
    }

    public function testGetParticipantWithDefaultTermCode()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollmentForTokenUser()
        ]);

        $mockedTerm = $this->getMockedTerm();

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewTermService
            ->method('getCurrentTerm')
            ->willReturn($mockedTerm);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant?crn=70591&role=student");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [$this->getMockedCourseSectionParticipantTokenResponse()[1]]
            ]
        );
    }

    public function testGetParticipantWithUniqueIdNullAndCrnNull()
    {
        $mockedInstructorAssignmentCollection = new InstructorAssignmentCollection([
            $this->getMockedInstructor()
        ]);

        $mockedCourseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection([
            $this->getMockedCourseSectionEnrollmentForTokenUser()
        ]);

        $mockedTerm = $this->getMockedTerm();

        $this
            ->withToken('asdfasd')
            ->willAuthenticateUser('liaom');

        $this->authorizationValidator
            ->expects($this->at(0))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'full'),
                'liaom'
            )
            ->willReturn(false);

        $this->authorizationValidator
            ->expects($this->at(1))
            ->method('validateAuthorizationForKey')
            ->with(
                AuthorizationSpecification::fromValues('WebServices', 'EnrollmentService', 'standard'),
                'liaom'
            )
            ->willReturn(false);

        $this->viewTermService
            ->method('getCurrentTerm')
            ->willReturn($mockedTerm);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedInstructorAssignmentCollection);

        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn($mockedCourseSectionEnrollmentCollection);

        $this->viewCourseSectionService
            ->method('getByGuids')
            ->willReturn($this->getMockedCourseSection());

        $response = $this->getJson("/courseSection/v3/participant");

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => $this->getMockedCourseSectionParticipantTokenResponse()
            ]
        );
    }

}