<?php

namespace MiamiOH\CourseSectionWebService\Tests\Unit;

use MiamiOH\CourseSectionWebService\Services\ClassListService;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\User;
use ReflectionClass;


class ClassListServiceTest extends AbstractBaseServiceTestCase
{
    /**
     * @var ClassListService
     */
    private $classListService;

    protected function setUp(): void
    {
        parent::setUp();

        $user = $this->createMock(User::class);

        $this->classListService = new ClassListService();

        $this->classListService->setPikeServiceFactory($this->pikeServiceFactory);
        $this->classListService->setRequest($this->request);
        $this->classListService->setApiUser($user);
    }

    public function testGetClassListForDefaultTermCode()
    {
        $this->request->method('getOptions')->willReturn(['uniqueId' => ['publicjq']]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);

        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->method('checkAuthorization')->willReturn(true);
        $this->classListService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));

        $actualResult = $this->classListService->getClassList()->getPayload();

        $this->assertEquals(
            [
                $this->classListInstructorPayload1,
                $this->classListEnrollmentPayload1,
            ],
            $actualResult
        );

    }

    public function testQueryParameterValueDoesNotFound()
    {
        $reflectionRosterService = new ReflectionClass($this->classListService);
        $reflectionMethod = $reflectionRosterService->getMethod('getQueryParameterValueByKey');
        $reflectionMethod->setAccessible(true);

        $reflectionOptions = $reflectionRosterService->getProperty('options');
        $reflectionOptions->setAccessible(true);
        $reflectionOptions->setValue($this->classListService, []);

        $this->assertEquals(null,
            $reflectionMethod->invoke($this->classListService, 'a'));
    }
}