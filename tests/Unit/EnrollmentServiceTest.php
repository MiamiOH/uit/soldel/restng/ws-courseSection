<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/4/18
 * Time: 11:54 AM
 */

namespace MiamiOH\CourseSectionWebService\Tests\Unit;

use Carbon\Carbon;
use MiamiOH\CourseSectionWebService\Services\EnrollmentService;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\InvalidTermCodeException;
use MiamiOH\RESTng\Util\Request;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EnrollmentServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $request;
    /**
     * @var MockObject
     */
    private $pike;
    /**
     * @var MockObject
     */
    private $pikeServiceFactory;
    /**
     * @var MockObject
     */
    private $viewCourseSectionEnrollmentService;
    /**
     * @var EnrollmentService
     */
    private $enrollmentService;

    protected function setUp()
    {
        $this->request = $this->createMock(Request::class);
        $this->pike = $this->createMock(AppMapper::class);
        $this->pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->viewCourseSectionEnrollmentService = $this->createMock(ViewCourseSectionEnrollmentService::class);
        $this->enrollmentService = new EnrollmentService();

        $this->pike->method('getViewCourseSectionEnrollmentService')->willReturn($this->viewCourseSectionEnrollmentService);
        $this->pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($this->pike);
        $this->enrollmentService->setPikeServiceFactory($this->pikeServiceFactory);
        $this->enrollmentService->setRequest($this->request);
    }

    public function testUsingInvalidTermCodeWillThrowBadRequestError()
    {
        $this->request->method('getOptions')
            ->willReturn([
                'termCodes' => ['adsfasdf', '201910']
            ]);
        $this->request->method('getOffset')->willReturn(1);
        $this->request->method('getLimit')->willReturn(10);
        $this->viewCourseSectionEnrollmentService->method('search')
            ->willThrowException(new InvalidTermCodeException());

        $res = $this->enrollmentService->getEnrollment();
        $this->assertSame(400, $res->getStatus());
    }

    public function testUsingInvalidCrnWillThrowBadRequestError()
    {
        $this->request->method('getOptions')
            ->willReturn([
                'crns' => ['12345', 'sssss']
            ]);
        $this->request->method('getOffset')->willReturn(1);
        $this->request->method('getLimit')->willReturn(10);
        $this->viewCourseSectionEnrollmentService->method('search')
            ->willThrowException(new InvalidTermCodeException());

        $res = $this->enrollmentService->getEnrollment();
        $this->assertSame(400, $res->getStatus());
    }

    public function testUsingInvalidStatusWillThrowBadRequestError()
    {
        $this->request->method('getOptions')
            ->willReturn([
                'status' => 'something'
            ]);
        $this->request->method('getOffset')->willReturn(1);
        $this->request->method('getLimit')->willReturn(10);

        $res = $this->enrollmentService->getEnrollment();
        $this->assertSame(400, $res->getStatus());
    }

    public function testGetEnrollments()
    {
        $this->request->method('getOptions')
            ->willReturn([
                'termCodes' => ['201910'],
                'uniqueIds' => ['test1']
            ]);
        $this->request->method('getOffset')->willReturn(1);
        $this->request->method('getLimit')->willReturn(10);
        $enrollment = $this->createMock(CourseSectionEnrollment::class);

        $courseSection = $this->createMock(CourseSection::class);
        $courseSection->method('getGuid')->willReturn(new CourseSectionGuid('2537db84-abb2-4c64-9e71-2a41f8d1bfa3'));
        $courseSection->method('getCrn')->willReturn(new Crn('12345'));
        $courseSection->method('getCourseTitle')->willReturn('abcd');

        $viewCsService = $this->createMock(ViewCourseSectionService::class);
        $viewCsService->method('getByGuids')->willReturn(new CourseSectionCollection([$courseSection]));

        $this->pike->method('getViewCourseSectionService')->willReturn($viewCsService);

        $grade = $this->createMock(Grade::class);
        $grade->method('getType')->willReturn(GradeType::ofFinal());
        $grade->method('getValue')->willReturn(new GradeValue('A'));
        $grade->method('getGradeModeCode')->willReturn(new GradeModeCode('S'));

        $enrollment->method('hasAttended')->willReturn(true);
        $enrollment->method('lastAttendDate')->willReturn(Carbon::parse('2019-09-15'));
        $enrollment->method('isGradeSubmissionEligible')->willReturn(true);
        $enrollment->method('getGradeSubmissionEligibleComment')->willReturn('aaa');
        $enrollment->method('isFinalGradeRequired')->willReturn(true);
        $enrollment->method('isMidtermGradeRequired')->willReturn(true);
        $enrollment->method('getGrades')->willReturn(new GradeCollection([$grade]));
        $enrollment->method('getUniqueId')->willReturn(new UniqueId('test1'));
        $enrollment->method('getStatusCode')->willReturn(new CourseSectionEnrollmentStatusCode('RE'));
        $enrollment->method('getStatusDescription')->willReturn('sss');
        $enrollment->method('getCourseSectionGuid')->willReturn(new CourseSectionGuid('2537db84-abb2-4c64-9e71-2a41f8d1bfa3'));
        $enrollment->method('getCreditHours')->willReturn(3);
        $enrollment->method('getStudentLevelCode')->willReturn(new StudentLevelCode('UG'));
        $enrollment->method('getTermCode')->willReturn(new TermCode('201910'));
        $enrollment->method('getGradeModeCode')->willReturn(new GradeModeCode('S'));

        $collection = new CourseSectionEnrollmentCollection([$enrollment]);
        $collection->setTotalNumOfItems(1);
        $this->viewCourseSectionEnrollmentService->method('search')
            ->willReturn($collection);

        $res = $this->enrollmentService->getEnrollment();

        $this->assertSame(1, $res->getTotalObjects());
        $this->assertEquals([
            [
                'hasAttended' => true,
                'lastAttendDate' => '2019-09-15',
                'isEnrollmentActive' => false,
                'isGradeSubmissionEligible' => true,
                'gradeSubmissionEligibleComment' => 'aaa',
                'isFinalGradeRequired' => true,
                'isMidtermGradeRequired' => true,
                'grades' => [
                    [
                        'type' => 'final',
                        'grade' => 'A',
                        'gradeModeCode' => 'S',
                    ]
                ],
                'uniqueId' => 'test1',
                'enrollmentStatusCode' => 'RE',
                'enrollmentStatusDescription' => 'sss',
                'courseSectionGuid' => '2537db84-abb2-4c64-9e71-2a41f8d1bfa3',
                'creditHours' => 3.0,
                'studentLevelCode' => 'UG',
                'termCode' => '201910',
                'gradeModeCode' => 'S',
                'crn' => '12345',
                'title' => 'abcd'
            ]
        ], $res->getPayload());
    }
}