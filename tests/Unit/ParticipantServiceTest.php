<?php

namespace MiamiOH\CourseSectionWebService\Tests\Unit;

use MiamiOH\CourseSectionWebService\Services\ParticipantService;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\User;
use ReflectionClass;


/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 5/9/18
 * Time: 12:05 PM
 */
class ParticipantServiceTest extends AbstractBaseServiceTestCase
{
    /**
     * @var ParticipantService
     */
    private $participantService;

    protected function setUp(): void
    {
        parent::setUp();


        $this->participantService = new ParticipantService();

        $this->participantService->setPikeServiceFactory($this->pikeServiceFactory);
        $this->participantService->setRequest($this->request);
    }

    public function testValidateRole()
    {
        $reflectionRosterService = new ReflectionClass($this->participantService);
        $reflectionValidateRole = $reflectionRosterService->getMethod('validateRole');
        $reflectionValidateRole->setAccessible(true);

        try {
            $reflectionValidateRole->invoke($this->participantService, ['a']);
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }

        try {
            $reflectionValidateRole->invoke($this->participantService, ['student']);
            $this->pass();
        } catch (BadRequest $e) {
            $this->fail();
        }

        try {
            $reflectionValidateRole->invoke($this->participantService, ['instructor']);
            $this->pass();
        } catch (BadRequest $e) {
            $this->fail();
        }

        try {
            $reflectionValidateRole->invoke($this->participantService, ['all']);
            $reflectionValidateRole->invoke($this->participantService, ['all']);
            $this->pass();
        } catch (BadRequest $e) {
            $this->fail();
        }
    }

    public function testGetParticipantByDefaultTermCode()
    {
        $this->request->method('getOptions')->willReturn([
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->method('checkAuthorization')->willReturn('all');
        $this->participantService->setApiUser($user);


        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();
        $this->assertEquals(
            [
                $this->instructorAssignmentPayload1,
                $this->courseSectionEnrollmentPayload1
            ],
            $actualResult
        );

    }

    public function testGetParticipantByDefaultTermCodeByCrn()
    {
        $this->request->method('getOptions')->willReturn([
            'crn' => ['12345'],
            'role' => ['student']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->getMockBuilder(\RESTng\Util\User::class)
            ->setMethods(array('checkAuthorization', 'getUsername'))
            ->getMock();
        $user->method('getUsername')->willReturn('liaom');
        $user->method('checkAuthorization')->willReturn(true);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();
        $this->assertEquals(
            [
                $this->courseSectionEnrollmentPayload1
            ],
            $actualResult
        );
    }

    public function testGetParticipantByDefaultTermCodeByCrnUniqueIdRoleStudent()
    {
        $this->request->method('getOptions')->willReturn([
            'crn' => ['12345'],
            'role' => ['student'],
            'uniqueId' => ['liaom']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->method('checkAuthorization')->willReturn(true);
        $this->participantService->setApiUser($user);
        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();
        $this->assertEquals(
            [
                $this->courseSectionEnrollmentPayload1
            ],
            $actualResult
        );
    }

    public function testGetParticipantByDefaultTermCodeByCrnUniqueIdRoleInstructor()
    {
        $this->request->method('getOptions')->willReturn([
            'crn' => ['12345'],
            'role' => ['instructor'],
            'uniqueId' => ['liaom']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->method('checkAuthorization')->willReturn(true);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();
        $this->assertEquals(
            [
                $this->instructorAssignmentPayload1
            ],
            $actualResult
        );
    }

    public function testParticipantWithStandardAccessCannotSeeOtherStudentSensitiveInformation()
    {
        $this->request->method('getOptions')->willReturn([
            'uniqueId' => ['liaom'],
            'role' => ['student']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(true);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();

        $this->assertEquals(
            [
                $this->courseSectionEnrollmentPayload1
            ],
            $actualResult
        );

    }

    public function testParticipantWithStandardAccessCannotSeeOtherInstructorSensitiveInformation()
    {
        $this->request->method('getOptions')->willReturn([
            'uniqueId' => ['liaom'],
            'role' => ['instructor']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(true);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();

        $this->assertEquals(
            [
                $this->instructorAssignmentPayload1
            ],
            $actualResult
        );

    }

    public function testCourseSectionNotFoundWhenLoadingInstructorAssignment()
    {
        $this->request->method('getOptions')->willReturn([
            'uniqueId' => ['liaom'],
            'role' => ['instructor']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(true);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([]));

        $this->expectException(\Exception::class);
        $this->expectExceptionMessageRegExp('/Course section not found.+/');

        $this->participantService->getParticipant()->getPayload();
    }

    public function testParticipantWithStandardAccessCannotSeeOtherSensitiveInformation()
    {
        $this->request->method('getOptions')->willReturn([
            'role' => ['all']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(true);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment2]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));

        $actualResult = $this->participantService->getParticipant()->getPayload();

        $this->assertEquals(
            [
                $this->instructorAssignmentPayload1,
                $this->courseSectionEnrollmentPayload2
            ],
            $actualResult
        );

    }

    public function testParticipantWithNoAccessCanSeeTokenUserWithStudentRole()
    {
        $this->request->method('getOptions')->willReturn([
            'role' => ['student']
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(false);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();

        $this->assertEquals(
            [
                $this->courseSectionEnrollmentPayload1,
            ],
            $actualResult
        );

    }


    public function testParticipantWithNoAccessCanSeeTokenUserWithInstructorRole()
    {
        $this->request->method('getOptions')->willReturn([
            'role' => ['instructor'],
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(false);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();

        $this->assertEquals(
            [
                $this->instructorAssignmentPayload1,
            ],
            $actualResult
        );

    }


    public function testParticipantWithNoAccessCanSeeTokenUser()
    {
        $this->request->method('getOptions')->willReturn([
            'role' => ['all'],
        ]);
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->currentTerm);
        $user = $this->createMock(User::class);
        $user->method('getUsername')->willReturn('liaom');
        $user->expects($this->at(1))
            ->method('checkAuthorization')
            ->willReturn(false);
        $user->expects($this->at(2))
            ->method('checkAuthorization')
            ->willReturn(false);
        $this->participantService->setApiUser($user);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection([$this->instructorAssignment1]));
        $this->viewCourseSectionEnrollmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1]));
        $this->viewCourseSectionService->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$this->courseSection1]));


        $actualResult = $this->participantService->getParticipant()->getPayload();

        $this->assertEquals(
            [
                $this->instructorAssignmentPayload1,
                $this->courseSectionEnrollmentPayload1

            ],
            $actualResult
        );

    }

    public function testQueryParameterValueDoesNotFound()
    {
        $reflectionRosterService = new ReflectionClass($this->participantService);
        $reflectionMethod = $reflectionRosterService->getMethod('getQueryParameterValueByKey');
        $reflectionMethod->setAccessible(true);

        $reflectionOptions = $reflectionRosterService->getProperty('options');
        $reflectionOptions->setAccessible(true);
        $reflectionOptions->setValue($this->participantService, []);

        $this->assertEquals(null,
            $reflectionMethod->invoke($this->participantService, 'a'));
    }

    public function testCourseSectionEnrollmentToArray()
    {
        $reflectionRosterService = new ReflectionClass($this->participantService);
        $reflectionMethod = $reflectionRosterService->getMethod('courseSectionEnrollmentToArray');
        $reflectionMethod->setAccessible(true);

        $actualResult = $reflectionMethod->invoke($this->participantService,
            $this->courseSectionEnrollment1, $this->courseSection1);
        $this->assertEquals($this->courseSectionEnrollmentPayload1, $actualResult);
    }

    public function testCourseSectionEnrollmentCollectionToArray()
    {
        $reflectionRosterService = new ReflectionClass($this->participantService);
        $reflectionMethod = $reflectionRosterService->getMethod('courseSectionEnrollmentCollectionToArray');
        $reflectionMethod->setAccessible(true);

        $this->viewCourseSectionService->method('getByGuids')->willReturn(
            new CourseSectionCollection([$this->courseSection1])
        );

        $actualResult = $reflectionMethod->invoke(
            $this->participantService,
            new CourseSectionEnrollmentCollection([$this->courseSectionEnrollment1])
        );

        $this->assertEquals([$this->courseSectionEnrollmentPayload1], $actualResult);
    }

    public function testInstructorAssignmentToArray()
    {
        $reflectionRosterService = new ReflectionClass($this->participantService);
        $reflectionMethod = $reflectionRosterService->getMethod('instructorAssignmentToArray');
        $reflectionMethod->setAccessible(true);

        $actualResult = $reflectionMethod->invoke($this->participantService,
            $this->instructorAssignment1, $this->courseSection1);

        $this->assertEquals($this->instructorAssignmentPayload1, $actualResult);
    }


}