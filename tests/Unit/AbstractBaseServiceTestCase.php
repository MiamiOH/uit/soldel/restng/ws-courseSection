<?php
/**
 * Author: liaom
 * Date: 5/2/18
 * Time: 4:23 PM
 */

namespace MiamiOH\CourseSectionWebService\Tests\Unit;

use Carbon\Carbon;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewCourseSectionAttributeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentDistributionService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentService;
use MiamiOH\Pike\App\Service\ViewCourseSectionScheduleService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\App\Service\ViewCrossListedCourseSectionService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCreditHoursDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionLevelDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionSummaryCollection;
use MiamiOH\Pike\Domain\Collection\CrossListedCourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\Model\CourseSectionCreditHoursDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentStatus;
use MiamiOH\Pike\Domain\Model\CourseSectionLevelDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\Model\CourseSectionSummary;
use MiamiOH\Pike\Domain\Model\CrossListedCourseSection;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Request;
use PHPUnit\Framework\MockObject\MockObject;

class AbstractBaseServiceTestCase extends TestCase
{
    /**
     * @var MockObject
     */
    protected $api;

    /**
     * @var MockObject
     */
    protected $request;

    /**
     * @var MockObject
     */
    protected $viewTermService;

    /**
     * @var MockObject
     */
    protected $viewCourseSectionEnrollmentService;

    /**
     * @var MockObject
     */
    protected $viewInstructorAssignmentService;

    /**
     * @var MockObject
     */
    protected $viewCourseSectionService;

    /**
     * @var MockObject
     */
    protected $viewCrossListedCourseSectionService;

    /**
     * @var MockObject
     */
    protected $pike;

    /**
     * @var MockObject
     */
    protected $pikeServiceFactory;

    /**
     * @var Term
     */
    protected $currentTerm;

    /**
     * @var PartOfTerm
     */
    protected $partOfTerm;

    /**
     * @var CourseSectionEnrollment
     */
    protected $courseSectionEnrollment1;

    /**
     * @var CourseSectionEnrollment
     */
    protected $courseSectionEnrollment2;

    /**
     * @var InstructorAssignment
     */
    protected $instructorAssignment1;

    /**
     * @var CourseSection
     */
    protected $courseSection1;

    /**
     * @var CourseSection
     */
    protected $courseSection4;

    /**
     * @var CourseSectionEnrollmentCount
     */
    protected $enrollmentCount1;

    /**
     * @var CourseSectionEnrollmentCountCollection
     */
    protected $courseSectionEnrollmentCountCollection1;

    /**
     * @var CrossListedCourseSection
     */
    protected $crossListedCourseSection;

    /**
     * @var CrossListedCourseSectionCollection
     */
    protected $crossListedCourseSectionCollection1;

    /**
     * @var CourseSectionAttributeCollection
     */
    protected $courseSectionAttributeCollection1;

    /**
     * @var CourseSectionScheduleCollection
     */
    protected $courseSectionScheduleCollection1;

    /**
     * @var array
     */
    protected $courseSectionEnrollmentPayload1;

    /**
     * @var array
     */
    protected $courseSectionEnrollmentPayload2;

    /**
     * @var array
     */
    protected $instructorAssignmentPayload1;

    /**
     * @var array
     */
    protected $courseSectionPayload1;

    /**
     * @var array
     */
    protected $courseSectionPayload4;

    /**
     * @var array
     */
    protected $enrollmentCountPayload;

    /**
     * @var array
     */
    protected $courseSectionEnrollmentStandardPayload1;

    /**
     * @var array
     */
    protected $courseSectionEnrollmentStandardPayload2;

    /**
     * @var array
     */
    protected $person1Data;

    protected $person1;

    protected $assignment1;

    protected $assignment1Data;
    /*
     * @var CourseSectionAttribute
     */
    protected $attribute;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionAttributeService;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionScheduleService;

    /**
     * @var array
     */
    protected $attributesPayload;

    /**
     * @var array
     */
    protected $schedulePayload;

    /**
     * @var array
     */
    protected $crossListedCourseSectionePayload;

    /**
     * @var CourseSectionSchedule
     */
    private $schedule;


    protected $guid;
    /**
     * @var MockObject
     */
    protected $viewCourseSectionEnrollmentDistributionService;
    /**
     * @var CourseSectionEnrollmentDistributionCollection
     */
    protected $courseSectionEnrollmentDistributionCollection;
    /**
     * @var CourseSectionEnrollmentDistribution
     */
    protected $courseSectionEnrollmentDistribution;
    /**
     * @var CourseSectionCreditHoursDistributionCollection
     */
    protected $courseSectionCreditHoursDistributionCollection;

    /**
     * @var CourseSectionLevelDistributionCollection
     */
    protected $courseSectionLevelDistributionCollection;

    /**
     * @var CourseSectionSummaryCollection
     */
    protected $courseSectionSummaryCollection;
    /**
     * @var CourseSectionCreditHoursDistribution
     */
    protected $courseSectionCreditHoursDistribution;
    /**
     * @var CourseSectionLevelDistribution
     */
    protected $courseSectionLevelDistribution;

    /**
     * @var CourseSectionSummary
     */
    protected $courseSectionSummary;
    /**
     * @var array
     */
    protected $courseSectionEnrollmentDistributionPayload;

    /** @var string  */
    protected $classListEnrollmentPayload1;
    /** @var string  */
    protected $classListInstructorPayload1;

    protected function pass()
    {
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->request = $this->createMock(Request::class);

        $this->viewTermService = $this->createMock(ViewTermService::class);
        $this->viewInstructorAssignmentService = $this->createMock(ViewInstructorAssignmentService::class);
        $this->viewCourseSectionEnrollmentService = $this->createMock(ViewCourseSectionEnrollmentService::class);
        $this->viewCourseSectionService = $this->createMock(ViewCourseSectionService::class);
        $this->viewCourseSectionAttributeService = $this->createMock(ViewCourseSectionAttributeService::class);
        $this->viewCourseSectionScheduleService = $this->createMock(ViewCourseSectionScheduleService::class);
        $this->viewCrossListedCourseSectionService = $this->createMock(ViewCrossListedCourseSectionService::class);
        $this->viewCourseSectionEnrollmentDistributionService = $this->createMock(ViewCourseSectionEnrollmentDistributionService::class);


        $this->pike = $this->createMock(AppMapper::class);
        $this->pike->method('getViewTermService')->willReturn($this->viewTermService);
        $this->pike->method('getViewInstructorAssignmentService')->willReturn($this->viewInstructorAssignmentService);
        $this->pike->method('getViewCourseSectionEnrollmentService')->willReturn($this->viewCourseSectionEnrollmentService);
        $this->pike->method('getViewCourseSectionService')->willReturn($this->viewCourseSectionService);
        $this->pike->method('getViewCourseSectionScheduleService')->willReturn($this->viewCourseSectionScheduleService);
        $this->pike->method('getViewCourseSectionAttributeService')->willReturn($this->viewCourseSectionAttributeService);
        $this->pike->method('getViewCrossListedCourseSectionService')->willReturn($this->viewCrossListedCourseSectionService);
        $this->pike->method('getViewCourseSectionEnrollmentDistributionService')->willReturn($this->viewCourseSectionEnrollmentDistributionService);


        $this->pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($this->pike);

        $this->partOfTerm = new PartOfTerm(
            new PartOfTermCode('1'),
            'Full Semester',
            Carbon::create(2017, 1, 1, 0, 0, 0),
            Carbon::create(2017, 5, 1, 0, 0, 0)
        );

        $this->currentTerm = new Term(
            new TermCode('201810'),
            'asdklsd',
            Carbon::create(2017, 8, 24, 0, 0, 0),
            Carbon::create(2017, 12, 12, 0, 0, 0),
            true
        );
        $this->guid = CourseSectionGuid::create();


        $this->instructorAssignment1 = new InstructorAssignment(
            new InstructorAssignmentGuid($this->guid),
            new UniqueId('liaom'),
            new TermCode('201810'),
            new Crn('12345'),
            new CourseSectionGuid($this->guid),
            true
        );

        $this->instructorAssignmentPayload1 = [
            'termCode' => '201810',
            'uniqueId' => 'liaom',
            'courseSectionGuid' => $this->guid,
            'studentLevel' => null,
            'creditHours' => null,
            'hasAttended' => null,
            'lastAttendDate' => null,
            'enrollmentStatusCode' => null,
            'enrollmentStatusDescription' => null,
            'isEnrollmentActive' => null,
            'isMidtermGradeRequired' => null,
            'isFinalGradeRequired' => null,
            'isGradeSubmissionEligible' => null,
            'gradeSubmissionEligibleComment' => null,
            'grades' => null,
            //    'midtermGrade' => null,
            //   'finalGrade' => null,
            'role' => 'instructor',
            'crn' => '12345',
            'title' => 't1'
        ];

        $this->courseSection1 = new CourseSection(
            new CourseSectionGuid($this->guid),
            new Course(
                new CourseGuid($this->guid),
                new TermCode('201710'),
                new SubjectCode('CSE'),
                new CourseNumber('174'),
                new TermCode('201610'),
                't1',
                'a',
                new SchoolCode('AO'),
                'ab',
                new DepartmentCode('CSE'),
                'bc',
                'aa',
                3,
                3,
                2,
                2,
                2,
                1,
                1,
                null,
                '3',
                []
            ),
            'course section 1 title',
            new TermCode('201810'),
            new Crn('12345'),
            'aa',
            'A',
            '3',
            '3',
            '3',
            [3],
            new InstructionalTypeCode('A'),
            'dd',
            new CampusCode('O'),
            'Oxford',
            new CourseSectionStatusCode('A'),
            'active',
            24,
            $this->createMock(PartOfTerm::class),
            true,
            true,
            true,
            true,
            new DivisionCode('CAS'),
            'Name',
            new DepartmentCode('ACC'),
            'Name',
            new DepartmentCode('ACC'),
            'name'
        );

        $this->courseSection4 = new CourseSection(
            new CourseSectionGuid($this->guid),
            new Course(
                new CourseGuid($this->guid),
                new TermCode('201610'),
                new SubjectCode('CSE'),
                new CourseNumber('174'),
                new TermCode('201810'),
                't1',
                'Spring Semester 2017-18',
                new SchoolCode('AO'),
                'Division Name',
                new DepartmentCode('CSE'),
                'Department Name',
                'aa',
                3,
                3,
                2,
                2,
                2,
                1,
                1,
                null,
                '1 to 3',
                []
            ),
            'course section 4 title',
            new TermCode('201810'),
            new Crn('12345'),
            'Spring Semester 2017-18',
            'A',
            '3',
            '3',
            '3',
            [3],
            new InstructionalTypeCode('L'),
            'Lecture',
            new CampusCode('O'),
            'Oxford',
            new CourseSectionStatusCode('A'),
            'Active',
            24,
            $this->partOfTerm,
            true,
            true,
            true,
            true,
            new DivisionCode('FSB'),
            'Farmer School of Business',
            new DepartmentCode('ACC'),
            'Accountancy',
            new DepartmentCode('ACC'),
            'Accountancy'
        );

        $this->enrollmentCount1 = new CourseSectionEnrollmentCount(
            new CourseSectionGuid($this->guid),
            5,
            5,
            5,
            5

        );

        $this->courseSectionEnrollmentCountCollection1 = new CourseSectionEnrollmentCountCollection();
        $this->courseSectionEnrollmentCountCollection1->push($this->enrollmentCount1);


        $this->attribute = new CourseSectionAttribute(
            new CourseSectionAttributeCode('CLS'),
            'class',
            new CourseSectionGuid($this->guid)
        );
        $this->courseSectionAttributeCollection1 = new CourseSectionAttributeCollection();
        $this->courseSectionAttributeCollection1->push($this->attribute);

        $this->schedule = new CourseSectionSchedule(
            new CourseSectionGuid($this->guid),
            Carbon::create(2018, 6, 15, 0, 0, 0),
            Carbon::create(2018, 6, 15, 0, 0, 0),
            Carbon::create(2018, 6, 15, 8, 30, 0),
            Carbon::create(2018, 6, 15, 9, 30, 0),
            new CourseSectionScheduleDays('M'),
            '103',
            new BuildingCode('14567'),
            'cse',
            new CourseSectionScheduleTypeCode("SFDF"),
            '123'
        );
        $this->courseSectionScheduleCollection1 = new CourseSectionScheduleCollection();
        $this->courseSectionScheduleCollection1->push($this->schedule);

        $this->crossListedCourseSection = new CrossListedCourseSection(
            new Crn('12345'),
            new SubjectCode('ABC'),
            'A',
            new CourseNumber('123'),
            new CourseSectionGuid($this->guid),
            new CourseSectionGuid($this->guid)
        );

        $this->crossListedCourseSectionCollection1 = new CrossListedCourseSectionCollection();
        $this->crossListedCourseSectionCollection1->push($this->crossListedCourseSection);

        $this->courseSectionCreditHoursDistribution = new CourseSectionCreditHoursDistribution(
            1.5,
            2,
            3
        );
        $this->courseSectionCreditHoursDistributionCollection = new CourseSectionCreditHoursDistributionCollection();
        $this->courseSectionCreditHoursDistributionCollection->push($this->courseSectionCreditHoursDistribution);

        $this->courseSectionLevelDistribution = new CourseSectionLevelDistribution(
            '1',
            '2',
            3,
            4
        );
        $this->courseSectionLevelDistributionCollection = new CourseSectionLevelDistributionCollection();
        $this->courseSectionLevelDistributionCollection->push($this->courseSectionLevelDistribution);


        $this->courseSectionSummary = new CourseSectionSummary(
            "123",
            2,
            3
        );
        $this->courseSectionSummaryCollection = new CourseSectionSummaryCollection();
        $this->courseSectionSummaryCollection->push($this->courseSectionSummary);



        $this->courseSectionEnrollmentDistribution = new CourseSectionEnrollmentDistribution(
            new CourseSectionGuid($this->guid),
            $this->courseSectionCreditHoursDistributionCollection,
            $this->courseSectionLevelDistributionCollection,
            $this->courseSectionSummaryCollection
        );

        $this->courseSectionEnrollmentDistributionCollection = new CourseSectionEnrollmentDistributionCollection();
        $this->courseSectionEnrollmentDistributionCollection->push($this->courseSectionEnrollmentDistribution);
        $this->courseSectionEnrollment1 = new CourseSectionEnrollment(
            true,
            true,
            true,
            'aaa',
            false,
            'true',
            new GradeCollection([
                new Grade(new GradeValue('A'), new GradeModeCode('S'), GradeType::ofMidterm(), new CourseSectionGuid($this->guid),
                    new Crn('1234'), new TermCode('201720'), new UniqueId('liaom'))
            ]),
            new UniqueId('liaom'),
            new CourseSectionEnrollmentStatus(
                new CourseSectionEnrollmentStatusCode('AU'),
                'asfd'
            ),
            new CourseSectionGuid($this->guid),
            3,
            new StudentLevelCode('UG'),
            new TermCode('201810'),
            new GradeModeCode('S')
        );

        $this->courseSectionEnrollment2 = new CourseSectionEnrollment(
            true,
            true,
            true,
            'aaa',
            false,
            'true',
            new GradeCollection([
                new Grade(new GradeValue('A'), new GradeModeCode('S'), GradeType::ofMidterm(), new CourseSectionGuid($this->guid),
                    new Crn('1234'), new TermCode('201720'), new UniqueId('liaom'))
            ]),
            new UniqueId('xiaw'),
            new CourseSectionEnrollmentStatus(
                new CourseSectionEnrollmentStatusCode('AU'),
                'asfd'
            ),
            new CourseSectionGuid($this->guid),
            3,
            new StudentLevelCode('UG'),
            new TermCode('201810'),
            new GradeModeCode('S')
        );

        $this->courseSectionEnrollmentPayload1 = [
            'termCode' => '201810',
            'courseSectionGuid' => $this->guid,
            'studentLevel' => 'UG',
            'creditHours' => 3,
            'hasAttended' => true,
            'lastAttendDate' => null,
            'enrollmentStatusCode' => 'AU',
            'enrollmentStatusDescription' => 'asfd',
            'isEnrollmentActive' => true,
            'isMidtermGradeRequired' => true,
            'isFinalGradeRequired' => false,
            'isGradeSubmissionEligible' => true,
            'gradeSubmissionEligibleComment' => 'aaa',
            'grades' => [
                [
                    'value' => 'A',
                    'gradeModeCode' => 'S',
                    'type' => 'midterm'
                ]
            ],
            'uniqueId' => 'liaom',
            'role' => 'student',
            'crn' => '12345',
            'title' => 't1'
        ];

        $this->courseSectionEnrollmentPayload2 = [
            'termCode' => '201810',
            'courseSectionGuid' => $this->guid,
            'studentLevel' => 'UG',
            'creditHours' => 3,
            'hasAttended' => true,
            'lastAttendDate' => null,
            'enrollmentStatusCode' => 'AU',
            'enrollmentStatusDescription' => 'asfd',
            'isEnrollmentActive' => true,
            'isMidtermGradeRequired' => true,
            'isFinalGradeRequired' => false,
            'isGradeSubmissionEligible' => true,
            'gradeSubmissionEligibleComment' => 'aaa',
            'grades' => [
                [
                    'value' => 'A',
                    'gradeModeCode' => 'S',
                    'type' => 'midterm'
                ]
            ],
            'uniqueId' => 'xiaw',
            'role' => 'student',
            'crn' => '12345',
            'title' => 't1'
        ];

        $this->courseSectionEnrollmentStandardPayload1 = [
            'termCode' => '201810',
            'courseSectionGuid' => $this->guid,
            'studentLevel' => 'UG',
            'uniqueId' => 'liaom',
            'role' => 'student',
            'crn' => '12345',
            'title' => 't1'
        ];

        $this->courseSectionEnrollmentStandardPayload2 = [
            'termCode' => '201810',
            'courseSectionGuid' => $this->guid,
            'studentLevel' => 'UG',
            'uniqueId' => 'xiaw',
            'role' => 'student',
            'crn' => '12345',
            'title' => 't1'
        ];

        $this->courseSectionPayload4 = [
            'termCode' => '201810',
            'termDescription' => 'Spring Semester 2017-18',
            'crn' => '12345',
            'courseSectionGuid' => (string)$this->guid,
            'sectionName' => 'CSE 174 A',
            'instructionTypeCode' => 'L',
            'instructionTypeDescription' => 'Lecture',
            'courseSectionCode' => 'A',
            'courseSectionStatusCode' => 'A',
            'courseSectionStatusDescription' => 'Active',
            'campusCode' => 'O',
            'campusName' => 'Oxford',
            'creditHoursDescription' => '3',
            'creditHoursLow' => 3.0,
            'creditHoursHigh' => 3.0,
            'isMidtermGradeSubmissionAvailable' => true,
            'isFinalGradeSubmissionAvailable' => true,
            'isFinalGradeRequired' => true,
            'standardizedDivisionCode' => 'FSB',
            'standardizedDivisionName' => 'Farmer School of Business',
            'standardizedDepartmentCode' => 'ACC',
            'standardizedDepartmentName' => 'Accountancy',
            'legacyStandardizedDepartmentCode' => 'ACC',
            'legacyStandardizedDepartmentName' => 'Accountancy',
            'isDisplayed' => true,
            'course' => array(
                'schoolCode' => 'AO',
                'schoolName' => 'Division Name',
                'departmentCode' => 'CSE',
                'departmentName' => 'Department Name',
                'title' => 't1',
                'subjectCode' => 'CSE',
                'subjectDescription' => 'aa',
                'number' => '174',
                'lectureHoursDescription' => '1 to 3',
                'labHoursDescription' => null,
                'creditHoursHigh' => 3.0,
                'creditHoursLow' => 3.0,
                'lectureHoursHigh' => 2.0,
                'lectureHoursLow' => 2.0,
                'labHoursHigh' => 1.0,
                'labHoursLow' => 2.0,
                'description' => 'Spring Semester 2017-18'
            ),
            'partOfTerm' => array(
                'code' => '1',
                'name' => 'Full Semester',
                'startDate' => '2017-01-01',
                'endDate' => '2017-05-01'
            ),
            'creditHoursAvailable' => ['3'],
            'title' => 'course section 4 title'
        ];

        $this->enrollmentCountPayload = [
            'enrollmentCount' => array(
                'numberOfMax' => 5,
                'numberOfCurrent' => 5,
                'numberOfActive' => 5,
                'numberOfAvailable' => 5
            )
        ];

        $this->person1Data = [
            'getUniqueId' => new UniqueId('aaaabb'),
            'getLastName' => 'Asdf',
            'getFirstName' => 'DSs',
            'getMiddleName' => 'D.',
            'getPrefix' => 'Dr.',
            'getSuffix' => null,
            'getPreferredFirstName' => 'Dia',
            'getInformalDisplayedName' => 'Ooal Dlsk',
            'getFormalDisplayedName' => 'Larem Drr',
            'getInformalSortedName' => 'DD',
            'getFormalSortedName' => 'A B',
        ];
        $this->person1 = $this->createMock(Person::class);
        $this->person1->method('getUniqueId')->willReturn($this->person1Data['getUniqueId']);
        $this->person1->method('getLastName')->willReturn($this->person1Data['getLastName']);
        $this->person1->method('getFirstName')->willReturn($this->person1Data['getFirstName']);
        $this->person1->method('getMiddleName')->willReturn($this->person1Data['getMiddleName']);
        $this->person1->method('getPrefix')->willReturn($this->person1Data['getPrefix']);
        $this->person1->method('getSuffix')->willReturn($this->person1Data['getSuffix']);
        $this->person1->method('getPreferredFirstName')->willReturn($this->person1Data['getPreferredFirstName']);
        $this->person1->method('getInformalDisplayedName')->willReturn($this->person1Data['getInformalDisplayedName']);
        $this->person1->method('getFormalDisplayedName')->willReturn($this->person1Data['getFormalDisplayedName']);
        $this->person1->method('getInformalSortedName')->willReturn($this->person1Data['getInformalSortedName']);
        $this->person1->method('getFormalSortedName')->willReturn($this->person1Data['getFormalSortedName']);

        $this->assignment1Data = [
            'isPrimary' => true
        ];

        $this->assignment1 = $this->createMock(InstructorAssignment::class);
        $this->assignment1->method('isPrimary')->willReturn($this->assignment1Data['isPrimary']);
        $this->attributesPayload = array(
            'attributes' => array(
                array(
                    'code' => 'CLS',
                    'description' => 'class'
                )
            )
        );

        $this->schedulePayload = array(
            'schedules' => array(
                array(
                    'startDate' => '2018-06-15',
                    'endDate' => '2018-06-15',
                    'startTime' => '08:30',
                    'endTime' => '09:30',
                    'roomNumber' => '103',
                    'buildingCode' => '14567',
                    'buildingName' => 'cse',
                    'days' => 'M',
                    'scheduleTypeCode' => 'SFDF',
                    'scheduleTypeDescription' => '123',
                )
            )
        );

        $this->crossListedCourseSectionePayload = array(
            'crossListedCourseSections' => array(
                array(
                    'crn' => '12345',
                    'courseSectionCode' => 'A',
                    'sectionName' => 'ABC 123 A',
                    'courseSubjectCode' => 'ABC',
                    'courseNumber' => '123',
                    'courseSectionGuid' => $this->guid,
                )
            )
        );

        $this->courseSectionEnrollmentDistributionPayload = array(
            'enrollmentDistribution' => array(
                'guid' => $this->guid,
                'levelDistribution' => array(
                    array(
                        'code' => '1',
                        'description' => '2',
                        'numberOfCurrentEnrollment' => 3,
                        'numberOfActiveEnrollment' => 4
                    )
                ),
                'creditHoursDistribution' => array(
                    array(
                        'hours' => 1.5,
                        'numberOfCurrentEnrollment' => 2,
                        'numberOfActiveEnrollment' => 3,
                    )
                ),
                'summaries' => array(
                    array(
                        'description' => 123,
                        'numberOfCurrentEnrollment' => 2,
                        'numberOfActiveEnrollment' => 3,
                    )
                )
            )
        );

        $this->classListEnrollmentPayload1 = [
            'termCode' => '201810',
            'courseSectionGuid' => (string) $this->guid,
            'uniqueId' => 'liaom',
            'role' => 'student',
            'crn' => '12345',
            'title' => 't1',
            'sectionCode' => 'A',
            'subjectCode' => 'CSE',
            'number' => '174',
            'sectionName' => 'CSE 174 A',
            'instructionalTypeCode' => 'A',
            'instructionalType' => 'dd',
            'campusCode' => 'O',
            'campus' => 'Oxford',
        ];

        $this->classListInstructorPayload1 = [
            'termCode' => '201810',
            'uniqueId' => 'liaom',
            'courseSectionGuid' => (string) $this->guid,
            'role' => 'instructor',
            'crn' => '12345',
            'title' => 't1',
            'sectionCode' => 'A',
            'subjectCode' => 'CSE',
            'number' => '174',
            'sectionName' => 'CSE 174 A',
            'instructionalTypeCode' => 'A',
            'instructionalType' => 'dd',
            'campusCode' => 'O',
            'campus' => 'Oxford',
        ];
    }
}