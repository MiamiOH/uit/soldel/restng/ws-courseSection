<?php

namespace MiamiOH\CourseSectionWebService\Tests\Unit;

use Carbon\Carbon;
use MiamiOH\CourseSectionWebService\Services\CourseSectionService;
use MiamiOH\Pike\Domain\Collection\CourseNumberCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use ReflectionClass;

/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 6/4/18
 * Time: 08:05 AM
 */
class  CourseSectionServiceTest extends AbstractBaseServiceTestCase
{
    /**
     * @var \MiamiOH\Pike\Domain\ValueObject\TermCode
     */
    private $termCode;
    /**
     * @var CourseSectionService
     */
    private $courseSectionService;
    /**
     * @var SearchCourseSectionRequest
     */
    private $searchCourseSectionRequest;
    /**
     * @var CourseSectionGuid
     */
    private $curseSectionGuid;

    protected function setUp(): void
    {
        parent::setUp();
        $this->courseSectionService = new CourseSectionService();

        $this->courseSectionService->setPikeServiceFactory($this->pikeServiceFactory);
        $this->courseSectionService->setRequest($this->request);
        $this->searchCourseSectionRequest = new SearchCourseSectionRequest();
    }

    public function testGetCourseSectionByCourseSectionGuidFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'courseSectionGuids' => [$this->guid]
        ]);
        $this->request->method('getLimit')->willReturn(3);

        $this->request->method('getSubObjects')->willReturn([]);

        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));

        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();

        $this->assertEquals([$this->courseSectionPayload4], $actualResult);

    }

    public function testCanSetFilters(){
        $filters = $this->courseSectionService->setFilters([
            'courseSectionGuid' => [new CourseSectionGuid($this->guid)],
            'campusCode' => ['O'],
            'termCode' => ['201810'],
            'crn' => ['12345'],
            'course_subjectCode' => ['CSE'],
            'course_number' => ['345'],
            'courseSectionCode' => ['L'],
            'courseSectionStatusCode' => ['A'],
            'enrollmentCount_numberOfMax' => '2 to 5',
            'enrollmentCount_numberOfCurrent' => '2 to 5',
            'enrollmentCount_numberOfActive' => '2 to 5',
            'enrollmentCount_numberOfAvailable' => '2 to 5',
            'partOfTerm_startDate' => '2017-01-01 to 2017-01-10',
            'partOfTerm_endDate' => '2017-02-01 to 2017-02-10',
            'schedules_startDate' => '2017-03-01 to 2017-03-10',
            'schedules_endtDate' => '2017-04-01 to 2017-04-10',
            'schedules_startTime' => '11:01 to 12:01',
            'schedules_endTime' => '13:01 to 14:01',
            'schedules_buildingCode' => ['ABC'],
            'schedules_roomNumber' => ['123'],
            'schedules_days' => 'M',
            'instructors_uniqueId' => ['aaaaa'],
            'isMidtermGradeSubmissionAvailable' => 'true',
            'isFinalGradeSubmissionAvailable' => 'true',
            'hasSeatAvailable' => 'true',
            'partOfTerm_code' => ['1'],
            'limit' => 3,
            'offset' => 3,
        ]);

        $this->assertEquals($this->guid,$filters->getCourseSectionGuidCollection()->get(0));
        $this->assertEquals('O',$filters->getCampusCodeCollection()->get(0));
        $this->assertEquals('201810',$filters->getTermCodeCollection()->get(0));
        $this->assertEquals('12345',$filters->getCrnCollection()->get(0));
        $this->assertEquals('CSE',$filters->getSubjectCodeCollection()->get(0));
        $this->assertEquals('345',$filters->getCourseNumberCollection()->get(0));
        $this->assertEquals(['L'],$filters->getCourseSectionCodes(0));
        $this->assertEquals('2',$filters->getNumOfMaxEnrollments());
        $this->assertEquals('5',$filters->getNumOfMaxEnrollmentsCount());
        $this->assertEquals('2',$filters->getNumOfCurrentEnrollments());
        $this->assertEquals('5',$filters->getNumOfCurrentEnrollmentsCount());
        $this->assertEquals('2',$filters->getNumOfActiveEnrollments());
        $this->assertEquals('5',$filters->getNumOfActiveEnrollmentsCount());
        $this->assertEquals('2',$filters->getNumOfAvailableEnrollments());
        $this->assertEquals('5',$filters->getNumOfAvailableEnrollmentsCount());
        $this->assertEquals(Carbon::createFromFormat('Y-m-d H:i', '2017-01-01 00:00'),$filters->getPartOfTermStartDate());
        $this->assertEquals(Carbon::createFromFormat('Y-m-d H:i', '2017-01-10 00:00'),$filters->getPartOfTermStartDateEnd());
        $this->assertEquals(Carbon::createFromFormat('Y-m-d H:i', '2017-02-01 00:00'),$filters->getPartOfTermEndDate());
        $this->assertEquals(Carbon::createFromFormat('Y-m-d H:i', '2017-02-10 00:00'),$filters->getPartOfTermEndDateEnd());
        $this->assertEquals(Carbon::createFromFormat('Y-m-d H:i', '2017-03-01 00:00'),$filters->getScheduleStartDate());
        $this->assertEquals(Carbon::createFromFormat('Y-m-d H:i', '2017-03-10 00:00'),$filters->getScheduleStartDateEnd());
        $this->assertEquals(Carbon::createFromFormat('H:i', '11:01'),$filters->getScheduleStartTime());
        $this->assertEquals(Carbon::createFromFormat('H:i', '12:01'),$filters->getScheduleStartTimeEnd());
        $this->assertEquals(Carbon::createFromFormat('H:i', '13:01'),$filters->getScheduleEndTime());
        $this->assertEquals(Carbon::createFromFormat('H:i', '14:01'),$filters->getScheduleEndTimeEnd());
        $this->assertEquals('ABC',$filters->getScheduleBuildingCodeCollection()->get(0));
        $this->assertEquals(['123'],$filters->getScheduleRooms());
        $this->assertEquals('M',$filters->getScheduleDays());
        $this->assertEquals('aaaaa',$filters->getInstructorUniqueIdCollection()->get(0));
        $this->assertTrue($filters->isMidtermGradeSubmissionAvailable());
        $this->assertTrue($filters->isFinalGradeSubmissionAvailable());
        $this->assertTrue($filters->getHasSeatAvailable());
        $this->assertEquals('1',$filters->getPartOfTermCodeCollection()->get(0));
        $this->assertEquals(3,$filters->getLimit());
        $this->assertEquals(2,$filters->getOffset());
    }

    public function testCanSetFiltersFalse (){
        $filters = $this->courseSectionService->setFilters([
            'isMidtermGradeSubmissionAvailable' => 'false',
            'isFinalGradeSubmissionAvailable' => 'false',
            'hasSeatAvailable' => 'false',
        ]);
        $this->assertFalse($filters->isMidtermGradeSubmissionAvailable());
        $this->assertFalse($filters->isFinalGradeSubmissionAvailable());
        $this->assertFalse($filters->getHasSeatAvailable());
    }

    public function testCannotSetFilters()
    {
        try {
            $this->courseSectionService->setFilters([
                'isMidtermGradeSubmissionAvailable' => 'notTrue'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid isMidtermGradeSubmissionAvailable', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'isFinalGradeSubmissionAvailable' => 'notTrue'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid isFinalGradeSubmissionAvailable', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'hasSeatAvailable' => 'notTrue'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid hasSeatAvailable', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'enrollmentCount_numberOfMax' => -2
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid numOfMaxEnrollments', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'enrollmentCount_numberOfCurrent' => -2
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid numOfCurrentEnrollments', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'enrollmentCount_numberOfActive' => -2
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid numOfActiveEnrollments', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'enrollmentCount_numberOfAvailable' => -2
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid numOfAvailableEnrollments', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'partOfTerm_startDate' => '2017/01/01 to 2017-01-10'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid partOfTermStartDate', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'partOfTerm_endDate' => '2017/01/01 to 2017/01/10'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid partOfTermEndDate', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'schedules_startDate' => '2017/01/01 to 2017/01/10'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid scheduleStartDate', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'schedules_endDate' => '2017/01/01 to 2017/01/10'
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid scheduleEndDate', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'schedules_startTime' => '1101 to 1201',
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid scheduleStartTime', $e->getMessage());
        }
        try {
            $this->courseSectionService->setFilters([
                'schedules_endTime' => '1101 to 1201',
            ]);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid scheduleEndTime', $e->getMessage());
        }
    }

    public function testGetCourseSectionByCourseSectionGuidWithPartOfTermStartDateFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'term_code' => ['201810'],
            'crn' => ['12345'],
            'partOfTerm_startDate' => '2017-01-01 to 2017-01-10'
        ]);
        $this->request->method('getSubObjects')->willReturn([]);
        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));
        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();
        $this->assertEquals([$this->courseSectionPayload4], $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithPartOfTermEndDateFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'term_code' => ['201810'],
            'crn' => ['12345'],
            'partOfTerm_endDate' => '2017-04-01 to 2017-05-10'
        ]);
        $this->request->method('getSubObjects')->willReturn([]);
        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));
        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();
        $this->assertEquals([$this->courseSectionPayload4], $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithScheduleStartDateFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'term_code' => ['201810'],
            'crn' => ['12345'],
            'schedules_startDate' => '2017-06-01 to 2017-06-30'
        ]);
        $this->request->method('getSubObjects')->willReturn([
            'schedules' => []
        ]);
        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));
        $this->viewCourseSectionScheduleService->method('getCollectionByCourseSectionGuidCollection')
            ->willReturn($this->courseSectionScheduleCollection1);
        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();
        $this->assertEquals([array_merge($this->courseSectionPayload4, $this->schedulePayload)], $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithScheduleEndDateFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'term_code' => ['201810'],
            'crn' => ['12345'],
            'schedules_endDate' => '2017-06-01 to 2017-06-30'
        ]);
        $this->request->method('getSubObjects')->willReturn([
            'schedules' => []
        ]);
        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));
        $this->viewCourseSectionScheduleService->method('getCollectionByCourseSectionGuidCollection')
            ->willReturn($this->courseSectionScheduleCollection1);
        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();
        $this->assertEquals([array_merge($this->courseSectionPayload4, $this->schedulePayload)], $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithScheduleStartTimeFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'term_code' => ['201810'],
            'crn' => ['12345'],
            'schedules_startTime' => '08:00 to 09:00'
        ]);
        $this->request->method('getSubObjects')->willReturn([
            'schedules' => []
        ]);
        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));
        $this->viewCourseSectionScheduleService->method('getCollectionByCourseSectionGuidCollection')
            ->willReturn($this->courseSectionScheduleCollection1);
        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();
        $this->assertEquals([array_merge($this->courseSectionPayload4, $this->schedulePayload)], $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithScheduleEndTimeFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'term_code' => ['201810'],
            'crn' => ['12345'],
            'schedules_endTime' => '09:00 to 10:30'
        ]);
        $this->request->method('getSubObjects')->willReturn([
            'schedules' => []
        ]);
        $this->viewCourseSectionService->method('searchCourseSection')
            ->willReturn(new CourseSectionCollection([$this->courseSection4]));
        $this->viewCourseSectionScheduleService->method('getCollectionByCourseSectionGuidCollection')
            ->willReturn($this->courseSectionScheduleCollection1);
        $actualResult = $this->courseSectionService->getCourseSection()->getPayload();
        $this->assertEquals([array_merge($this->courseSectionPayload4, $this->schedulePayload)], $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuid()
    {
        $this->request->method('getResourceParam')->willReturn($this->guid);
        $this->request->method('getSubObjects')->willReturn([]);

        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->courseSection4);

        $actualResult = $this->courseSectionService->getCourseSectionGuid()->getPayload();

        $this->assertEquals($this->courseSectionPayload4, $actualResult);

    }

    public function testGetCourseSectionByCourseSectionGuidWithEnrollmentCount()
    {
        $this->request->method('getResourceParam')->willReturn($this->guid);
        $this->request->method('getSubObjects')->willReturn([
            'enrollmentCount' => []
        ]);

        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->courseSection4);

        $this->viewCourseSectionService->method('getEnrollmentCountBysectionGuids')
            ->willReturn($this->courseSectionEnrollmentCountCollection1);

        $actualResult = $this->courseSectionService->getCourseSectionGuid()->getPayload();
        $this->assertEquals(array_merge($this->courseSectionPayload4,
            $this->enrollmentCountPayload), $actualResult);

    }

    public function testPersonToArray()
    {
        $reflectionService = new ReflectionClass($this->courseSectionService);
        $reflectionMethod = $reflectionService->getMethod('personToArray');
        $reflectionMethod->setAccessible(true);

        $this->assertEquals(
            [
                'uniqueId' => (string)$this->person1Data['getUniqueId'],
                'lastName' => $this->person1Data['getLastName'],
                'firstName' => $this->person1Data['getFirstName'],
                'middleName' => $this->person1Data['getMiddleName'],
                'prefix' => $this->person1Data['getPrefix'],
                'suffix' => $this->person1Data['getSuffix'],
                'preferredFirstName' => $this->person1Data['getPreferredFirstName'],
                'informalDisplayedName' => $this->person1Data['getInformalDisplayedName'],
                'formalDisplayedName' => $this->person1Data['getFormalDisplayedName'],
                'informalSortedName' => $this->person1Data['getInformalSortedName'],
                'formalSortedName' => $this->person1Data['getFormalSortedName'],
            ],
            $reflectionMethod->invoke(
                $this->courseSectionService,
                $this->person1
            )
        );


    }

    public function testAssignmentToArray()
    {
        $reflectionService = new ReflectionClass($this->courseSectionService);
        $reflectionMethod = $reflectionService->getMethod('assignmentToArray');
        $reflectionMethod->setAccessible(true);
        $this->assertEquals(
            [
                'isPrimary' => $this->assignment1Data['isPrimary'],
                'person' => [
                    'uniqueId' => (string)$this->person1Data['getUniqueId'],
                    'lastName' => $this->person1Data['getLastName'],
                    'firstName' => $this->person1Data['getFirstName'],
                    'middleName' => $this->person1Data['getMiddleName'],
                    'prefix' => $this->person1Data['getPrefix'],
                    'suffix' => $this->person1Data['getSuffix'],
                    'preferredFirstName' => $this->person1Data['getPreferredFirstName'],
                    'informalDisplayedName' => $this->person1Data['getInformalDisplayedName'],
                    'formalDisplayedName' => $this->person1Data['getFormalDisplayedName'],
                    'informalSortedName' => $this->person1Data['getInformalSortedName'],
                    'formalSortedName' => $this->person1Data['getFormalSortedName']
                ]
            ],
            $reflectionMethod->invoke(
                $this->courseSectionService,
                $this->assignment1,
                $this->person1
            )
        );
    }

    public function testGetCourseSectionByCourseSectionGuidWithAttributes()
    {
        $this->request->method('getResourceParam')->willReturn($this->guid);
        $this->request->method('getSubObjects')->willReturn([
            'attributes' => []]);
        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->courseSection4);
        $this->viewCourseSectionAttributeService->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->courseSectionAttributeCollection1);
        $actualResult = $this->courseSectionService->getCourseSectionGuid()->getPayload();
        $this->assertEquals(array_merge($this->courseSectionPayload4, $this->attributesPayload), $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithSchedules()
    {
        $this->request->method('getResourceParam')->willReturn($this->guid);
        $this->request->method('getSubObjects')->willReturn([
            'schedules' => []]);
        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->courseSection4);
        $this->viewCourseSectionScheduleService->method('getCollectionByCourseSectionGuidCollection')
            ->willReturn($this->courseSectionScheduleCollection1);
        $actualResult = $this->courseSectionService->getCourseSectionGuid()->getPayload();
        $this->assertEquals(array_merge($this->courseSectionPayload4, $this->schedulePayload), $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithCrossListedCourseSection()
    {
        $this->request->method('getResourceParam')->willReturn($this->guid);
        $this->request->method('getSubObjects')->willReturn([
            'crossListedCourseSections' => []]);
        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->courseSection4);
        $this->viewCrossListedCourseSectionService->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->crossListedCourseSectionCollection1);
        $actualResult = $this->courseSectionService->getCourseSectionGuid()->getPayload();
        $this->assertEquals(array_merge($this->courseSectionPayload4, $this->crossListedCourseSectionePayload), $actualResult);
    }

    public function testGetCourseSectionByCourseSectionGuidWithEnrollmentDistribution()
    {
        $this->request->method('getResourceParam')->willReturn($this->guid);
        $this->request->method('getSubObjects')->willReturn([
            'enrollmentDistribution' => []]);
        $this->viewCourseSectionService->method('getByGuid')
            ->willReturn($this->courseSection4);
        $this->viewCourseSectionEnrollmentDistributionService->method('getCollectionByCourseSectionGuids')
            ->willReturn($this->courseSectionEnrollmentDistributionCollection);
        $actualResult = $this->courseSectionService->getCourseSectionGuid()->getPayload();
        $this->assertEquals(array_merge($this->courseSectionPayload4, $this->courseSectionEnrollmentDistributionPayload), $actualResult);
    }
}