# Schedule Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|startDate|2017-08-12|`v2`.startDate| |
|endDate|2017-12-12|`v2`.endDate| |
|startTime|15:00|`v2`.startTime| |
|endTime|17:00|`v2`.endTime| |
|roomNumber|0028|`v2`.room| |
|buildingCode|FSB|`v2`.buildingCode| |
|buildingName|Farmer School of Business|`v2`.buildingName| |
|days|T|`v2`.days| |
|scheduleTypeCode|FEXM|`v2`.scheduleTypeCode| |
|scheduleTypeDescription|Final Exam|`v2`.scheduleTypeDescription| |
