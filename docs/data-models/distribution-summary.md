# Distribution Summary Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|description|undergraduate|`v2`.undergraduate| |
|numberOfCurrentEnrollment|20|`v2`.enrollmentcountCurrent| |
|numberOfActiveEnrollment|20|`v2`.enrollmentcountActive| |
