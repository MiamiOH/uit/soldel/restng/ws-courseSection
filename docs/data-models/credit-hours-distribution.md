# Credit Hours Distribution Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|hours|3|`v2`.hours| |
|numberOfCurrentEnrollment|3|`v2`.enrollmentCountCurrent| |
|numberOfActiveEnrollment|3|`v2`.enrollmentCountActive| |
