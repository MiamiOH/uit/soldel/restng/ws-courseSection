# Instructor Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|isPrimary|true|`v2`.instructors[\<index\>].primaryInstructor| |
|person|[person](./person.md)|(see comment)|Person data (e.g. username, nameLst, etc.) is under root level of instructor model. It is now moved to under [person](./person.md) model|
