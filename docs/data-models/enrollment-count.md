# Enrollment Count Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|numberOfMax|16|`v2`.enrollmentCountMax| |
|numberOfCurrent|16|`v2`.enrollmentCountCurrent| |
|numberOfActive|16|`v2`.enrollmentCountActive| |
|numberOfAvailable|0|`v2`.enrollmentCountAvailable| |