# Person Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|uniqueId|eighmeje|`v2`.username| |
|lastName|Eighme|`v2`.nameLast| |
|firstName|Jan|`v2`.nameFirst| |
|middleName|Ellen|`v2`.nameMiddle| |
|prefix|Dr.|`v2`.namePrefix| |
|suffix|null|`v2`.nameSuffix| |
|preferredFirstName|null|`v2`.nameFirstPrefered| |
|informalDisplayedName|Jan Ellen Eighme|`v2`.nameDisplayInformal| |
|formalDisplayedName|Dr. Jan Ellen Eighme|`v2`.nameDisplayFormal| |
|informalSortedName|Eighme, Jan Ellen|`v2`.nameSortedInformal| |
|formalSortedName|Eighme, Jan Ellen Dr.|`v2`.nameSortedFormal| |

## Note:
 1. `personResource` is removed.