# Part Of Term Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|code|1|`v2`.partOfTermCode| |
|name|Full Semester|`v2`.partOfTermName| |
|startDate|2017-08-28|`v2`.partOfTermStartDate| |
|endDate|2017-12-16|`v2`.partOfTermEndDate| |