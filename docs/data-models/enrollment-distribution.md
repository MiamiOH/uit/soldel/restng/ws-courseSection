# Enrollment Distribution Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|levelDistribution|a collection of [level distribution](./level-distribution.md)|`v2`.levelDistribution| |
|creditHoursDistribution|a collection of [credit hours distribution](./credit-hours-distribution.md)|`v2`.creditHoursDistribution| |
|summaries|a collection of [distribution summary](./distribution-summary.md)|`v2`.summaries|Change it to collections|
