# Course Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|schoolCode|BU|`v2`.schoolCode| |
|schoolName|Farmer School of Business|`v2`.schoolName|
|departmentName|Accountancy|`v2`.deptName| |
|departmentCode|ACC|`v2`.deptCode| |
|title|Government and NFP Accounting|`v2`.courseTitle| |
|subjectCode|ACC|`v2`.courseSubjectCode| |
|subjectDescription|Accountancy|`v2`.courseSubjectDesc| |
|number|568|`v2`.courseNumber| |
|lectureHoursDescription|3|`v2`.lectureHoursDesc| |
|labHoursDescription|null|`v2`.labHoursDesc| |
|creditHoursHigh|3|`v2`.creditHoursHigh| |
|creditHoursLow|3|`v2`.creditHoursLow| |
|lectureHoursLow|3|`v2`.lectureHoursLow| |
|lectureHoursHigh|3|`v2`.lectureHoursHigh| |
|labHoursLow|0|`v2`.labHoursLow| |
|labHoursHigh|0|`v2`.labHoursHigh| |
|description|Accounting for Governmental Organization|`v2`.courseDescription| |
