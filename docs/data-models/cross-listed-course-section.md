# Cross Listed Course Section Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|crn|68057|`v2`.crn| |
|courseSectionCode|A|N/A|Added "courseSectionCode"|
|sectionName|ACC 568 A|N/A|Added "sectionName"|
|courseSubjectCode|ACC|`v2`.subjectCode| |
|courseNumber|468|`v2`.courseNumber| |
|courseSectionGuid|2bf6ded8-be1a-3327-1026-f65fafd37ad5|N/A|Added "courseSectionGuid"|
