# Core Course Section Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|termCode|201810|`v2`.academicTerm| |
|termDescription|Fall Semester 2018 - 19|`v2`.academicTermDesc| |
|crn|72583|`v2`.courseId| |
|courseSectionGuid|1cf6ded8-be1a-3327-1026-f65fafd37ad5|N/A|Added "courseSectionGuid"|
|sectionName|ACC 568 A|`v2`.courseCode| |
|instructionalTypeCode|L|`v2`.instructionalType| |
|instructionalTypeDescription|Lecture|`v2`.instructionalTypeDescription| |
|courseSectionCode|A|`v2`.courseSectionCode| |
|courseSectionStatusCode|A|N/A|Added "courseSectionStatusCode"|
|courseSectionStatusDescription|Active|`v2`.courseStatus| |
|campusCode|O|`v2`.campusCode| |
|campusName|Oxford|`v2`.campusName| |
|creditHoursDescription|1 to 3|`v2`.creditHoursDesc| |
|creditHoursLow|3|`v2`.creditHoursLow| |
|creditHoursHigh|3|`v2`.creditHoursHigh| |
|enrollmentCount|an [enrollmentCount](./enrollment-count.md)|(see comment)|Original fields are in root level in old data model. They are now under [enrollmentCount](./enrollment-count.md) model|
|partOfTerm|a [partOfTerm](./part-of-term.md)|(see comment)|Original fields are in root level in old data model. They are now under [partOfTerm](./part-of-term.md) model|
|isMidtermGradeSubmissionAvailable|false|`v2`.midtermGradeSubmissionAvailable|Value is either 'Y' or 'N' in old data model. It is now boolean value, true or false.|
|isFinalGradeSubmissionAvailable|false|`v2`.finalGradeSubmissionAvailable|Value is either 'Y' or 'N' in old data model. It is now boolean value, true or false.|
|isFinalGradeRequired|true|`v2`.gradeRequiredFinal| |
|isDisplayed|true|`v2`.prntInd|Value is either 'Y' or 'N' in old data model. It is now boolean value, true or false.|
|schedules|a collection of [Schedules](./schedule.md)|`v2`.courseSchedules| |
|instructors|a collection of [Instructors](./instructor.md)|`v2`.instructors| |
|attributes|a collection of [attributes](./attribute.md)|`v2`.attributes| |
|crossListedCourseSections|a collection of [sections](./cross-listed-course-section.md)|`v2`.crossListedSections| |
|enrollmentDistribution|an [enrollmentDistribution](./enrollment-distribution.md)|`v2`.enrollmentDistributions| |
|standardizedDivisionCode|FSB|`v2`.standardizedDivisionCode| |
|standardizedDivisionName|Farmer School of Business|`v2`.standardizedDivisionName| |
|standardizedDepartmentCode|ACC|`v2`.standardizedDeptCode|TraditionalStandardizedDepartmentCode/Name stores the old name and standardizedDepartmentCode/Name is the current name of those changed department code/name. For example, ZOO was the old department code. So traditionalStandardizedDepartmentCode would be ZOO. New department name for ZOO is BIO, then the standardizedDepartmentCode become "BIO".|
|standardizedDepartmentName|Accountancy|`v2`.standardizedDeptName| |
|LegacyStandardizedDepartmentCode|ACC|`v2`.traditionalStandardizedDeptCode| |
|LegacyStandardizedDepartmentName|Accountancy|`v2`.traditionalStandardizedDeptName| |
|creditHoursAvailable|[1,1.5,2.0,2.5,3]|N/A| |
|course|a [course](./course.md)|`v2`.course| |

## Note:
 1. `v2` is the data model used in RestServer CourseSectionV2 web service.
 2. `recordNumber` is removed since it does not belong to [courseSection](./core-course-section.md) model.
 3. All `resource` attributes (including **courseSectionResource**`, **enrollmentResource** and **academicTermResource**) are removed.
 4. `enrollmentCount`, `instructors`, `attributes`, `schedules`, `crossListedCourseSections` and `enrollmentDistribution` are not returned by default. They are only returned when user requesting them explicitly.
