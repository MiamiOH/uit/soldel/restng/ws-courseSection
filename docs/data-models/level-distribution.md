# Level Distribution Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|code|10|`v2`.code| |
|description|Master|`v2`.description| |
|numberOfCurrentEnrollment|16|`v2`.numberOfCurrentEnrollment| |
|numberOfActiveEnrollment|16|`v2`.numberOfActiveEnrollment| |
