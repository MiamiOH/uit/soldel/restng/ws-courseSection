# Attribute Data Model

|Key|Sample Value|Old Model Key|Comments|
|:-----:|:-------:|:-------:|:-------:|
|code|BUS|`v2`.attributes[\<index\>].attributeCode| |
|description|Bus Course for Surcharge|`v2`.attributes[\<index\>].attributeDescription| |
