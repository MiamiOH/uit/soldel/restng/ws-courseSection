# Course Section Participant Data Model

|Key|Sample Value|
|:-----:|:-------:|
|termCode|201710 | 
|crn| 70591| 
|courseSectionGuid|387425cc-7fe8-44e9-9278-229cad6c064e | 
|studentLevel|GR |
|creditHours| 2| 
|hasAttended| true |
|enrollmentStatusCode|RE |
|enrollmentStatusDescription|Registered |
|isEnrollmentActive|true |
|isMidtermGradeRequired|false |
|isFinalGradeRequired| false| 
|isGradeSubmissionEligible| false| 
|gradeSubmissionEligibleComment|Grade has already been rolled into academic history | 
|grades| [ grades model](./grades.md) |
|uniqueId|akermasc | 
|role| student| 
|title|Conservation Science&Community | 