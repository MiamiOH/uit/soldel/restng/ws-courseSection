# Resolution: GUID Not Found

> **Notes:**  
> At the date (10/31/2019) I wrote this post, Miami's Banner Student API version is 9.15. This post will no longer be valid once we upgrade it to 9.17.

## The Issue
You may get a similar response below when you trying to retrieve student's course section participant information:

Request:
```bash
curl "https://ws.apps.miamioh.edu/api/courseSection/v3/participant?uniqueId=<unique_id>&token=<token>"
```

Response: 
```json
{
  "data": {
    "message": "Course section not found (GUID: 638492ed-e4ac-412a-8373-6e2f4173a3f5).",
    "line": 136,
    "file": "/var/www/restng-addons/vendor/miamioh/ws-course-section/src/Services/BaseCourseSectionService.php"
  },
  "status": 500,
  "error": true
}
```

## Root Cause & Permanent Fix

The issue above is caused by a known Ellucian defect `CR-000167459`. On 09/26/2019, Ellucian has released the fix in the `BA STU API 9.17`. This bug will continue to happen unless Miami upgrades to 9.17.

## Temporary Resolution

Find course section information:
```sql
SELECT ssbsgid_guid,
       ssbsect_term_code,
       ssbsect_crn,
       ssbsect_subj_code,
       ssbsect_crse_numb
       ssbsect_seq_numb
FROM   ssbsect
       join ssbsgid
         ON ssbsgid_term_code = ssbsect_term_code
            AND ssbsgid_crn = ssbsect_crn
WHERE  ssbsgid_guid = '<put_guid_here>'; 
```
Output:

|SSBSGID_GUID|SSBSECT_TERM_CODE|SSBSECT_CRN|SSBSECT_SUBJ_CODE|SSBSECT_CRSE_NUMB|SSBSECT_SEQ_NUMB|
|-----|------|------|-----|-----|-----|
|638492ed-e4ac-412a-8373-6e2f4173a3f5|202010|76594|IMS|341|UA|

Find course information:
```sql
SELECT scbcgid_guid,
       scbcgid_subj_code,
       scbcgid_crse_numb,
       scbcgid_term_code_eff
FROM   scbcgid
WHERE  scbcgid_subj_code = '<subject_code_from_query_above>'
       AND scbcgid_crse_numb = '<course_number_from_query_above>'; 
```
Output:

|SCBCGID_GUID|SCBCGID_SUBJ_CODE|SCBCGID_CRSE_NUMB|SCBCGID_TERM_CODE_EFF|
|-----|-----|-----|-----|
|99cd5a33-0919-49dd-8d47-9b3ff15b857d|IMS|341|202020|

From the example above, `IMS 341 UA` section in the term `202010` does not match any valid version of course (the course from the second query is not valid until `202020`).

The fix will be as simple as (use data from the example above), ask DBA to run a SQL query:
```sql
UPDATE scbcgid
SET    scbcgid_term_code_eff = '202010'
WHERE  scbcgid_guid = '99cd5a33-0919-49dd-8d47-9b3ff15b857d'; 
```
