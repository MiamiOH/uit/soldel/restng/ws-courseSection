## Pagination

Pagination is forced enabled in `/courseSection/v3/courseSection` resource. 

### Page Size

By default, page size is 50. It can be customized by specifying 
`limit` query parameter. The max page size can be specified is 200 
inclusive. Value larger than 200 will therefore, cause a 
`400 BadRequest` exception.

### Offset

By default, offset is starting from 1. You can customized it by 
specifying `offset` query parameter.

### Exception

Parsing negative number, non-numeric value or zero to 
`limit` or `offset` will cause a `400 BadRequest` exception.

### Example

 1. Making GET request to `uri`?limit=5 will return first 5 records.
 2. Making GET request to `uri`?offset=10 will return 50 records starting from 10th position.
 3. Making GET request to `uri`?offset=100&limit=10 will return 10 records starting from 100th position.
 4. Making GET request to `uri`?offset=0 will return a BadRequest exception.
 5. Making GET request to `uri`?limit=-3 will return a BadRequest exception.
