## General Expectations

 - each filter has `AND` relation with other filters. For example, 
 calling to resource `url?termCode=201710&crn=12345` will return a course section
 which is in term, 201710 and has crn, 12345.
 - each comma separated value in the filter has `OR` relation with other values. 
 For example, calling resource `uri?termCode=201710,201720` will return all sections
 in the term 201710 or term 201720.

## Accepted Filters

### courseSectionGuid

|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?courseSectionGuid=`guid`|`uri`?courseSectionGuid=a1a102d5-b0e8-4f9d-b503-2d8b3cb625f8|
|a list of string, comma separated|`uri`?courseSectionGuid=`guid 1`,`guid 2`,...,`guid n`>|`uri`?courseSectionGuid=a1a102d5-b0e8-4f9d-b503-2d8b3cb625f8,95624a85-5e36-425c-b2b5-a2cfb9455e1b|

### campusCode
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?campusCode=`code`|`uri`?campusCode=O|
|a list of string, comma separated|`uri`?campusCode=`code 1`,`code 2`,...,`code n`>|`uri`?campusCode=O,H|

### termCode
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?termCode=`code`|`uri`?termCode=201710|
|a list of string, comma separated|`uri`?termCode=`code 1`,`code 2`,...,`code n`>|`uri`?termCode=201710,201720|

### crn
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?crn=`crn`|`uri`?crn=10111|
|a list of string, comma separated|`uri`?crn=`crn 1`,`crn 2`,...,`crn n`>|`uri`?crn=10111,70591|

### course_subjectCode
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?subjectCode=`code`|`uri`?subjectCode=CSE|
|a list of string, comma separated|`uri`?subjectCode=`code 1`,`code 2`,...,`code n`>|`uri`?subjectCode=CSE,ENG|

### course_number
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?courseNumber=`courseNumber`|`uri`?courseNumber=101|
|a list of string, comma separated|`uri`?courseNumber=`courseNumber 1`,`courseNumber 2`,...,`courseNumber n`>|`uri`?courseNumber=101,102|

### courseSectionCode
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?courseSectionCode=`code`|`uri`?courseSectionCode=A|
|a list of string, comma separated|`uri`?courseSectionCode=`code 1`,`code 2`,...,`code n`>|`uri`?courseSectionCode=A,B,C|

### courseSectionStatusCode
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?courseSectionStatusCode=`code`|`uri`?courseSectionStatusCode=A|
|a list of string, comma separated|`uri`?courseSectionStatusCode=`code 1`,`code 2`,...,`code n`>|`uri`?courseSectionStatusCode=A,I|

### enrollmentCount_numberOfMax
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|integer|`uri`?numberOfMaxEnrollments=`number`|`uri`?numberOfMaxEnrollments=10|
|counter range|`uri`?numberOfMaxEnrollments=`number to number`|`uri`?numberOfMaxEnrollments=10 to 20|

### enrollmentCount_numberOfCurrent
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|integer|`uri`?numberOfCurrentEnrollments=`number`|`uri`?numberOfCurrentEnrollments=10|
|counter range|`uri`?numberOfCurrentEnrollments=`number to number`|`uri`?numberOfCurrentEnrollments=10 to 20|

### enrollmentCount_numberOfActive
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|integer|`uri`?numberOfActiveEnrollments=`number`|`uri`?numberOfActiveEnrollments=10|
|counter range|`uri`?numberOfActiveEnrollments=`number to number`|`uri`?numberOfActiveEnrollments=10 to 20|

### enrollmentCount_numberOfAvailable
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|integer|`uri`?numberOfAvailableEnrollments=`number`|`uri`?numberOfAvailableEnrollments=10|
|counter range|`uri`?numberOfAvailableEnrollments=`number to number`|`uri`?numberOfAvailableEnrollments=10 to 20|

### isMidtermGradeSubmissionAvailable
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|boolean|`uri`?isMidtermGradeSubmissionAvailable=`boolean`|`uri`?isMidtermGradeSubmissionAvailable=true|

### isFinalGradeSubmissionAvailable
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|boolean|`uri`?isFinalGradeSubmissionAvailable=`boolean`|`uri`?isFinalGradeSubmissionAvailable=true|

### hasSeatAvailable
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|boolean|`uri`?hasSeatAvailable=`boolean`|`uri`?hasSeatAvailable=true|

### partOfTerm_code
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?partOfTermCode=`code`|`uri`?partOfTermCode=1|
|a list of string, comma separated|`uri`?partOfTermCode=`code 1`,`code 2`,...,`code n`>|`uri`?partOfTermCode=1,H|

### partOfTerm_startDate
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|date|`uri`?partOfTermStartDate=`<YYYY-MM-DD>`|`uri`?partOfTermStartDate=2017-01-03|
|date range|`uri`?partOfTermStartDate=`<YYYY-MM-DD> to <YYYY-MM-DD>`|`uri`?partOfTermStartDate=2017-01-03 to 2017-02-28|

#### Note:
 - this parameter used to filter course sections started after (on) the specified date.

### partOfTerm_endDate
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|date|`uri`?partOfTermEndDate=`<YYYY-MM-DD>`|`uri`?partOfTermEndDate=2017-01-03|
|date range|`uri`?partOfTermEndDate=`<YYYY-MM-DD> to <YYYY-MM-DD>`|`uri`?partOfTermEndDate=2017-01-03 to 2017-02-28|

#### Note:
 - this parameter used to filter course sections ended before (on) the specified date.

### schedules_startDate
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|date|`uri`?scheduleStartDate=`<YYYY-MM-DD>`|`uri`?scheduleStartDate=2017-01-03|
|date range|`uri`?scheduleStartDate=`<YYYY-MM-DD> to <YYYY-MM-DD>`|`uri`?scheduleStartDate=2017-01-03 to 2017-02-28|

#### Note:
 - this parameter used to filter course sections which scheduled date started after (on) the specified date.

### schedules_endDate
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|date|`uri`?scheduleEndDate=`<YYYY-MM-DD>`|`uri`?scheduleEndDate=2017-01-03|
|date range|`uri`?scheduleEndDate=`<YYYY-MM-DD> to <YYYY-MM-DD>`|`uri`?scheduleEndDate=2017-01-03 to 2017-02-28|

#### Note:
 - this parameter used to filter course sections which scheduled date ended before (on) the specified date.

### schedules_startTime
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|time|`uri`?scheduleStartTime=`<HH:ii>`|`uri`?scheduleStartTime=14:35|
|time range|`uri`?scheduleStartTime=`<HH:ii> to <HH:ii>`|`uri`?scheduleStartTime=14:35 to 15:50|

#### Note:
 - this parameter used to filter course sections which scheduled time started after (on) the specified time.

### schedules_endTime
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|time|`uri`?scheduleEndTime=`<HH:ii>`|`uri`?scheduleEndTime=14:35|
|time range|`uri`?scheduleEndTime=`<HH:ii> to <HH:ii>`|`uri`?scheduleEndTime=14:35 to 15:50|

#### Note:
 - this parameter used to filter course sections which scheduled time ended before (on) the specified time.

### schedules_buildingCode
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?buildingCode=`code`|`uri`?buildingCode=BEN|
|a list of string, comma separated|`uri`?buildingCode=`code 1`,`code 2`,...,`code n`>|`uri`?buildingCode=BAC,HYT,BEN|

### schedules_roomNumber
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?roomNumber=`code`|`uri`?roomNumber=206|
|a list of string, comma separated|`uri`?roomNumber=`code 1`,`code 2`,...,`code n`>|`uri`?roomNumber=206,207|

### schedules_Days
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?days=`code`|`uri`?scheduleDays=MWF|

#### Note:
 - each value specified in the filter has `AND` relation with other values. 
 For example, applying filter `scheduleDays=MWF` will return section(s) 
 scheduled on Monday, Wednesday and Friday.

#### Valid Values:
 - M (Monday)
 - T (Tuesday)
 - W (Wednesday)
 - R (Thursday)
 - F (Friday)
 - S (Saturday)
 - U (Sunday)

### instructors_uniqueId
|Description|Accepted Format|Sample Usage|
|:---------:|:-------------:|:----------:|
|string|`uri`?instructorUniqueId=`uniqueId`|`uri`?instructorUniqueId=kidddw|
|a list of string, comma separated|`uri`?instructorUniqueId=`uniqueId 1`,`uniqueId 2`,...,`uniqueId n`>|`uri`?instructorUniqueId=kidddw,tepeds|

