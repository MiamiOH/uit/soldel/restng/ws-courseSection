<?php

return [
    'resources' => [
        'courseSection' => [
            MiamiOH\CourseSectionWebService\Resources\CourseSectionResourceProvider::class,
            MiamiOH\CourseSectionWebService\Resources\ParticipantResourceProvider::class,
            MiamiOH\CourseSectionWebService\Resources\EnrollmentResourceProvider::class,
            MiamiOH\CourseSectionWebService\Resources\ClassListResourceProvider::class,
        ]
    ]
];